<?php
/**
 * Template Name: equipment start
 */
get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/start.css">

<section class="chief"
         style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_start/main.jpg')">
    <div class="chief__container">
        <div class="breadcrumbs">
            <?php the_breadcrumb() ?>
        </div>
        <h1 class="chief__title">
            Комплектация <br>
            START
        </h1>
        <div class="chief__description">
            <p class="chief__description__information">
                Включает в себя только панель управления с различными способами оплаты и контроллер, либо электрический
                шкаф в сборе, с помощью которого управляется силовое и механическое оборудование.
            </p>
            <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                    var s = d.createElement('script');
                    s.async = true;
                    s.src = u + '?' + (Date.now() / 180000 | 0);
                    var h = d.getElementsByTagName('script')[0];
                    h.parentNode.insertBefore(s, h);
                })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
            <button class="chief__description__button">Оформить заявку</button>
        </div>
    </div>
</section>

<section class="small__advantages">
    <div class="small__advantages__container">
        <div class="small__advantages__block">
            <img class="small__advantages__image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon1.png" alt="">
            <p class="small__advantages__block__bold_text">
                Используйте любые компоненты на своё усмотрение
            </p>
            <p class="small__advantages__block__normal_text">
                Двигатели, насосы, клапана, моющие пистолеты, консоли, колчаны и т.д. покупаются отдельно
            </p>
        </div>
        <div class="small__advantages__block">
            <img class="small__advantages__image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon2.png" alt="">
            <p class="small__advantages__block__bold_text">
                Экономьте на монтаже оборудования
            </p>
            <p class="small__advantages__block__normal_text">
                Монтаж оборудования осуществляется силами заказчика
            </p>
        </div>
        <div class="small__advantages__block">
            <img class="small__advantages__image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon3.png" alt="">
            <p class="small__advantages__block__bold_text">
                START – для самостоятельной сборки МСО
            </p>
            <p class="small__advantages__block__normal_text">
                Гарантия на управляющий контроллер и пульт – 1 год
            </p>
        </div>
    </div>
</section>

<section class="equipment__description">
    <div class="equipment__description__container">
        <div class="equipment__description__title">
            <h2>
                Комплектация START-1
            </h2>
        </div>

        <div class="equipment__description__blocks">
            <div class="equipment__description__block1">
                <img class="block__image_1" src="<?= get_template_directory_uri(); ?>/img/equipment_start/6.jpg"
                     title="Панель управления start от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления start оборудование  мойки самообслуживания">

                <div class="equipment__description__block1__information">
                    <div class="equipment__description__block1__column">
                        <p class="equipment__description__subtitle">Моющие программы:</p>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon4.png" alt="">
                            <p>пена</p>
                        </div>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon5.png" alt="">
                            <p>вода под давлением</p>
                        </div>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon6.png" alt="">
                            <p>воск</p>
                        </div>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon7.png" alt="">
                            <p>стоп/пауза</p>
                        </div>
                    </div>
                    <button class="equipment__description__button">Рассчитать стоимость</button>
                </div>
            </div>
            <div class="equipment__description__block2">
                <p class="equipment__description__block2__subtitle">
                    Панель управления из нержавеющей стали
                </p>
                <ul>
                    <li>Информационный 4-х секционный дисплей</li>
                    <li>Моющие программы (пена, вода под давлением, воск)</li>
                    <li>Возможность подключения дополнительных программ (воздух, воск, осмос, мойка дисков, средство от
                        насекомых, щетка)
                    </li>
                    <li>Купюроприемник ICT</li>
                    <li>Монетоприёмник EU2</li>
                    <li>Оплата картами лояльности</li>
                    <li>Пеноизоляция</li>
                    <li>Теплоизоляция</li>
                    <li>Контроллер управления силовым оборудованием</li>
                </ul>

                <p class="equipment__description__p">
                    Возможны другие цветовые решения
                </p>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Цена
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                1 300 €
                            </div>
                            <div class="block__cost__number2">
                                474 691 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dop__description dop__description__grey">
    <div class="dop__description__container dop__description__container__reverse dop__description__container__padding">
        <div class="dop__description__block__img">
            <img class="dop__description__image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/1.jpg"
                 title="Сборочный цех компании Nerta-SW"
                 alt="Сборка силовой рамы оборудования для мойки самообслуживания |Nerta-SW">
        </div>
        <div class="dop__description__block ">
            <div class="dop__description__block__title">
                <p>Дополнительные опции</p>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Купюроприемник
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            200 €
                        </div>
                        <div class="block__cost__number3">
                            20 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Система удаленного контроля и мониторинга
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            300 €
                        </div>
                        <div class="block__cost__number3">
                            30 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Оплата пластиковыми картами лояльности
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            200 €
                        </div>
                        <div class="block__cost__number3">
                            20 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Бесконтактный расчет банковскими картами, смартфонами (Apple/Samsung/ GooglePay)
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            400 €
                        </div>
                        <div class="block__cost__number3">
                            40 000 ₽
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="equipment__description">
    <div class="equipment__description__container">
        <div class="equipment__description__title">
            <h2>
                Комплектация START-2
            </h2>
        </div>

        <div class="equipment__description__blocks">
            <div class="equipment__description__block2">
                <p class="equipment__description__block2__subtitle">
                    Включает в себя панель и электрический шкаф. С помощью которого управляется силовое и механическое
                    оборудование.
                </p>
                <p class="equipment__description__block2__p">
                    Пульт управления из нержавеющей стали
                </p>
                <ul>
                    <li>Информационный 4-х секционный дисплей</li>
                    <li>Моющие программы (пена, вода под давлением, воск)</li>
                    <li>Возможность подключения дополнительных программ (воздух, воск, осмос, мойка дисков, средство от
                        насекомых, щетка)
                    </li>
                    <li>Купюроприемник ICT</li>
                    <li>Монетоприёмник EU2</li>
                    <li>Оплата картами лояльности</li>
                    <li>Пеноизоляция</li>
                    <li>Теплоизоляция</li>
                </ul>
            </div>
            <div class="equipment__description__block1">
                <img class="block__image_1" src="<?= get_template_directory_uri(); ?>/img/equipment_start/4.jpg"
                     title="Панель управления start-2 от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления start-2 оборудование  мойки самообслуживания">

                <div class="equipment__description__block1__information">
                    <div class="equipment__description__block1__column">
                        <p class="equipment__description__subtitle">Моющие программы:</p>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon4.png" alt="">
                            <p>пена</p>
                        </div>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon5.png" alt="">
                            <p>вода под давлением</p>
                        </div>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon6.png" alt="">
                            <p>воск</p>
                        </div>
                        <div class="equipment__description__block1__row">
                            <img class="inf__block__image"
                                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/icon7.png" alt="">
                            <p>стоп/пауза</p>
                        </div>
                    </div>
                    <button class="equipment__description__button">Рассчитать стоимость</button>
                </div>
            </div>

        </div>
    </div>
</section>

<section class="dop__description dop__description__blue">
    <div class="dop__description__container dop__description__container__reverse dop__description__container__padding ">
        <div class="dop__description__block ">
            <div class="dop__description__block__title">
                <p>Электротехнический шкаф Schneider Electric</p>
            </div>
            <ul>
                <li>
                    Магнитный пускатель Schneider Electric
                </li>
                <li>
                    Трансформатор Schneider Electric
                </li>
                <li>
                    Автоматические выключатели Schneider Electric
                </li>
                <li>
                    Плата с функцией контроля и мониторинга (выручка, расход моющего средства, электричества, воду и
                    т.д.)
                </li>
                <li>
                    Дозатор шампуня Etatron
                </li>
            </ul>
            <p class="dop__description__p">
                Возможны другие цветовые решения
            </p>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Цена
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            2 600 €
                        </div>
                        <div class="block__cost__number3">
                            260 000 ₽
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dop__description__block__img ">
            <img class="dop__description__image" src="<?= get_template_directory_uri(); ?>/img/equipment_start/2.jpg"
                 title="Электротехнический шкаф от производителя оборудования для моек самообслуживания  NERTA-SW"
                 alt="Электротехнический шкаф для мойки самообслуживания">
        </div>
    </div>
</section>

<!-- Slider main container -->
<section class="swiper__content swiper__content__grey">
    <div class="swiper">
        <div class="swiper-button-container">
            <div class="swiper-button-prev swiper-button-prev1"></div>
            <div class="swiper-button-next swiper-button-next1"></div>
        </div>
        <div class="swiper-container swiper-1">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s1.png"
                         alt="шкаф управления оборудованием мойки самообслуживания |Nerta-SW"
                         title="Шкаф управления оборудованием для мойки самообслуживания из нержавейки|Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Тепло-гидроизоляция
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s2.png"
                         title="Система дозировки для моек самообслуживания |Nerta-SW"
                         alt="Система дозировки Etatron для мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Система дозировки Etatron
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s3.png"
                         title="Рукав высокого давления для моек самообслуживания |Nerta-SW"
                         alt="Рукав высокого давления для мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Рукав высокого давления
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s4.png"
                         title="Электродвигатель для моек самообслуживания |Nerta-SW"
                         alt="Электродвигатель Ravel для мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Электродвигатель Ravel
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s5.png"
                         title="Монетоприёмник для моек самообслуживания |Nerta-SW"
                         alt="Монетоприёмник EU-2 для мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Монетоприёмник EU-2
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s6.png"
                         title="Электромагнитный клапан Danfoss  для мойки самообслуживания |Nerta-SW"
                         alt="Электромагнитный клапан для моек самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Электромагнитный клапан Danfoss
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s7.png"
                         title="Купюроприёмник для моек самообслуживания |Nerta-SW"
                         alt="Купюроприёмник ICTдля мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Купюроприемник ICT
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s8.png"
                         title="Воздушный компрессор для моек самообслуживания |Nerta-SW"
                         alt="Воздушный компрессор  Remez для мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Воздушный компрессор Remeza (Италия - Белоруссия)
                    </p>
                </div>
                <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                    <img class="swiper-slide__img" src="<?= get_template_directory_uri(); ?>/img/equipment_start/s9.png"
                         title="Система дозировки для моек самообслуживания |Nerta-SW"
                         alt="Дозатор моющего средства SECO для мойки самообслуживания |Nerta-SW">
                    <p class="swiper-slide__image__options">
                        Дозатор моющего средства SECO
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="dop__description">
    <div class="dop__description__container dop__description__container__margin">
        <div class="dop__description__block ">
            <div class="dop__description__block__title">
                <p>Дополнительные опции</p>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Электромагнитный клапан 30-150 бар NRT
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            200 €
                        </div>
                        <div class="block__cost__number3">
                            20 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Дополнительный дозатор (пена, воск) Etatron
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            300 €
                        </div>
                        <div class="block__cost__number3">
                            30 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Удаленный контроль и мониторинг
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            200 €
                        </div>
                        <div class="block__cost__number3">
                            20 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Оплата пластиковыми картами лояльности
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            200 €
                        </div>
                        <div class="block__cost__number3">
                            20 000 ₽
                        </div>
                    </div>
                </div>
            </div>
            <div class="block__cost">
                <div class="block__cost__container">
                    <div class="block__cost__text">
                        Бесконтактный расчет банковскими картами, смартфонами (Apple/Samsung/ GooglePay)
                    </div>
                    <div class="block__cost__number">
                        <div class="block__cost__number1">
                            400 €
                        </div>
                        <div class="block__cost__number3">
                            40 000 ₽
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="dop__description__block__img ">
            <img class="dop__description__image" src="<?= get_template_directory_uri(); ?>/img/equipment_start/3.jpg"
                 alt="">
        </div>
    </div>
</section>

<!-- Slider main container -->
<section class="swiper__content swiper__content__display">
    <div class="swiper">
        <div class="swiper__content__title">
            <h2>
                Пример цветового оформления панелей
            </h2>
        </div>
        <div class="swiper-button-container">
            <div class="swiper-button-prev swiper-button-prev2"></div>
            <div class="swiper-button-next swiper-button-next2"></div>
        </div>
        <div class="swiper-container swiper-2">
            <div class="swiper-wrapper">
                <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                    <img class="swiper-slide__color_image"
                         src="<?= get_template_directory_uri(); ?>/img/equipment_start/4.jpg"
                         title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления start  для оборудования  мойки самообслуживания –цвет красный">
                </div>
                <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                    <img class="swiper-slide__color_image"
                         src="<?= get_template_directory_uri(); ?>/img/equipment_start/5.jpg"
                         title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления start  для оборудования  мойки самообслуживания –цвет зеленый">
                </div>
                <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                    <img class="swiper-slide__color_image"
                         src="<?= get_template_directory_uri(); ?>/img/equipment_start/6.jpg"
                         title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления start  для оборудования  мойки самообслуживания –цвет синий">
                </div>
                <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                    <img class="swiper-slide__color_image"
                         src="<?= get_template_directory_uri(); ?>/img/equipment_start/7.jpg"
                         title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления start  для оборудования  мойки самообслуживания –цвет пурпурный">
                </div>
            </div>
        </div>
    </div>
</section>

<section class="color__variations">
    <div class="color__variations__container">
        <div class="color__variations__title">
            <h2>
                Пример цветового оформления панелей
            </h2>
        </div>
        <div class="color__variations__content">
            <img class="swiper-slide__color_image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/4.jpg"
                 title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                 alt="Панель управления start  для оборудования  мойки самообслуживания –цвет красный">
            <img class="swiper-slide__color_image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/5.jpg"
                 title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                 alt="Панель управления start  для оборудования  мойки самообслуживания –цвет зеленый">
            <img class="swiper-slide__color_image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/6.jpg"
                 title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                 alt="Панель управления start  для оборудования  мойки самообслуживания –цвет синий">
            <img class="swiper-slide__color_image"
                 src="<?= get_template_directory_uri(); ?>/img/equipment_start/7.jpg"
                 title="Панель управления start- от производителя оборудования для моек самообслуживания  NERTA-SW"
                 alt="Панель управления start  для оборудования  мойки самообслуживания –цвет пурпурный">
        </div>
    </div>
</section>

<section class="feedback"
         style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_start/feed.jpg')">
    <div class="feedback__container">
        <div class="feedback__title">
            Рассчитать стоимость оборудования и сравнить характеристики
        </div>
        <p>
            Возможна постановка всех комплектующих для самостоятельной сборки полного комплекта мойки. Звоните,
            инженеры
            компании расскажут об особенностях.
        </p>
        <div class="feedback__phone_number">
            8 (800) 555-25-93
        </div>
        <div class="feedback__button">
            <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                    var s = d.createElement('script');
                    s.async = true;
                    s.src = u + '?' + (Date.now() / 180000 | 0);
                    var h = d.getElementsByTagName('script')[0];
                    h.parentNode.insertBefore(s, h);
                })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
            <button class="feedback__button__1">Оформить заявку</button>
            <button class="feedback__button__2">Калькулятор</button>
        </div>
    </div>
</section>
<!-- Slider main container -->

<section class="options__mini">
    <div class="options__mini__container">
        <div class="options__mini__subtitle">
            <h2>
                Посмотреть другие комплектации
            </h2>
        </div>
        <div class="options__mini__row">
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_basic/'"
                 class="options__mini__block color_2">
                <div class="options__mini__block__content">
                    <p class="options__mini__title__big">
                        BASE
                    </p>
                    <p class="options__mini__title">
                        Для тех кто любит дёшево.
                    </p>
                    <ul class="options__mini__list">
                        <li>Полный комплект для организации мойки;</li>
                        <li>Пена наносится под высоким давлением;</li>
                        <li>Комплектующие начального уровня (Евросоюз, Китай);</li>
                        <li>Гарантия 1 год.</li>
                    </ul>
                </div>
                <div class="options__mini__block__cost">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    5 000 €
                                </div>
                                <div class="block__cost__number2">
                                    500 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_light/'"
                 class="options__mini__block color_3">
                <div class="options__mini__block__content">
                    <p class="options__mini__title__big">
                        LIGHT
                    </p>
                    <p class="options__mini__title">
                        Начальный уровень.
                        Для тех кто экономит с умом.
                    </p>
                    <ul class="options__mini__list">
                        <li>Полный комплект для организации МСО;</li>
                        <li>Пена наносится под низким давлением;</li>
                        <li>Комплектующие начального уровня (Евросоюз, Китай, Россия);</li>
                        <li>Гарантия 1 год.</li>
                    </ul>
                </div>
                <div class="options__mini__block__cost">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    8 000 €
                                </div>
                                <div class="block__cost__number2">
                                    800 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_optima/'"
                 class="options__mini__block color_4">
                <div class="options__mini__block__content">
                    <p class="options__mini__title__big">
                        OPTIMA
                    </p>
                    <p class="options__mini__title">
                        Оптимальное соотношение цена/качества.
                        Для тех кто умеет считать.
                    </p>
                    <ul class="options__mini__list">
                        <li>Полный комплект автомоек самообслуживания;</li>
                        <li>Пена наносится поднизким давлением;</li>
                        <li>Комплектующие среднего уровня (Евросоюз).</li>
                    </ul>
                </div>
                <div class="options__mini__block__cost">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    10 500 €
                                </div>
                                <div class="block__cost__number2">
                                    1 050 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_maxi/'"
                 class="options__mini__block color_5">
                <div class="options__mini__block__content">
                    <p class="options__mini__title__big">
                        MAXI
                    </p>
                    <p class="options__mini__title">
                        Самоё надежное и неприхотливое.
                        Для тех кому лучше идеально, чем дёшево.
                    </p>
                    <ul class="options__mini__list">
                        <li>Полный комплект для мойки самообслуживания;</li>
                        <li>Пена наносится поднизким давлением;</li>
                        <li>Типовые комплектующие (Япония, Канада, Евросоюз);</li>
                        <li>Монтаж;</li>
                        <li>Гарантия 2 года.</li>
                    </ul>
                </div>
                <div class="options__mini__block__cost">
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                Цена за 1 пост
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    13 000 €
                                </div>
                                <div class="block__cost__number2">
                                    1 300 000 ₽
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<script src="<?php bloginfo("template_url"); ?>/js/cost.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/gallery.js"></script>

<?php get_footer(); ?>
