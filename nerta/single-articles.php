<?php
get_header();
$data = get_fields();
$post = get_queried_object();
$count = 1;
function translit_sef($value)
{
    $converter = array(
        'а' => 'a', 'б' => 'b', 'в' => 'v', 'г' => 'g', 'д' => 'd',
        'е' => 'e', 'ё' => 'e', 'ж' => 'zh', 'з' => 'z', 'и' => 'i',
        'й' => 'y', 'к' => 'k', 'л' => 'l', 'м' => 'm', 'н' => 'n',
        'о' => 'o', 'п' => 'p', 'р' => 'r', 'с' => 's', 'т' => 't',
        'у' => 'u', 'ф' => 'f', 'х' => 'h', 'ц' => 'c', 'ч' => 'ch',
        'ш' => 'sh', 'щ' => 'sch', 'ь' => '', 'ы' => 'y', 'ъ' => '',
        'э' => 'e', 'ю' => 'yu', 'я' => 'ya',
    );
    $value = mb_strtolower($value);
    $value = strtr($value, $converter);
    $value = mb_ereg_replace('[^-0-9a-z]', '-', $value);
    $value = mb_ereg_replace('[-]+', '-', $value);
    $value = trim($value, '-');
    return $value;
}
//To return the URL of the previous page, use the following php code:
$prev = get_permalink(get_adjacent_post(false, '', false));

//To return the URL of the next page, use the following php code:
$next = get_permalink(get_adjacent_post(false, '', true));

$link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']
    === 'on' ? "https" : "http") .
    "://" . $_SERVER['HTTP_HOST'] .
    $_SERVER['REQUEST_URI'];



?>
<div id="nerta-main-page">
    <section class="pageBanner">
        <div class="inner">
            <picture class="pageBanner__bg">
                <source srcset="<?= get_field('detail_image', $post->ID) ?>.webp" type="image/webp">
                <img src="<?= get_field('detail_image', $post->ID) ?>" alt="Баннер">
            </picture>
            <div class="pageBanner__content">
                <h1 class="pageBanner__title"><?= $post->post_title ?></h1>
                <div class="publish-date"><span>Дата публикации&nbsp;</span><span class="date"><?= get_the_date('d.m.Y', $post->ID) ?></span></div>
            </div>
            <div class="breadcrumbs"><a href="https://nerta-sw.ru">Главная</a><a href="/articles/">Статьи</a><span><?= $post->post_title ?></span></div>
        </div>
    </section>
    <section class="article-detail">
        <div class="inner">
            <div class="article-detail__head">
                <div class="notime">
                    <span>Время прочтения статьи <span class="medium"><?= get_field('time', $post->ID) ?></span></span>
                    <div class="notime__btn" style="text-align:center;">Нет времени?</div>
                    <div class="notime__form">
                        <form action="/sendtomail.php" class="subscribe" method="post">
                            <div class="form__field">
                                <input type="email" placeholder="Ваша почта" required name="mail">
                                <input type="hidden" name="url" value="<?= $link ?>">
                                <input type="hidden" name="name" value="<?= $post->post_title ?>">
                            </div>
                            <div class="form__submit">
                                <button class="btn" type="submit">Отправить</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="socials"><span>Поделиться статьёй в соц. сетях:</span>
                    <ul>
                        <li><a href="http://vkontakte.ru/share.php?url=<?= $link ?>" target="_blank">
                                <svg width="16" height="16">
                                    <use xlink:href="<?php bloginfo("template_url"); ?>/img/sprites/sprite.svg#vk"></use>
                                </svg>
                            </a></li>
                        <li><a href="http://www.facebook.com/sharer/sharer.php?u=<?= $link ?>" target="_blank">
                                <svg width="16" height="16">
                                    <use xlink:href="<?php bloginfo("template_url"); ?>/img/sprites/sprite.svg#facebook"></use>
                                </svg>
                            </a></li>
                        <li><a href="https://connect.ok.ru/offer?url=<?= $link ?>" target="_blank">
                                <svg width="16" height="16">
                                    <use xlink:href="<?php bloginfo("template_url"); ?>/img/sprites/sprite.svg#odnoklassniki"></use>
                                </svg>
                            </a></li>
                    </ul>
                </div>
            </div>
            <div class="article-detail__contents">
                <div class="h3">Содержание</div>
                <div class="article-detail__contentsLeft">
                    <ul>
                        <?php foreach (get_field('content', $post->ID) as $i => $item) : ?>
                            <?php if ($i <= 6) : ?>
                                <?php if ($item['title']) : ?>
                                    <li><a href="#<?= translit_sef($item['title']) ?>" data-number="<?= $count ?>"><?= $item['title'] ?></a></li>
                                    <?php $count++ ?>
                                <?php endif; ?>
                            <?php endif; ?>

                        <?php endforeach; ?>
                    </ul>
                </div>
                <?php if (count(get_field('content', $post->ID)) > 6) : ?>
                    <div class="article-detail__contentsRight">
                        <ul>
                            <?php foreach (get_field('content', $post->ID) as $i => $item) : ?>
                                <?php if ($i > 6) : ?>
                                    <?php if ($item['title']) : ?>
                                        <li><a href="#<?= translit_sef($item['title']) ?>" data-number="<?= $count ?>"><?= $item['title'] ?></a></li>
                                        <?php $count++ ?>
                                    <?php endif; ?>
                                <?php endif; ?>

                            <?php endforeach; ?>
                            </li>
                        </ul>
                    </div>
                <?php endif; ?>
            </div>
            <?php foreach (get_field('content', $post->ID) as $item) : ?>
                <div class="article-detail__text" id="<?= (!empty($item['title']) ? translit_sef($item['title']) : "") ?>">
                    <?= $item['text'] ?>
                </div>
            <?php endforeach; ?>
        </div>
    </section>
    <div class="article-detail__author">
        <div class="inner">
            <div class="author">
                <div class="author-image">
                    <picture>
                        <source srcset="<?= get_field('author', $post->ID)['image'] ?>.webp" type="image/webp">
                        <img src="<?= get_field('author', $post->ID)['image'] ?>" alt="" loading="lazy">
                    </picture>
                </div>
                <div class="author-text">
                    <div class="author-text__tag">Автор статьи</div>
                    <div class="author-text__name"><?= get_field('author', $post->ID)['name'] ?></div>
                    <div class="author-text__work"><?= get_field('author', $post->ID)['position'] ?></div>
                </div>
            </div>
            <div class="navigation">
                <?php if ($prev != get_permalink()) : ?>
                    <a class="nav-item" href="<?= $prev ?>">
                        <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.792892 8.74617C0.402369 8.35565 0.402369 7.72248 0.792892 7.33196L7.15685 0.967995C7.54738 0.57747 8.18054 0.57747 8.57107 0.967995C8.96159 1.35852 8.96159 1.99168 8.57107 2.38221L2.91421 8.03906L8.57107 13.6959C8.96159 14.0864 8.96159 14.7196 8.57107 15.1101C8.18054 15.5007 7.54738 15.5007 7.15685 15.1101L0.792892 8.74617ZM20.5 9.03906H1.5V7.03906H20.5V9.03906Z" fill="#A3A4A5" />
                        </svg>
                        <span>Предыдующая статья </span></a>
                <?php endif; ?>
                <?php if ($next != get_permalink()) : ?>
                    <a class="nav-item" href="<?= $next ?>"><span>Следующая статья</span>
                        <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20.2071 8.74617C20.5976 8.35565 20.5976 7.72248 20.2071 7.33196L13.8431 0.967995C13.4526 0.57747 12.8195 0.57747 12.4289 0.967995C12.0384 1.35852 12.0384 1.99168 12.4289 2.38221L18.0858 8.03906L12.4289 13.6959C12.0384 14.0864 12.0384 14.7196 12.4289 15.1101C12.8195 15.5007 13.4526 15.5007 13.8431 15.1101L20.2071 8.74617ZM0.5 9.03906H19.5V7.03906H0.5V9.03906Z" fill="#A3A4A5" />
                        </svg>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </div>
    <section class="formQuestions">
        <div class="inner">
            <div class="formQuestions__left">
                <div class="formQuestions__title">ОСТАЛИСЬ ВОПРОСЫ? ПОЛУЧИТЕ КОНСУЛЬТАЦИЮ СПЕЦИАЛИСТА<br><span class="medium">И
                        ПОМОЩЬ В ОЦЕНКЕ ВЫБРАННОГО ВАМИ ЗЕМЕЛЬНОГО УЧАСТКА</span></div>
            </div>
            <div class="formQuestions__right">
                <form class="formQuestions__form" action="/wp-json/contact-form-7/v1/contact-forms/14805/feedback" method="POST" data-form="banner">
                    <div class="form__field">
                        <input type="text" required name="your-name" placeholder="Имя">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__field">
                        <input type="tel" required name="your-phone" placeholder="+7 (9__) ___ __ __">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__submit">
                        <button type="submit">Оставить заявку</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <section class="comments formQuestions">
        <div class="inner">
            <?php comments_template('', true); ?>
        </div>
    </section>
</div>

<?php get_footer(); ?>