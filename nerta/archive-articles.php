<?php
get_header();
$data = get_fields();
$my_posts = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'articles',
));
$themes = array();
$years = array();
foreach ($my_posts as $post) {
    setup_postdata($post);
    if (!in_array(get_field('theme'), $themes)) {
        array_push($themes, get_field('theme'));
    }
    if (!in_array(get_the_date('Y'), $years)) {
        array_push($years, get_the_date('Y'));
    }
}
if ($_GET['topic'] or $_GET['years']) {

    $my_posts = get_posts(array(
        'numberposts' => -1,
        'post_type' => 'articles',
        'meta_key' => 'theme',
        'meta_value' => $_GET['topic'],
        'date_query' => array(
            array(
                'year'  => $_GET['years'] 
            ),
        ),
    ));
} else {

    $my_posts = get_posts(array(
        'numberposts' => 6,
        'post_type' => 'articles',
    ));
}
?>
<div id="nerta-main-page">
    <section class="pageBanner">
        <div class="inner">
            <picture class="pageBanner__bg">
                <source srcset="<?php bloginfo("template_url"); ?>/img/article/banner.webp" type="image/webp">
                <img src="<?php bloginfo("template_url"); ?>/img/article/banner.jpg" alt="Баннер">
            </picture>
            <div class="pageBanner__content">
                <h1 class="pageBanner__title">CТАТЬИ ПРО МОЙКИ<br> САМООБСЛУЖИВАНИЯ</h1>
                <div class="pageBanner__desc">
                    В данном разделе вы найдете информацию, связанную с организацией и развитием
                    моек самообслуживания. Делимся накопленным с 2001 года опытом.
                </div>
            </div>
            <div class="breadcrumbs"><a href="https://nerta-sw.ru">Главная</a><span>Статьи</span></div>
        </div>
    </section>
    <section class="articles">
        <div class="inner">
            <div class="articles__head">
                <!--<div class="articles__headName">Статьи</div>-->
                <div class="articles__headSort"><span>Сортировка:</span>
                    <form action="" method="GET">
                        <select name="years">
                            <option value="Год" disabled selected>Год</option>
                            <option value="" selected>За все время</option>
                            <?php foreach ($years as $item) : ?>
                                <option value="<?= $item ?>" <?= (isset($_GET['years']) and $_GET['years'] == $item) ? "selected" : "" ?>><?= $item ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select name="topic">
                            <option value="Тематика" disabled selected>Тематика</option>
                            <option value="" selected>Все тематики</option>
                            <?php foreach ($themes as $item) : ?>
                                <option value="<?= $item ?>" <?= (isset($_GET['topic']) and $_GET['topic'] == $item) ? "selected" : "" ?>><?= $item ?></option>
                            <?php endforeach; ?>

                        </select>
                    </form>
                </div>
            </div>
            <div class="articles__items">
                <?php
                foreach ($my_posts as $post) {
                    setup_postdata($post);
                ?>
                    <a class="articles__item" href="<?= get_the_permalink() ?>" title="<?= get_the_title() ?>">
                        <div class="image">
                            <picture>
                                <source srcset="<?= get_field('preview_image', get_the_ID())['url'] ?>.webp" type="image/webp">
                                <img src="<?=get_field('preview_image', get_the_ID())['url']  ?>" alt="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['title'])  ?>"  loading="lazy">
                            </picture>
                        </div>
                        <div class="name"><?= get_the_title() ?></div>
                        <div class="date"><?= get_the_date('d.m.Y') ?></div>
                    </a>
                <?php
                }
                ?>
                <?php wp_reset_postdata(); ?>
            </div>
            <?php if (!$_GET['topic'] or !$_GET['years']):?>
            <button class="articles__items--more">Показать больше</button>
            <?php endif?>
        </div>
    </section>
    <section class="formQuestions">
        <div class="inner">
            <div class="formQuestions__left">
                <div class="formQuestions__title">ОСТАЛИСЬ ВОПРОСЫ?</div>
                <div class="formQuestions__desc">Звоните или оставляйте заявку на сайте. Мы ответим на все
                    интересующие вас вопросы!
                </div>
            </div>
            <div class="formQuestions__right">
                <form class="form-component formQuestions__form" action="/wp-json/contact-form-7/v1/contact-forms/14805/feedback" method="POST" data-form="banner">
                    <div class="form__field">
                        <input type="text" required name="your-name" placeholder="Имя">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__field">
                        <input type="tel" required name="your-phone" placeholder="+7 (9__) ___ __ __">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__submit">
                        <button type="submit">Оставить заявку</button>
                    </div>
                </form>
            </div>
        </div>

    </section>
</div>
<?php get_footer(); ?>