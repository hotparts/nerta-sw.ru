<?php
/**
 * Template Name: Inner
 */

get_header(); ?>
    <div class="inner-header">
        <div class="container">
            <div class="inner-header-company">Nerta Selfwash</div>
            <?php the_title( '<h1 class="h1">', '</h1>' );?>
        </div>
    </div>

<?php
the_post();
the_content();
?>
<?php get_footer(); ?>