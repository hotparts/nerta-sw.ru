<?php
/**
 * Template Name: New index
 */
get_header(); ?>
    <!--    <link rel="stylesheet" type="text/css" href="--><?php //bloginfo("template_url"); ?><!--/css/equipments.css">-->
    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/index.css">

    <div id="nerta-main-page">

    <section class="chief" style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/main.jpg')">
        <div class="chief__container">
            <h1 class="chief__title">
                МОЙКИ <br>САМООБСЛУЖИВАНИЯ <br>NERTA-SW
            </h1>
            <div class="chief__descriptions">
                <p class="chief__descriptions__informations">
                    Произвели более
                    800 постов
                </p>
                </p>
                <p class="chief__descriptions__informations">
                    Работаем с 2000 года
                </p>
                <p class="chief__descriptions__informations">
                    Гарантия – 2 года
                </p>
                <p class="chief__descriptions__informations chief__descriptions__informations_big">
                    Построили более 30 моек самообслуживания под ключ
                </p>
                <p class="chief__descriptions__informations chief__descriptions__informations_big">
                    Собственная сеть из 20 автомоек
                </p>
                <p class="chief__descriptions__informations chief__descriptions__informations_big">
                    Система онлайн-управления и мониторинга комплексом
                </p>
            </div>
        </div>
    </section>

    <section class="services">
        <div class="services__container">
            <div class="services__row">
                <div class="services__block"
                     style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/1.jpg')">
                    <div class="services__block__content">
                        <h2 class="services__title">
                            Оборудование для мойки самообслуживания
                        </h2>
                        <ul>
                        <li class="services__block__text">
                            Собственное производство
                        </li>
                        <li class="services__block__text">
                            Гарантия – 2 года
                        </li>
                        <li class="services__block__text">
                            Большой выбор моделей
                        </li>
                        <li class="services__block__text">
                            Срок производства от 14 дней
                        </li>
                        </ul>
                    </div>
                    <div class="services__information">
                        <div class="services__information__numbers">
                            <p class="services__information__number">от 2 500 €</p>
                            <p class="services__information__number_big">от 250 000 ₽</p>
                        </div>
                        <button onclick="return location.href = '<?php home_url("template_url"); ?>/oborudovanie_dlya-moek_samoobsluzhivaniya/'" class="services__button">Подробнее</button>
                    </div>
                </div>
                <div class="services__block"
                     style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/2.jpg')">
                    <div class="services__block__content">
                        <h2 class="services__title">
                            Мойка самообслуживания под ключ
                        </h2>

                        <p class="services__block__text">
                            Проектирование
                        </p>
                        <p class="services__block__text">
                            Строительство
                        </p>
                        <p class="services__block__text">
                            Монтаж
                        </p>
                        <p class="services__block__text">
                            Техническое обслуживание
                        </p>
                    </div>
                    <div class="services__information">
                        <div class="services__information__numbers">
                            <p class="services__information__number">от 2 800 €</p>
                            <p class="services__information__number_big">от 280 000 ₽</p>
                        </div>
                        <button onclick="return location.href = '<?php home_url("template_url"); ?>/mojka-samoobsluzhivaniya-pod-klyuch/'" class="services__button">Подробнее</button>
                    </div>
                </div>
                <div class="services__block"
                     style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/3.jpg')">
                    <div class="services__block__content">
                        <h2 class="services__title">
                            Продажа автомоек самообслуживания — готовый бизнес
                        </h2>

                        <p class="services__block__text">
                            Работающие МСО
                        </p>
                        <p class="services__block__text">
                            Гарантия на оборудование
                        </p>
                        <p class="services__block__text">
                            Ремонт, обслуживание, модернизация
                        </p>
                        <p class="services__block__text">
                            Долевое участие
                        </p>
                    </div>
                    <div class="services__information">
                        <div class="services__information__numbers">
                            <p class="services__information__number">50 000 €</p>
                            <p class="services__information__number_big">от 5 000 000 ₽</p>
                        </div>
                        <button onclick="return location.href = '<?php home_url("template_url"); ?>/gotovii-biznes/'" class="services__button">Подробнее</button>
                    </div>
                </div>
                <div class="services__block_empty"
                     style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/4.jpg')">
                    <div class="services__block__content">
                        <h2 class="services__title">
                            Моющая химия для мойки самообслуживания
                        </h2>
                        <p class="services__block_empty__text">
                            Собственное производство
                        </p>
                        <p class="services__block_empty__text">
                            Без вреда для оборудования
                        </p>
                        <p class="services__block_empty__text">
                            Не влияет на окружающую среду
                        </p>
                    </div>
                    <div class="services__block_empty__information">
                        <div class="services__block_empty__information__numbers">
                            <p class="services__block_empty__information__number_big">от 1 500 ₽ за канистру</p>
                        </div>
<!--                        <button class="services__block_empty__button">Подробнее</button>-->
                    </div>
                </div>
                <div class="services__block"
                     style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/5.jpg')">
                    <div class="services__block__content">
                        <h2 class="services__title">
                            Франшиза <br> автомойки <br> Nerta-SW
                        </h2>
                        <p class="services__block__text">
                            Без роялти и паушального взноса
                        </p>
                        <p class="services__block__text">
                            Менторство
                        </p>
                        <p class="services__block__text">
                            Свобода действий
                        </p>
                    </div>
                    <div class="services__information">
                        <div class="services__information__numbers">
                            <p class="services__information__number">30 000 €</p>
                            <p class="services__information__number_big">от 3 000 000 ₽</p>
                        </div>
                        <button onclick="return location.href = '<?php home_url("template_url"); ?>/franshiza-mojka-samoobsluzhivaniya/'" class="services__button">Подробнее</button>
                    </div>
                </div>
                <div class="services__block"
                     style="background-image: url('<?= get_template_directory_uri(); ?>/img/index/6.jpg')">
                    <div class="services__block__content">
                        <h2 class="services__title">
                            Автомойка самообслуживания в лизинг
                        </h2>
                        <p class="services__block__text">
                            Удорожание от 7,5% в год
                        </p>
                        <p class="services__block__text">
                            Авансовый платёж 25%
                        </p>
                        <p class="services__block__text">
                            Срок лизинга 12-36 месяцев
                        </p>
                    </div>
                    <div class="services__information">
                        <button onclick="return location.href = '<?php home_url("template_url"); ?>/avtomoika-v-lizing-pod-kluch/'" class="services__button_options">Подробнее</button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="differences">
        <div class="differences__container">
            <h2 class="differences__title">
                Чем отличаются мойки самообслуживания <span class="differences__title__blue">Nerta-SW</span>
            </h2>
            <div class="differences__row differences__row_reverse">
            <span class="differences__text">
                <h3>Надёжность и долговечность</h3>
                <p>
                    Не используем дешёвые, низкокачественные комплектующие при
                    производстве постов, аппаратов и строительстве автомоек.
                </p>
                <p>
                    Только проверенные комплектующие, который проходят 100% входной
                    контроль! Заработайте на отсутствии простоев автомойки!
                </p>
            </span>
                <img alt="оборудование для мойки самообслуживания насос высокого давления cats pump | Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/7.jpg">
            </div>

            <div class="differences__row">
                <img alt="автомойка самообслуживания купить оборудование | Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/8.jpg">
                <span class="differences__text">
                <h3>Простота и комфорт при использовании оборудования</h3>
                <p>
                    Продумано до мелочей.  Благодаря собственной сети МСО, мы
                    досконально изучили потребности потребителей, нюансы
                    эксплуатации комплекса.
                </p>
                <p>
                    Приобретая аппараты для МСО или заказывая мойку
                    самообслуживания под «ключ», вы получаете долговечный и
                    простой в использование продукт. В котором собран накопленный
                    опыт и знания за 15-летнюю деятельность компании.
                </p>
            </span>
            </div>

            <div class="differences__row differences__row_reverse">
            <span class="differences__text">
                <h3>Программа мониторинга и управления автомойкой</h3>
                <p>
                    Управляйте бизнесом из любой точки мира с помощью мобильного
                    телефона/ноутбука. Фирменная программа мониторинга и управления,
                    написана под наше оборудование.
                    </p>
                <p>
                    Программное обеспечение постоянно обновляется. Контроль, оперативное
                    управление комплексом, минимизирует издержки!
                </p>
            </span>
                <img alt="Система мониторинга оборудования мойки самообслуживания | Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/9.jpg">
            </div>

            <div class="differences__row">
                <img alt="Ремонт обрудования моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/10.jpg">
                <span class="differences__text">
                <h3>Оригинальная программа лояльности клиентов</h3>
                <p>
                    Благодаря специально разработанной программе лояльности, у вас
                    будет вся необходимая информация о потребителях.
                </p>
                <p>
                    Это позволит максимально эффективно работать с существующими
                    пользователями моечного комплекса, а также проводить
                    маркетинговые акции для привлечения новых. Помогает в низкий
                    сезон. Увеличив трафик - увеличится прибыль!
                </p>
            </span>
            </div>

            <div class="differences__row differences__row_reverse">
            <span class="differences__text">
                <h3>Техническое обслуживание и ремонт</h3>
                <p>
                    Благодаря системе мониторинга, специалисты в режиме online следят
                    за вашим оборудованием. Предупредим вас заранее о намечающимся
                    технических работах.
                </p>
                <p>
                    Напомним, что пора заказывать моющую химию. В случае поломки
                    свяжемся и согласуем дату ремонта. Стоим на страже вашего моечного
                    комплекса 24/7.
                </p>
            </span>
                <img alt="Ремонт обрудования моек самообслуживания | Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/11.jpg">
            </div>

            <div class="differences__row">
                <img alt="Индивидуальный подход к клиенту | Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/12.jpg">
                <span class="differences__text">
                <h3>Индивидуальный подход</h3>
                <p>
                    Собственное производство позволяет собрать комплекс под любые
                    требования и бюджет.
                </p>
                <p>
                    Строим мойки самообслуживания типовые и по индивидуальным
                    проектам.
                </p>
            </span>
            </div>
        </div>
    </section>
    <section class="differences">
        <div class="differences__container differences__container_margin">
            <h2 class="differences__title">
                Автомойка
                самообслуживания -
                выгодный бизнес
            </h2>
            <div class="differences__row differences__row_reverse">
            <span class="differences__text">
                <p>
                    Это высокорентабельное предприятие, с большой степенью
                    автоматизации. Рынок России переживает бум строительства
                    автомоек самообслуживания.
                </p>
                <p>
                    Ежегодно вводится в эксплуатацию более 1000 постов. Становится все
                    более важным удобство, надёжность моечного оборудования.
                </p>
                <p>
                    Средний срок окупаемости моечного комплекса, нашего
                    производства, составляет 12-36 месяцев. Ориентировочный срок
                    строительства 4 постового комплекса 2-4 месяца.
                </p>
                <p>
                    Есть кейс с окупаемостью шестипостовой мойки за 12 месяцев.
                    Скорость возврата инвестиций зависит от: места, качества, количества
                    постов, типа строения. Многопостовые комплексы окупаются быстрее.
                </p>
            </span>
                <img alt="где купить мойку самообслуживания | Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/13.jpg">
            </div>

            <div class="differences__row">
                <img alt="продажа автомойки самообслуживания| Nerta-sw"
                     title="Производство оборудования для моек самообслуживания | Nerta-sw"
                     src="<?= get_template_directory_uri(); ?>/img/index/14.jpg">
                <span class="differences__text differences__text_bottom">
                <p>
                    Благодаря программе удаленного контроля, моечный комплекс не
                    требует большого штата. На один комплекс достаточно 4-х человек
                    при графике работ 24/7.
                </p>
                <p>
                    От персонала не требуется квалификация, все процессы
                    автоматизированы. Это позволяет легко, а главное — быстро
                    масштабировать бизнес.
                </p>
                <p>
                    На Российском рынке множество компаний, продающих оборудование
                    для автомоек. К сожалению, только единицы имеют необходимые
                    знания  для реализации рентабельных проектов. Выбирайте
                    производственные компании, а не торговые представительства.
                </p>
            </span>
            </div>
        </div>
    </section>

    <section class="reasons">
        <div class="reasons__container">
            <h2 class="reasons__title">
                ИНВЕСТИРУЙТЕ В МОЙКУ САМООБСЛУЖИВАНИЯ
            </h2>
            <p class="reasons__description">
                6 причин инвестировать в мойку самообслуживания
            </p>
            <div class="reasons__blocks">
                <div class="reasons__row">
                    <div class="reasons__block">
                        <div class="reasons__number">1</div>
                        <div class="reasons__text">
                            <p class="reasons__text__bold">Рынок на этапе становления</p>
                            <p class="reasons__text__normal">
                                Доля МСО в России составляет 20%, в Европе около
                                70%. На рынке мало крупных игроков. Можно успеть
                                занять место под солнцем.
                            </p>
                        </div>
                    </div>
                    <div class="reasons__block">
                        <div class="reasons__number">2</div>
                        <div class="reasons__text">
                            <p class="reasons__text__bold">Масштабируемость бизнеса</p>
                            <p class="reasons__text__normal">
                                Большинство процессов автоматизировано.
                                Минимальный штат персонала. Отсутствие требований
                                к квалификации персонала.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="reasons__row">
                    <div class="reasons__block">
                        <div class="reasons__number">3</div>
                        <div class="reasons__text">
                            <p class="reasons__text__bold">Технологичность и автоматизация</p>
                            <p class="reasons__text__normal">
                                Комплекс работает в автоматическом режиме.
                                Требуется не более 4 сотрудников на одну мойку.
                            </p>
                            <p class="reasons__text__normal">
                                Удаленный контроль, управление через интернет.
                            </p>
                        </div>
                    </div>
                    <div class="reasons__block">
                        <div class="reasons__number">4</div>
                        <div class="reasons__text">
                            <p class="reasons__text__bold">Высокая доходность МСО</p>
                            <p class="reasons__text__normal">
                                4 постовая – 900 000 ₽/мес.
                            </p>
                            <p class="reasons__text__normal">
                                6 постовая - 1 300 000 ₽/мес.
                            </p>
                            <p class="reasons__text__normal">
                                8 постовая – 1 800 000 ₽/мес.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="reasons__row">
                    <div class="reasons__block">
                        <div class="reasons__number">5</div>
                        <div class="reasons__text">
                            <p class="reasons__text__bold">Система налогообложения</p>
                            <p class="reasons__text__normal">
                                Упрощенка (УСН), 6% от выручки.
                            </p>
                        </div>
                    </div>
                    <div class="reasons__block">
                        <div class="reasons__number">6</div>
                        <div class="reasons__text">
                            <p class="reasons__text__bold">Работа с корпоративным сектором</p>
                            <p class="reasons__text__normal">
                                Заключайте договора с таксомоторными парками,
                                прочими компаниями. Продавайте абонементы
                                на моечные услуги для юридических лиц.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="calculator">
        <div class="calculator__container">
            <h2 class="calculator__title">
                Калькулятор доходности
                автомойки
                самообслуживания
            </h2>
        </div>
        <div class="container_grey">
            <div class="calculator__container">
                <div class="calculator__column">
                    <div class="calculator-block">
                        <p class="calculator-block__title">Количество постов</p>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Количество постов, шт</span>
                                <span class="calculator-block__text_bold"></span>
                            </div>
                            <input type="range" min="1" max="12" class="coll_posts-js calculator-block__range">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text_grey">1</span>
                                <span class="calculator-block__text_grey">12</span>
                            </div>
                        </div>
                    </div>
                    <div class="calculator-block">
                        <p class="calculator-block__title">Средний чек</p>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <p class="calculator-block__text">Стоимость мойки 1 авто</p>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="50" max="350" class="average_check-js calculator-block__range"
                                   step="10">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text_grey">50 ₽</span>
                                <span class="calculator-block__text_grey">350 ₽</span>
                            </div>
                        </div>
                    </div>
                    <div class="calculator-block">
                        <p class="calculator-block__title">Загрузка</p>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Количество автомобилей в сутки</span>
                                <span class="calculator-block__text_bold"></span>
                            </div>
                            <input type="range" min="1" max="100" class="loading-js calculator-block__range">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text_grey">1%</span>
                                <span class="calculator-block__text_grey">100%</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="calculator__column calculator__column__center">
                    <div class="calculator-block">
                        <p class="calculator-block__title">Переменные затраты</p>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Вода и канализация (1м3)</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="1000"
                                   class="water_and_sewerage-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Электроэнергия (1 квт)</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="100" class="electricity-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Моющая химия (% от прибыли)</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>%</p>
                            </span>
                            </div>
                            <input type="range" min="1" max="30" class="detergent_chemistry-js calculator-block__range">
                        </div>
                    </div>
                    <div class="calculator-block">
                        <p class="calculator-block__title">Зимний период</p>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <p class="calculator-block__text">Длительность</p>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>мес.</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="12" class="duration-js calculator-block__range">
                        </div>
                    </div>
                    <div class="calculator-block">
                        <span class="calculator-block__text">Способ отопления</span>
                        <div class="calculator-block__buttons">
                            <button data-type="gas" class="gas-js button-js">ГАЗ</button>
                            <button data-type="diesel" class="diesel-js button-js">ДИЗЕЛЬ</button>
                            <button data-type="pellets" class="pellets-js button-js">ПЕЛЛЕТЫ</button>
                        </div>
                    </div>
                    <div class="calculator-block">
                        <div class="calculator-block__input calculator-block__text_bold">
                            <p class="fuel-title-js">Стоимость </p>
                            <input class="fuel-js input" type="number"/>
                            <p class="fuel__measure">
                                ₽/куб
                            </p>
                        </div>
                    </div>
                </div>
                <div class="calculator__column">
                    <div class="calculator-block">
                        <p class="calculator-block__title">Постоянные затраты, мес</p>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Аренда</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="1000000" step="1000" value="0"
                                   class="rent-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Количество операторов</span>
                                <span class="calculator-block__text_bold"></span>
                            </div>
                            <input type="range" min="1" max="10"
                                   class="number_of_operators-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">ЗП 1-го оператора</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="100000" step="500"
                                   class="operator_salary-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">ЗП управляющего</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="200000" step="500"
                                   class="manager_salary-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">ЗП бухгалтера</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="100000" step="500"
                                   class="accountant_salary-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Налог (% от оборота)</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>%</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="50" class="tax-js calculator-block__range">
                        </div>
                        <div class="calculator-block__content">
                            <div class="calculator-block__wrapper">
                                <span class="calculator-block__text">Прочие затраты</span>
                                <span class="calculator-block__text__container">
                                <p class="calculator-block__text_bold"></p>
                                <p>₽</p>
                            </span>
                            </div>
                            <input type="range" min="0" max="500000" step="1000"
                                   class="other_costs-js calculator-block__range">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="floating_block">
            <div class="floating_content">
                <p>Чистая прибыль за год</p>
                <div class="profit_for_the_year-js big_bold_number"></div>
            </div>
        </div>
        <div class="container_blue">
            <div class="calculator__information">
                <div class="calculator__result">
                    <p>Чистая прибыль за год</p>
                    <div class="profit_for_the_year-js calculator__result__big__number"></div>
                </div>
                <div class="calculator__contacts">
                    <script data-b24-form="click/4/zy99qe" data-skip-moving="true">
                        (function (w, d, u) {
                            var s = d.createElement('script');
                            s.async = true;
                            s.src = u + '?' + (Date.now() / 180000 | 0);
                            var h = d.getElementsByTagName('script')[0];
                            h.parentNode.insertBefore(s, h);
                        })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                    <button class="calculator__feedback__button">Оставить заявку</button>
                    <span class="calculator__feedback__phone-number">
                        +7 (800) 555-25-93
                        <br>
                        +7 (495) 6-777-000
                    </span>
                </div>
            </div>
            <div class="accordion">
                <div class="accordion__container">
                    <div class="accordion__header">
                        <span class="accordion__title">Подробнее</span>
                        <img class="accordion__image" src="<?= get_template_directory_uri(); ?>/img/index/arrow_up.svg"
                             id="image"/>
                    </div>
                    <div class="accordion__content">
                        <div class="accordion__column">
                            <div class="accordion__row accordion__row_margin">
                                <p>
                                    Затраты
                                </p>
                                <p></p>
                            </div>
                            <div class="accordion__row">
                                <p class="accordion__row__text">
                                    Переменные затраты в месяц в летний период
                                </p>
                                <p class="bold_number variable_costs_per_month_summer-js"></p>
                            </div>
                            <div class="accordion__row">
                                <p class="accordion__row__text">
                                    Переменные затраты в месяц в зимний период
                                </p>
                                <p class="bold_number variable_costs_per_month_winter-js"></p>
                            </div>
                            <div class="accordion__row">
                                <p class="accordion__row__text">
                                    Постоянные затраты в месяц
                                </p>
                                <p class="bold_number fixed_costs_per_month-js"></p>
                            </div>
                            <div class="accordion__row accordion__row_margin">
                                <p class="accordion__row__text">
                                    Затраты за год
                                </p>
                                <p class="big_bold_number costs_per_year-js"></p>
                            </div>
                        </div>
                        <div class="accordion__column">
                            <div class="accordion__row accordion__row_margin">
                                <p>
                                    Доходы
                                </p>
                                <p></p>
                            </div>
                            <div class="accordion__row">
                                <p class="accordion__row__text">
                                    Усредненный оборот в месяц
                                </p>
                                <p class="bold_number average_monthly_turnover-js"></p>
                            </div>
                            <div class="accordion__row">
                                <p class="accordion__row__text">
                                    Усредненнная прибыль в месяц
                                </p>
                                <p class="bold_number average_monthly_profit-js"></p>
                            </div>
                            <div class="accordion__row_margin">
                                <div class="accordion__row">
                                    <p class="accordion__row__text">
                                        Оборот за год
                                    </p>
                                    <p class="big_bold_number turnover_per_year-js"></p>
                                </div>
                                <div class="accordion__row">
                                    <p class="accordion__row__text">
                                        Прибыль за год
                                    </p>
                                    <p class="big_bold_number profit_for_the_year-js"></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="contractor">
        <div class="contractor__container">
            <div class="contractor__title">
                <h2>
                    КАК ВЫБРАТЬ ПОДРЯДЧИКА ПРИ ЗАКАЗЕ ОБОРУДОВАНИЯ И СТРОИТЕЛЬСТВЕ АВТОМОЙКИ
                </h2>
            </div>
            <div class="contractor__blocks">
                <div class="contractor__column">
                    <div class="contractor__block">
                        <div class="contractor__number">1</div>
                        <div class="contractor__text">
                            <p class="contractor__text__title">
                                Изучите портфолио с выполненными работами
                                компанией-подрядчиком.
                            </p>
                            <p class="contractor__text__inf">
                                Посетите несколько объектов, посмотрите как работают
                                автомойки, пообщайтесь с сотрудниками, пользователями.
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">2</div>
                        <div class="contractor__text">
                            <p class="contractor__text__title">
                                Обратите внимание на моющие аппараты.
                            </p>
                            <p class="contractor__text__inf">
                                Запросите спецификацию на комплектующие, они
                                должны быть от известных производителей.
                                Не рекомендуем использовать низкокачественные,
                                дешевые компоненты.
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">3</div>
                        <div class="contractor__text">
                            <p class="contractor__text__title">
                                По возможности посетите производство,
                                где собирается оборудование для МСО.
                            </p>
                            <p class="contractor__text__inf">
                                Только если присутствует культура производства
                                получаются качественные посты.
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">4</div>
                        <div class="contractor__text">
                            <p class="contractor__text__title">
                                Обратите внимание на стоимость.
                            </p>
                            <p class="contractor__text__inf">
                                Остерегайтесь сильно низких цен. Бесплатный сыр, как
                                говорится, только в мышеловке. При покупке дешевого
                                низкокачественного оборудования вы получите проблемы
                                при эксплуатации, простои постов, потери прибыли.
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">5</div>
                        <div class="contractor__text">
                            <p class="contractor__text__title">
                                Изучите систему дистанционного мониторинга и управления.
                            </p>
                            <p class="contractor__text__inf">
                                Без функциональной системы вы не сможете
                                контролировать и управлять процессами, что
                                приведёт к увеличению затрат, простою оборудования,
                                а возможно, и воровству персонала.
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">6</div>
                        <div class="contractor__text">
                            <p class="contractor__text__title">
                                Обратите внимание на программу лояльности
                                клиентов автомойки самообслуживания.
                            </p>
                            <p class="contractor__text__inf">
                                Вы должны владеть полной информацией о транзакциях,
                                времени нахождения на посту, частоте использования.
                                Эти данные необходимы для проведения маркетинговых
                                кампаний, для привлечения клиентов.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="contractor__column__image">
                    <img alt="Оборудование для мойки самообслуживания цены| Nerta-sw"
                         title="Производство оборудования для моек самообслуживания | Nerta-sw"
                         src="<?= get_template_directory_uri(); ?>/img/index/15.jpg">
                    <img alt="насос высокого давления annovi reverberi| Nerta-sw"
                         title="Производство оборудования для моек самообслуживания | Nerta-sw"
                         src="<?= get_template_directory_uri(); ?>/img/index/16.jpg" class="image-none">
                </div>
            </div>
        </div>
    </section>

    <section class="feedback_grey">
        <div class="feedback__container">
            <div class="feedback__block">
                <p class="feedback__title">
                    Остались вопросы?<br>
                    Звоните или оставляйте<br>
                    заявку на сайте.</p>
                <p class="feedback__title feedback__title_blue">
                    Мы ответим на все <br>
                    интересующие вас <br>
                    вопросы!</p>
            </div>
            <div class="feedback__form">
                <script data-b24-form="inline/2/bvcqvg" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_2.js'); </script>
                <!--                --><?php //echo do_shortcode('[contact-form-7 id="14156" title="Feedback index"]'); ?>
            </div>
        </div>
    </section>

    <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
    <script src="<?php bloginfo("template_url"); ?>/js/cost.js"></script>
    <script src="<?php bloginfo("template_url"); ?>/js/lib/jquery.waypoints.min.js"></script>
    <script src="<?php bloginfo("template_url"); ?>/js/equipment.js"></script>

<?php get_footer(); ?>