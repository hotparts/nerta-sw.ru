<?php
/**
 * Template Name: equipment light
 */
get_header(); ?>
    <link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/light.css">

    <section class="chief"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_light/main.jpg')">
        <div class="chief__container">
            <div class="breadcrumbs">
                <?php the_breadcrumb() ?>
            </div>
            <h1 class="chief__title">
                Комплектация <br>
                light
            </h1>
            <div class="chief__description">
                <p class="chief__description__information">
                    Полный комплект моечного оборудования
                </p>
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="chief__description__button">Оформить заявку</button>
            </div>
        </div>
    </section>

    <section class="small__advantages">
        <div class="small__advantages__container">
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon1.png" alt="">
                <p class="small__advantages__block__bold_text">
                    Выгодная подача пены под низким давлением
                </p>
                <p class="small__advantages__block__normal_text">
                    Мыть автомобиль комфортнее и проще. Данная технология
                    требует меньше внимания со стороны персонала мойки и снижает затраты на моющую химию
                </p>
            </div>
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon2.png" alt="">
                <p class="small__advantages__block__bold_text">
                    Полный комплект моечного оборудования
                </p>
                <p class="small__advantages__block__normal_text">
                    Используются комплектующие начального уровня для запуска мойки (Евросоюз, Китай, Россия)
                </p>
            </div>
            <div class="small__advantages__block">
                <img class="small__advantages__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon3.png" alt="">
                <p class="small__advantages__block__bold_text">
                    LIGHT – для тех, кто экономит с умом
                </p>
                <p class="small__advantages__block__normal_text">
                    Гарантия на оборудование – 1 год
                </p>
            </div>
        </div>
    </section>

    <section class="equipment__description">
        <div class="equipment__description__container">
            <div class="equipment__description__title">
                <h2>
                    Панель управления из нержавеющей стали
                </h2>
            </div>

            <div class="equipment__description__blocks">
                <div class="equipment__description__block1">
                    <img class="block__image_2" src="<?= get_template_directory_uri(); ?>/img/equipment_light/5.jpg"
                         title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                         alt="Панель управления light оборудование  мойки самообслуживания">

                    <div class="equipment__description__block1__information">
                        <div class="equipment__description__block1__column">
                            <p class="equipment__description__subtitle">Моющие программы:</p>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon4.png" alt="">
                                <p>пена</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon5.png" alt="">
                                <p>вода+пена</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon6.png" alt="">
                                <p>теплая вода</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon7.png" alt="">
                                <p>холодная вода</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon8.png" alt="">
                                <p>воск</p>
                            </div>
                            <div class="equipment__description__block1__row">
                                <img class="inf__block__image"
                                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/icon9.png" alt="">
                                <p>стоп/пауза</p>
                            </div>
                        </div>
                        <button class="equipment__description__button">Рассчитать стоимость</button>
                    </div>
                </div>
                <div class="equipment__description__block2">
                    <p class="equipment__description__block2__subtitle">
                        Внешняя панель управления мойкой из нержавеющей стали Aisi 304
                        с сенсорными клавишами с подсветкой
                    </p>
                    <ul>
                        <li>Моющие программы: пена, Вода+Пена (шампунь), вода под давлением, воск, стоп/пауза</li>
                        <li>Возможно подключить дополнительные программы:
                            воздух, осмос, мойка дисков, средство от насекомых, щетка
                        </li>
                        <li>Купюроприемник ICT (Тайвань)
                        </li>
                        <li>Монетоприемник EU-2 (Китай)
                        </li>
                        <li>Пеноизоляция</li>
                        <li>Теплоизоляция</li>
                        <li>Климат-контроль</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <section class="power__equipment">
        <div class="power__equipment__container">
            <div class="power__equipment__title">
                <h2>
                    Силовое оборудование
                </h2>
            </div>
            <div class="power__equipment__block__image">
                <img src="<?= get_template_directory_uri(); ?>/img/equipment_light/1.jpg"
                     title="Рама с силовым оборудованием  от компании Nerta-SW"
                     alt="Рама с силовым оборудованием для моек самообслуживания|Nerta-SW">
                <div class="power__equipment__block__image__title">
                    <p>Рама из нержавеющей стали Aisi 304</p>
                </div>
            </div>
            <div class="power__equipment__block__list">
                <div class="power__equipment__block__1">
                    <ul>
                        <li>
                            Рама из нержавеющей стали Aisi 204
                        </li>
                        <li>
                            Промышленный насос Hawk (Италия) напор воды от 15 до 200 бар
                        </li>
                        <li>
                            Электродвигатель Mazzoni мощностью 4 кВт (Италия)
                        </li>
                        <li>
                            Система против замерзания ANTI FROST
                        </li>
                        <li>
                            Воздушный компрессор Remeza (Италия-Белоруссия)
                        </li>
                        <li>
                            Насосная станция Wilo на 220В (Германия)
                        </li>
                        <li>
                            Система дозировки жидких химических реагентов Etatron (Италия)
                        </li>
                    </ul>
                </div>
                <div class="power__equipment__block__1">
                    <ul>
                        <li>
                            Магистрали подачи воды с шаровым кранами
                        </li>
                        <li>
                            Электромагнитный клапан низкого давления Danfoss (Германия)
                        </li>
                        <li>
                            Электромагнитный клапан  высокого давления NRT (Китай)
                        </li>
                        <li>
                            Электрический шкаф (Россия) - на каждый пост индивидуальный
                        </li>
                        <li>
                            Частотный преобразователь Prostar (Китай)
                        </li>
                        <li>
                            Пистолеты высокого давления ST 2600 R+M (Германия)
                        </li>
                        <li>
                            Комплект рукавов высокого давления (Тайвань)
                        </li>
                    </ul>
                </div>
            </div>

        </div>
    </section>

    <section class="dop__description dop__description__blue">
        <div class="dop__description__container dop__description__container__padding">
            <div class="dop__description__block__img ">
                <img class="dop__description__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/2.jpg"
                     title="Панель управления из нержавейки для моек самообслуживания от производителя Nerta-SW"
                     alt="Панель управления из нержавейки для моек самообслуживания|Nerta-SW">
            </div>
            <div class="dop__description__block ">
                <ul>
                    <li>
                        Система удаленного контроля и управления постом самообслуживания
                    </li>
                    <li>
                        Пистолеты высокого давления ST 2600 R+M (Германия)
                    </li>
                    <li>
                        Пистолеты низкого давления R+M (Германия)
                    </li>
                    <li>
                        Держатели ковриков из нержавеющей стали
                    </li>
                    <li>
                        Держатель для пистолета из нержавеющей стали
                    </li>
                    <li>
                        Поворотная консоль 1800 мм из нержавеющей стали
                    </li>
                    <li>
                        Накопительная емкость для воды на 2000 л
                    </li>
                </ul>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Цена 1 поста
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                от 8 000 €
                            </div>
                            <div class="block__cost__number3">
                                800 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Монтаж, в независимости от количества постов
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                2 000 €
                            </div>
                            <div class="block__cost__number3">
                                200 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Slider main container -->
    <section class="swiper__content swiper__content__grey">
        <div class="swiper">
            <div class="swiper-button-container">
                <div class="swiper-button-prev swiper-button-prev1"></div>
                <div class="swiper-button-next swiper-button-next1"></div>
            </div>
            <div class="swiper-container swiper-1">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/s1.png"
                             title="Насос высокого давления (Light) для моек самообслуживания|Nerta-SW"
                             alt="Насос высокого давления Hawk для моек самообслуживания|Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Насос Hawk
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/s2.png"
                             title="Моющий пистолет для моек самообслуживания Nerta-SW"
                             alt="Моющий пистолет R+M для моек самообслуживания|Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Моечный пистолет R+M (Германия)
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/s3.png"
                             title="Электромагнитный клапан для моек самообслуживания |Nerta-SW"
                             alt="Электромагнитный клапан Danfoss  для мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Электромагнитный клапан Danfoss Ravel
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/s4.png"
                             title=" Купюроприёмник для моек самообслуживания |Nerta-SW"
                             alt="Купюроприёмник ICTдля мойки самообслуживания |Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Купюроприемник ICT
                        </p>
                    </div>
                    <div class="swiper-slide swiper-slide__margin_top swiper-slide_center">
                        <img class="swiper-slide__img"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/s5.png"
                             title="Насосная станция (Light) для моек самообслуживания|Nerta-SW"
                             alt="Насосная станция Wilo для моек самообслуживания|Nerta-SW">
                        <p class="swiper-slide__image__options">
                            Насосная станция Wilo
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="dop__description">
        <div class="dop__description__container dop__description__container__margin dop__description__container__reverse">
            <div class="dop__description__block__img ">
                <img class="dop__description__image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/3.jpg"
                     title="Склад компании производителя оборудования для моек самообслуживания Nerta-SW"
                     alt="Склад комплектующих для сборки оборудования  моек самообслуживания|Nerta-SW">
            </div>
            <div class="dop__description__block ">
                <div class="dop__description__block__title">
                    <p>Дополнительные опции</p>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена системы дозировки на Seco AKL
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                100 €
                            </div>
                            <div class="block__cost__number3">
                                10 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена частотного преобразователя на Schneider Electric
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                150 €
                            </div>
                            <div class="block__cost__number3">
                                15 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бесконтактная оплата с помощью  пластиковых карт лояльности
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                200 €
                            </div>
                            <div class="block__cost__number3">
                                20 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бесконтактная оплата с помощью банковских карт и смартфонов (Apple/Samsung/ GooglePay)
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                400 €
                            </div>
                            <div class="block__cost__number3">
                                40 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Замена насоса Wilo c 220Вт на 380Вт
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                400 €
                            </div>
                            <div class="block__cost__number3">
                                40 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
                <div class="block__cost">
                    <div class="block__cost__container">
                        <div class="block__cost__text">
                            Бойлер косвенного нагрева на 300 литров
                        </div>
                        <div class="block__cost__number">
                            <div class="block__cost__number1">
                                500 €
                            </div>
                            <div class="block__cost__number3">
                                50 000 ₽
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!-- Slider main container -->
    <section class="swiper__content swiper__content__display">
        <div class="swiper">
            <div class="swiper__content__title">
                <h2>
                    Пример цветового оформления панелей
                </h2>
            </div>
            <div class="swiper-button-container">
                <div class="swiper-button-prev swiper-button-prev2"></div>
                <div class="swiper-button-next swiper-button-next2"></div>
            </div>
            <div class="swiper-container swiper-2">
                <div class="swiper-wrapper">
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/5.jpg"
                             title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления light для оборудования  мойки самообслуживания –цвет красный">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/4.jpg"
                             title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления light для оборудования  мойки самообслуживания –цвет красный">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/7.jpg"
                             title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления light для оборудования  мойки самообслуживания –цвет красный">
                    </div>
                    <div class="swiper-slide swiper-slide__margin_bottom swiper-slide_center">
                        <img class="swiper-slide__color_image"
                             src="<?= get_template_directory_uri(); ?>/img/equipment_light/6.jpg"
                             title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                             alt="Панель управления light для оборудования  мойки самообслуживания –цвет красный">
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="color__variations">
        <div class="color__variations__container">
            <div class="color__variations__title">
                <h2>
                    Пример цветового оформления панелей
                </h2>
            </div>
            <div class="color__variations__content">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/5.jpg"
                     title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления light для оборудования  мойки самообслуживания –цвет зеленый">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/4.jpg"
                     title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления light для оборудования  мойки самообслуживания –цвет синий">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/7.jpg"
                     title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления light для оборудования  мойки самообслуживания –цвет красный">
                <img class="swiper-slide__color_image"
                     src="<?= get_template_directory_uri(); ?>/img/equipment_light/6.jpg"
                     title="Панель управления light от производителя оборудования для моек самообслуживания  NERTA-SW"
                     alt="Панель управления light для оборудования  мойки самообслуживания –цвет пурпурный">
            </div>
        </div>
    </section>

    <section class="feedback"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/equipment_light/feed.jpg')">
        <div class="feedback__container">
            <div class="feedback__title">
                Звоните, инженеры компании помогут с выбором оборудования
            </div>
            <p>
                Сравните характеристики комплектаций, рассчитайте стоимость оборудования
            </p>
            <div class="feedback__phone_number">
                8 (800) 555-25-93
            </div>
            <div class="feedback__button">
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="feedback__button__1">Оформить заявку</button>
                <button class="feedback__button__2">Калькулятор</button>
            </div>
        </div>
    </section>
    <!-- Slider main container -->
    <section class="options__mini">
        <div class="options__mini__container">
            <div class="options__mini__subtitle">
                <h2>
                    Посмотреть другие комплектации
                </h2>
            </div>
            <div class="options__mini__row">

                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_start/'"
                     class="options__mini__block color_1 shadow_box">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            START
                        </p>
                        <p class="options__mini__title">
                            Для тех кто собирает мойку самостоятельно, на своих компонентах.
                        </p>
                        <ul class="options__mini__list">
                            <li>Панель управления;</li>
                            <li>Электротехнический шкаф;</li>
                            <li>Дозатор моющей химии.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        2 400 €
                                    </div>
                                    <div class="block__cost__number2">
                                        240 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_basic/'"
                     class="options__mini__block color_2">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            BASE
                        </p>
                        <p class="options__mini__title">
                            Для тех кто любит дёшево.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для организации мойки;</li>
                            <li>Пена наносится под высоким давлением;</li>
                            <li>Комплектующие начального уровня (Евросоюз, Китай);</li>
                            <li>Гарантия 1 год.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        5 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        500 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_optima/'"
                     class="options__mini__block color_4">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            OPTIMA
                        </p>
                        <p class="options__mini__title">
                            Оптимальное соотношение цена/качества.
                            Для тех кто умеет считать.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект автомоек самообслуживания;</li>
                            <li>Пена наносится поднизким давлением;</li>
                            <li>Комплектующие среднего уровня (Евросоюз).</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        10 500 €
                                    </div>
                                    <div class="block__cost__number2">
                                        1 050 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div onclick="return location.href = '<?php home_url("template_url"); ?>/komplektaciya_maxi/'"
                     class="options__mini__block color_5">
                    <div class="options__mini__block__content">
                        <p class="options__mini__title__big">
                            MAXI
                        </p>
                        <p class="options__mini__title">
                            Самоё надежное и неприхотливое.
                            Для тех кому лучше идеально, чем дёшево.
                        </p>
                        <ul class="options__mini__list">
                            <li>Полный комплект для мойки самообслуживания;</li>
                            <li>Пена наносится поднизким давлением;</li>
                            <li>Типовые комплектующие (Япония, Канада, Евросоюз);</li>
                            <li>Монтаж;</li>
                            <li>Гарантия 2 года.</li>
                        </ul>
                    </div>
                    <div class="options__mini__block__cost">
                        <div class="block__cost">
                            <div class="block__cost__container">
                                <div class="block__cost__text">
                                    Цена за 1 пост
                                </div>
                                <div class="block__cost__number">
                                    <div class="block__cost__number1">
                                        13 000 €
                                    </div>
                                    <div class="block__cost__number2">
                                        1 300 000 ₽
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="<?php bloginfo("template_url"); ?>/js/cost.js"></script>
    <script src="<?php bloginfo("template_url"); ?>/js/gallery.js"></script>


<?php get_footer(); ?>