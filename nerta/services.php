<div class="service-gallery">
    <div class="service-gallery-item">
        <div class="service-gallery-title">Проектирование</div>
    </div>
    <div class="service-gallery-item">
        <div class="service-gallery-title">Строительство</div>
    </div>
    <div class="service-gallery-item">
        <div class="service-gallery-title">Изготовление и установка металлоконструкции</div>
    </div>
    <div class="service-gallery-item">
        <div class="service-gallery-title">Производство и монтаж оборудования</div>
    </div>
    <div class="service-gallery-item">
        <div class="service-gallery-title">Рекламная поддержка и помощь в продвижении</div>
    </div>
</div>