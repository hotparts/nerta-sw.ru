"use strict";

var Tooltip = (function () {
    var Tooltip = function (container) {
        this.container = container;
    };

    Tooltip.prototype.show = function (class_name, info) {
        this.container.append('div')
            .attr('class', class_name)
            .style('left', (d3.event.pageX - 100) + 'px')
            .style('top', (d3.event.pageY - 80) + 'px')
            .html(info);
    };

    Tooltip.prototype.hide = function (class_name) {
        this.container.select('.' + class_name).remove();
    };

    Tooltip.prototype.move = function (class_name) {
        this.container.select('.' + class_name)
            .style('left', (d3.event.pageX - 100) + 'px')
            .style('top', (d3.event.pageY - 80) + 'px');
    };

    return Tooltip;
})();