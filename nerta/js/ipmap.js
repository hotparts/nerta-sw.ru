"use strict";
var data_city = [
    /*{
        id: 21,
        name: 'Дмитров',
        lat: 56.3427702,
        lon: 37.5288416
    },*/
    {
        id: 2,
        name: 'Липецк',
        lat: 52.61022,
        lon: 39.594719
    },
    {
        id: 3,
        name: 'Калуга',
        lat: 54.507014,
        lon: 36.252277
    },
    {
        id: 4,
        name: 'Казань',
        lat: 55.795793,
        lon: 49.106585
    },
    {
        id: 5,
        name: 'Псков',
        lat: 57.819365,
        lon: 28.331786
    },
    {
        id: 6,
        name: 'Ульяновск',
        lat: 54.317002,
        lon: 48.402243
    },
    {
        id: 7,
        name: 'Нальчик',
        lat: 43.4950,
        lon: 43.6045
    },
    {
        id: 8,
        name: 'Первоуральск',
        lat: 56.908099,
        lon: 59.942935
    },
    {
        id: 10,
        name: 'Березовский',
        lat: 56.9089,
        lon: 60.7950
    },
    {
        id: 9,
        name: 'Екатеринбург',
        lat: 56.838002,
        lon: 60.597295
    },
    {
        id: 11,
        name: 'Тутаев',
        lat: 57.8676,
        lon: 39.5333
    },
    {
        id: 12,
        name: 'Печоры',
        lat: 57.8146,
        lon: 27.6269
    },
    {
        id: 13,
        name: 'Шлиссельбург',
        lat: 59.9353,
        lon: 31.0290
    },
    {
        id: 14,
        name: 'Саров',
        lat: 54.9343,
        lon: 43.3253
    },
    {
        id: 15,
        name: 'Нижний Новгород',
        lat: 56.323902,
        lon: 44.002267
    },
    {
        id: 16,
        name: 'Набережные Челны',
        lat: 55.743553,
        lon: 52.39582
    },
    {
        id: 17,
        name: 'Уфа',
        lat: 54.734768,
        lon: 55.957838
    },
    {
        id: 18,
        name: 'Учалы',
        lat: 54.3090,
        lon: 59.4089
    },
    {
        id: 19,
        name: 'Евпатория',
        lat: 45.1905,
        lon: 33.3669
    },
    {
        id: 20,
        name: 'Рассказово',
        lat: 52.6587,
        lon: 41.8815
    },
    {
        id: 21,
        name: 'Москва',
        lat: 55.755773,
        lon: 37.617761
    },
        {
        id: 22,
        name: 'Каменск-Уральский',
        lat: 56.248938,
        lon: 61.551343 
    },
            {
        id: 23,
        name: 'Владивосток',
        lat: 43.0620000,
        lon: 131.522400 
    },
                {
        id: 24,
        name: 'Козельск',
        lat: 54.216260,
        lon: 35.469033
    },
                  {
        id: 25,
        name: 'Кирсанов',
        lat: 52.390388,
        lon: 42.437198
    },
                    {
        id: 26,
        name: 'Кстово',
        lat: 56.085000,
        lon: 44.115200
    },
                        {
        id: 27,
        name: 'Нижняя Тура',
        lat: 58.378616,
        lon: 59.511258
    },
                            {
        id: 28,
        name: 'Новошахтинск',
        lat: 47.454643,
        lon: 39.561858
    },
                                {
        id: 29,
        name: 'Пермь',
        lat: 58.061550,
        lon: 56.140517
    },
                                    {
        id: 30,
        name: 'Рязань',
        lat: 54.377501,
        lon: 39.440957
    },
                                        {
        id: 31,
        name: 'Сосенский',
        lat: 54.033200,
        lon: 35.574400
    },
                                            {
        id: 32,
        name: 'Тверь',
        lat: 56.515767,
        lon: 35.547138
    },
                                                {
        id: 33,
        name: 'Челябинск',
        lat: 55.961700,
        lon: 61.240514
    },
                                                    {
        id: 34,
        name: 'Хабаровск',
        lat: 48.283550,
        lon: 135.34639
    },
                                                        {
        id: 35,
        name: 'Ясногорск',
        lat: 54.287733,
        lon: 37.413813
    },

];

var IPMap = (function () {
    d3.selection.prototype.moveToFront = function() {
        return this.each(function(){
            this.parentNode.appendChild(this);
        });
    };

    var IPMap = function (params) {
        this.selector = params.selector || 'map';
        this.topojsonPath = params.topojsonPath || '';
    };

    IPMap.prototype.drawMap = function () {
        this.init();
        this.loadMap(this.draw);
    };

    IPMap.prototype.init = function () {
        var _this = this;
        _this.map = d3.select('#' + this.selector);
        _this.tooltip = new Tooltip(this.map);

        _this.svg = this.map.append('svg')
            .attr('width', '100%')
            .attr('height', '100%');

        var mapRect = _this.map.node().getBoundingClientRect();

        _this.width = mapRect.width;
        _this.height = mapRect.height;

        _this.scale = 500;
        if (Math.round(_this.height) > 400) this.scale = 700;
        if (Math.round(_this.height) > 500) this.scale = 860;

        this.projection = d3.geoAlbers()
            .rotate([-105, 0])
            .center([-10, 65])
            .parallels([52, 64])
            .scale(_this.scale)
            .translate([_this.width / 2, _this.height / 2]);

        this.path = d3.geoPath()
            .projection(this.projection);
    };

    IPMap.prototype.loadMap = function (callback) {
        var _this = this;

        d3.json(_this.topojsonPath, function (error, topology) {
            callback && callback.call(_this, topology);
        });
    };

    IPMap.prototype.draw = function (topology) {
        var _this = this;
        _this.regions = topojson.feature(topology, topology.objects.collection).features;
        this.group = this.svg.append('g');
        this.group.selectAll('path').data(_this.regions).enter().append('path')
            .attr('class', 'country')
            .style('fill','#0d0e4c')
            .style('stroke','#0d0e4c')
            .attr('d', this.path);

        var circle = '<g><circle r="9" fill="#fff"></circle><circle r="8" fill="#0d0e4c"></circle><circle r="6" fill="#fff"></circle></g>';

        var city = this.svg.selectAll("g.city")
            .data(data_city)
            .enter()
            .append("g")
            .attr("class", "city")
            .html(circle)
            .attr("transform", function(d) { return "translate(" + _this.projection([d.lon, d.lat]) + ")"; })
            .on('mouseover', function (d) {
                d3.select(this).moveToFront();
                _this.tooltip.show('tooltip dot_' + d.id, '<span class="tooltip-text">' + d.name + '</span>');
            })
            .on('mouseout', function (d) {
                _this.tooltip.hide('dot_' + d.id);
            })
            .on('mousemove', function (d) {
                _this.tooltip.move('dot_' + d.id);
            });
       };

    return IPMap;
})();

