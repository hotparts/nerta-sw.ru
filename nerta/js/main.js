
var notime = document.querySelectorAll('.notime__btn');
if (notime.length !== 0) {
    notime.forEach(element => {
        element.addEventListener('click', function () {
            document.querySelector('.notime__form').classList.toggle('is-active');
        })
    });
}