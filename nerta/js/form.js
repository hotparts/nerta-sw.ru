
document.addEventListener("DOMContentLoaded", function () {

    /********************************************
     * ajax request
     ********************************************/

    function ajax(params, btn, response){

        let xhr = new XMLHttpRequest();
        xhr.open('POST', params.url)
        xhr.send(params.data)

        xhr.onload = function() {
            response(xhr.status, xhr.response)
        };

        xhr.onerror = function() {
            console.error(`Ошибка соединения`);
        };

        xhr.onreadystatechange = function() {



            if (xhr.readyState == 3) {
                btn.classList.add('btn-loading')
            }

            if (xhr.readyState == 4) {
                setTimeout(function(){btn.classList.remove('btn-loading')},300)

            }

        };
    }

    /* Вывод сообщений */
    function state(type_err, message){

        if(!document.querySelector('#status')){
            var elem = document.createElement("div");
            elem.setAttribute('id', 'status');
            document.body.append(elem)
        }

        document.querySelector('#status').classList.remove('complete')
        document.querySelector('#status').classList.remove('error')

        if(type_err){
            document.querySelector('#status').classList.add('complete')
            document.querySelector('#status').innerHTML = message

        }else{
            document.querySelector('#status').classList.add('error')
            document.querySelector('#status').innerHTML = message
        }

        setTimeout(function(){
            document.querySelector('#status').classList.remove('complete')
            document.querySelector('#status').classList.remove('error')
        }, 8000);


    };

    document.querySelectorAll('[data-form]').forEach(function(item){
        item.addEventListener('submit', function(event){
            event.preventDefault()

            let _this = this;
            let formData = new FormData(this);
            let btn = this.querySelector('.btn')

            ajax({ url: this.getAttribute('action'), data: formData }, btn, function(status, response){

                let dataResponse = JSON.parse(response)

                switch(dataResponse['status']){

                    case 'mail_sent':
                        _this.reset()
                        state(true, dataResponse['message'])

                        _this.querySelectorAll('.err').forEach(function(input){
                            input.classList.remove('err')
                        })

                        break;

                    case 'validation_failed':
                        state(false, dataResponse['message'])

                        _this.querySelectorAll('.err').forEach(function(input){
                            input.classList.remove('err')
                        })

                        dataResponse['invalid_fields'].forEach(function(item){
                            let id_field = item['error_id'].replace('-ve-', '')
                            console.log(id_field)
                            _this.querySelector('[name="'+id_field+'"]').classList.add('err')
                        })

                        break;

                    default: state(false, 'Вы должны подтвердить своё согласие на обработку персональных данных.')

                }

                console.log(dataResponse)
            })

        })
    })

    var selects = document.querySelectorAll('.projects__headSort select, .articles__headSort select');
    for (var i = 0; i<selects.length; i++){
        selects[i].addEventListener('change', function(){
            var form = this.closest('form');
            form.submit();
        })
    }





}); //DOMContentLoaded




// Аякс запрос для показать больше картинок
function query(data, elem, callback) {
    jQuery.ajax({
        type: "POST",
        url: '/wp-admin/admin-ajax.php',
        data: data,

        beforeSend: function () {

        },
        success: function (server_response) {
            var data = eval("(" + server_response + ")");
            callback({
                data: data,
            })
        },
        error: function (err, texterr) {

        },
    });
}






//Отправка формы

function status(type_err, message) {
    jQuery("#status").removeClass();
    if (!message) {
        return false;
    }
    if (type_err) {
        jQuery("#status").removeClass().text(message).addClass("complete");
    } else {
        jQuery("#status").removeClass().text(message).addClass("error");
    }

    setTimeout(function () {
        jQuery("#status").removeClass();
    }, 5000);
}


function query_send(data, elem, callback) {
    jQuery.ajax({
        type: "POST",
        url: '/wp-admin/admin-ajax.php',
        data: data,

        beforeSend: function () {
            status(true, "Отправка...");
        },
        success: function (server_response) {
            var data = eval("(" + server_response + ")");
            if (data.status) {
                callback({
                    status: true,
                    data: data,
                });
            } else {
                status(false, data.msg);
                callback({
                    status: false,
                });
            }
        },
        error: function (err, texterr) {
            status(false, "error_ajax" + texterr);
        },
    });
}

window.addEventListener('load', function () {
    //check projects exists
    jQuery.ajax({
        url: "/wp-admin/admin-ajax.php",
        data: {
            action: "loadproj",
            count: 1,
        },
        type: "POST",
        success: function (data) {
            if (data) {
                var dt = JSON.parse(data);
                if (dt.count > 0) {
                    jQuery(".projects__items--more").addClass('is-visible');
                }
            }
        },
    });
    //check projects exists
    jQuery.ajax({
        url: "/wp-admin/admin-ajax.php",
        data: {
            action: "loadart",
            count: 1,
        },
        type: "POST",
        success: function (data) {
            if (data) {
                var dt = JSON.parse(data);
                if (dt.count > 0) {
                    jQuery(".articles__items--more").addClass('is-visible');
                }
            }
        },
    });
})


jQuery(".projects__items--more").on("click", function (event) {
    var elem =jQuery(this);
    var parent = jQuery(".projects__items");
    var count = parent.find(".projects__item").length;

    var data = {
        action: "loadproj",
        count: count,
    };

    jQuery.ajax({
        // you can also use $.post here
        url: "/wp-admin/admin-ajax.php", // AJAX handler
        data: data,
        type: "POST",
        beforeSend: function (xhr) {
            elem.text("Загрузка...");
        },
        success: function (data) {
            if (data) {
                var dt = JSON.parse(data);
                if (dt.count < 4) {
                    elem.remove();
                }

                elem.text("Показать еще");
                parent.append(dt.text);

            } else {
                elem.remove();
            }
        },
    });
});


jQuery(".articles__items--more").on("click", function (event) {
    var elem =jQuery(this);
    var parent = jQuery(".articles__items");
    var count = parent.find(".articles__item").length;

    var data = {
        action: "loadart",
        count: count,
    };

    jQuery.ajax({
        // you can also use $.post here
        url: "/wp-admin/admin-ajax.php", // AJAX handler
        data: data,
        type: "POST",
        beforeSend: function (xhr) {
            elem.text("Загрузка...");
        },
        success: function (data) {
            if (data) {
                var dt = JSON.parse(data);
                if (dt.count < 4) {
                    elem.remove();
                }

                elem.text("Показать еще");
                parent.append(dt.text);

            } else {
                elem.remove();
            }
        },
    });
});

jQuery(document).on("submit", ".subscribe", function (event) {
    event.preventDefault();

    var elem = jQuery(this);




    var formData = jQuery(this).serialize();
    var data = {
        action: "form",
        formData: formData,
    };



    query_send(data, elem, function (callback) {
        if (callback.status) {
            status(
                true,
                "Ссылка на статью придет вам на почту!"
            );

            setTimeout(function () {
                elem[0].reset();
            }, 2000);
        }
    });



});

window.addEventListener("load", function () {
    var maska = document.createElement("script");
    maska.src = "/wp-content/themes/nerta/js/maska.js";
    maska.onload = function () {
        var mask = Maska.create('input[type="tel"]', {
        mask: '+7 (###) ###-##-##'
    });
    }
    document.body.appendChild(maska);

});

function scrollTop() {
    if (window.pageYOffset > 0) {
        this.document.querySelector('.new-footer__scrollTop').style.display = "flex";
    }
    else {
        this.document.querySelector('.new-footer__scrollTop').style.display = "none";
    }
}

scrollTop();

window.addEventListener('scroll', function () {
    scrollTop();
})
/*

var more = document.querySelector('.projects__items--more');
more.addEventListener('submit', function (e) {
    e.preventDefault();
    var request = new XMLHttpRequest();
    var data = new FormData(this);
    request.addEventListener('readystatechange', function (data) {
        if (request.readyState == 4) {
            data = JSON.parse(request.responseText);
        }
    });
    request.open("GET",more.action);
    request.send(data);
})

*/
