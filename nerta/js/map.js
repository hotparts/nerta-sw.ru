ymaps.ready(function(){
    var nertaMap = new ymaps.Map("map_contacts", {
        center: [55.723800, 37.701349],
        zoom: 17,
        controls: []
    });
    var nertaObject = new ymaps.Placemark(
        [55.723800, 37.701349],
        {
            iconCaption: 'Остаповский проезд, 3 стр. 5'
        },{
            preset: 'islands#blueCircleDotIconWithCaption'
        }
    );
    nertaMap.geoObjects.add(nertaObject);
    nertaMap.controls.add(
        new ymaps.control.ZoomControl()
    );
    nertaMap.behaviors.disable('scrollZoom');
});