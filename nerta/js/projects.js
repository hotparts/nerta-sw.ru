/* eslint-disable linebreak-style */
window.addEventListener("load", function () {
  var slider = document.querySelectorAll(".splide");
  if (slider.length !== 0) {
    var glide = document.createElement("script");
    glide.src = "/wp-content/themes/nerta/js/splide.min.js";
    glide.onload = initSliders;
    document.body.appendChild(glide);
  }

  var maps = document.createElement("script");
  maps.src = "//api-maps.yandex.ru/2.1/?apikey=17fcb403-dca9-44b3-87ea-497e17b51d13&lang=ru_RU&load=Map,Placemark";
  maps.onload = initMap;
  document.body.appendChild(maps);

});

function initMap() {
  var map = document.querySelector('.projects-detail__mainslider #map');
  ymaps.ready(function () {
    var myMap = new ymaps.Map("map", {
      center: [map.getAttribute('data-x'),map.getAttribute('data-y')],
      zoom: 16,
      controls: [],
    });
    var myPlacemark = new ymaps.Placemark([map.getAttribute('data-x'),map.getAttribute('data-y')], {}, {
      iconLayout: "default#image",
      iconImageHref: "/wp-content/themes/nerta/img/map1.png",
      iconImageSize: [45, 65],
    });
    myMap.geoObjects.add(myPlacemark);
  });


}

function initSliders() {
  var main = new Splide(".projects-detail__mainslider", {
    type: "fade",
    rewind: true,
    pagination: false,
    arrows: true,
    drag: false,
  });

  var thumbnails = new Splide(".projects-detail__thumbslider", {
    fixedWidth: 86,
    fixedHeight: 52,
    gap: 18,
    arrows: false,
    rewind: true,
    pagination: false,
    cover: true,
    isNavigation: true,
  });

  main.sync(thumbnails);
  main.mount();
  thumbnails.mount();

}