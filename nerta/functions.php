<?php
class WPSE_33175_Simple_Walker extends Walker
{
    public function walk($elements, $max_depth)
    {
        $list = array();

        foreach ($elements as $item)
            $list[] = "<li><a href='$item->url'>$item->title</a></li>";

        return join("\n", $list);
    }
}
/**
 * Removes a function from a specified filter hook.
 * More about flush_rewrite_rules(); @ https://developer.wordpress.org/reference/functions/flush_rewrite_rules/
 */
flush_rewrite_rules();

/*
* Remove rewrite rules and then recreate rewrite rules.
* More about remove_filter(); @ https://developer.wordpress.org/reference/functions/remove_filter/
*/
remove_filter( 'template_redirect', 'redirect_canonical' );
if (function_exists('acf_add_options_page')) {

    acf_add_options_page(array(
        'page_title'     => 'Общие настройки',
        'menu_title'    => 'Общие настройки',
        'menu_slug'     => 'theme-general-settings',
        'capability'    => 'edit_posts',
        'redirect'        => false
    ));
}


remove_filter('comment_text', 'wptexturize');

if (function_exists('add_theme_support')) {
    add_theme_support('menus');
};
add_filter('comment_form_default_fields', 'tu_comment_form_change_cookies_consent');
function tu_comment_form_change_cookies_consent($fields)
{
    $commenter = wp_get_current_commenter();

    $consent   = empty($commenter['comment_author_email']) ? '' : ' checked="checked"';

    $fields['email'] = '<label for="email">Email <span class="required">*</span></label>
<input id="email" name="email" type="email" value="" size="30" maxlength="100" aria-describedby="email-notes" required="required">
';
    return $fields;
}
function show_custom_menu()
{
    $menu_name = 'main';
    $menu = wp_get_nav_menu_object($menu_name);
    $menu_items = wp_get_nav_menu_items($menu->term_id);
    // print_r($menu_items);
    foreach ((array) $menu_items as $key => $menu_item) {
        $title  = $menu_item->title;
        $url    = $menu_item->url;

        if ($menu_item->menu_item_parent == '0') {
            $menu_list .= '<div class="nav__block">';

            $menu_list .= '<a href="' . $url . '">' . $title . '</a>';

            $menu_list .= '<div class="nav__block__pod">';
            foreach ((array) $menu_items as $key => $menu_item_2) {

                if ($menu_item_2->menu_item_parent == $menu_item->db_id) {
                    $title  = $menu_item_2->title;
                    $url    = $menu_item_2->url;
                    $menu_list .= '<a href="' . $url . '">' . $title . '</a>';
                }
            }
            $menu_list .= '</div>';
            $menu_list .= '</div>';
        }
    }

    echo $menu_list;
}


function sendContactForm($search, $data, $isEmail = false)
{

 

    bitrixSend("crm.deal.add", $data);

}


function bitrixSend($method, $params)
{
    $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://nerta.bitrix24.ru/rest/12/f45rcbsb663pw35h/' . $method,
        CURLOPT_POSTFIELDS => http_build_query($params)
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($result, true);
     
    return $result;
}

add_action('wpcf7_mail_sent', 'your_wpcf7_mail_sent_function');
function your_wpcf7_mail_sent_function($contact_form)
{

    $posted_data = $contact_form->posted_data;
    if (14805 == $contact_form->id) {
        $submission = WPCF7_Submission::get_instance();
        $posted_data = $submission->get_posted_data();
        $name = $posted_data['your-name'];

        $tel = $posted_data['your-phone'];
        $params = array(
            'fields' => array(
                'NAME' => $name  ,
                'PHONE' => array(array('VALUE' => $tel, 'VALUE_TYPE' => 'HOME')),
            ),
            'params' => array("REGISTER_SONET_EVENT" => "Y")
        );

        $curl = curl_init();
    curl_setopt_array($curl, array(
        CURLOPT_SSL_VERIFYPEER => 0,
        CURLOPT_POST => 1,
        CURLOPT_HEADER => 0,
        CURLOPT_RETURNTRANSFER => 1,
        CURLOPT_URL => 'https://nerta.bitrix24.ru/rest/12/f45rcbsb663pw35h/' . 'crm.contact.add',
        CURLOPT_POSTFIELDS => http_build_query($params)
    ));

    $result = curl_exec($curl);
    curl_close($curl);

    $result = json_decode($result, true);
    $contact_id=$result['result'];

       
      

        $queryData = array(
            'fields' => array(
                'TITLE' => 'форма - ОСТАЛИСЬ ВОПРОСЫ?',
                'NAME' => $name,    
                "CATEGORY_ID"=> 0,
                'CONTACT_ID' =>$contact_id,
                'PHONE' => array(array('VALUE' => $tel, 'VALUE_TYPE' => 'HOME')),
            ),
            'params' => array("REGISTER_SONET_EVENT" => "Y")
        );

        sendContactForm($tel, $queryData);
    }

}


add_action('wp_footer', 'mycustom_wp_footer');

function mycustom_wp_footer()
{
?>
    <script type="text/javascript">
        document.addEventListener('wpcf7mailsent', function(event) {
            if ('12' == event.detail.contactFormId) {
                yaCounter22985389.reachGoal('callback');
            }
            if ('62' == event.detail.contactFormId) {
                yaCounter22985389.reachGoal('Feedback');
            }
        }, false);
    </script>
<?php
}

add_action('wp_ajax_loadproj', 'my_loadproj');
add_action('wp_ajax_nopriv_loadproj', 'my_loadproj');

function my_loadproj()
{
    $args = array(
        'post_type' => 'projects',
        'posts_per_page' => 4,
        'offset' => $_POST["count"],
        'post_status' => 'publish'
    );
    $count = 0;
    $data = array();
    query_posts($args);
    ob_start();
    if (have_posts()) :
        while (have_posts()): the_post();
            $count++;
            
        ?>
            <div class="projects__item">
                <a href="<?= get_permalink() ?>" class="image">
                    <picture>

                        <source srcset="<?= get_field('preview_image', get_the_ID())['url'] ?>.webp" type="image/webp">
                        <img src="<?=get_field('preview_image', get_the_ID())['url']  ?>" alt="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['title'])  ?>" loading="lazy">

                    </picture>
                </a>
                <a href="<?= get_permalink() ?>" class="name"><?= get_the_title() ?></a>
                <div class="list">
                    <?php foreach (get_field('attr', get_the_ID()) as $item) : ?>
                        <div class="list-item">
                            <div class="desc"><?= $item['name'] ?></div>
                            <div class="value"><?= $item['value'] ?></div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <div class="more"><a href="<?= get_permalink() ?>">Узнать подробнее</a></div>
            </div>
        <?php

        endwhile;
    endif;
    $content = ob_get_contents();
    ob_end_clean();
    $data["count"] = $count;
    $data['text']=$content;
    echo json_encode($data);
    exit;
}


add_action('wp_ajax_loadart', 'my_loadart');
add_action('wp_ajax_nopriv_loadart', 'my_loadart');

function my_loadart()
{
    $args = array(
        'post_type' => 'articles',
        'posts_per_page' => 4,
        'offset' => $_POST["count"],
        'post_status' => 'publish'
    );
    $count = 0;
    $data = array();
    query_posts($args);
    ob_start();
    if (have_posts()) :
        while (have_posts()): the_post();
            $count++;

            ?>
            <a class="articles__item" href="<?= get_the_permalink() ?>" title="<?= get_the_title() ?>">
                <div class="image">
                    <picture>

                        <source srcset="<?= get_field('preview_image', get_the_ID())['url'] ?>.webp" type="image/webp">
                        <img src="<?=get_field('preview_image', get_the_ID())['url']  ?>" alt="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['title'])  ?>"  loading="lazy">

                    </picture>
                </div>
                <div class="name"><?= get_the_title() ?></div>
                <div class="date"><?= get_the_date('d.m.Y') ?></div>
            </a>
        <?php

        endwhile;
    endif;
    $content = ob_get_contents();
    ob_end_clean();
    $data["count"] = $count;
    $data['text']=$content;
    echo json_encode($data);
    exit;
}
function the_breadcrumb()
{

    // получаем номер текущей страницы
    $pageNum = (get_query_var('paged')) ? get_query_var('paged') : 1;

    $separator = ' '; //  »

    // если главная страница сайта
    if (is_front_page()) {

        if ($pageNum > 1) {
            echo '<a href="' . site_url() . '">Главная</a>' . $separator . $pageNum . '-я страница';
        } else {
            echo 'Вы находитесь на главной странице';
        }
    } else { // не главная

        echo '<a href="' . site_url() . '">Главная</a>' . $separator;


        if (is_single()) { // записи

            the_category(', ');
            echo $separator;
            the_title();
        } elseif (is_page()) { // страницы WordPress
            $parent = get_post_parent();
            if ($parent) {
                echo '<a href="' . site_url() . '/' . $parent->post_name . '">' . $parent->post_title . '</a>' . $separator;
            }
            echo '<span>';
            the_title();
            echo '</span>';
        } elseif (is_category()) {

            single_cat_title();
        } elseif (is_tag()) {

            single_tag_title();
        } elseif (is_day()) { // архивы (по дням)

            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
            echo '<a href="' . get_month_link(get_the_time('Y'), get_the_time('m')) . '">' . get_the_time('F') . '</a>' . $separator;
            echo get_the_time('d');
        } elseif (is_month()) { // архивы (по месяцам)

            echo '<a href="' . get_year_link(get_the_time('Y')) . '">' . get_the_time('Y') . '</a>' . $separator;
            echo get_the_time('F');
        } elseif (is_year()) { // архивы (по годам)

            echo get_the_time('Y');
        } elseif (is_author()) { // архивы по авторам

            global $author;
            $userdata = get_userdata($author);
            echo 'Опубликовал(а) ' . $userdata->display_name;
        } elseif (is_404()) { // если страницы не существует

            echo 'Ошибка 404';
        }

        if ($pageNum > 1) { // номер текущей страницы
            echo ' (' . $pageNum . '-я страница)';
        }
    }
}


add_filter('xmlrpc_enabled', '__return_false');
remove_action('wp_head', 'rsd_link');
add_filter('big_image_size_threshold', '_return_false');
add_filter('jpeg_quality', create_function('', 'return 100;'));
add_action('init', 'register_post_types');
add_action('init', 'register_taxonomies');


function register_post_types()
{
    $labels = array(
        'name' => 'projects',
        'menu_name' => 'Проекты',
        'edit_item' => 'Edit',
        'add_new'  => 'Добавить ',
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'supports' => array('title', 'thumbnail'), // title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'
        'public' => true,
        'map_meta_cap' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-admin-post',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true, // Добовляют ссылку к посту
        'exclude_from_search' => false,
        'has_archive' => 'projects',
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array("slug" => "projects"), // URL
        'capability_type' => 'post'
    );

    register_post_type('projects', $args);


    $labels = array(
        'name' => 'articles',
        'menu_name' => 'Статьи',
        'edit_item' => 'Edit',
        'add_new'  => 'Добавить ',
    );
    $args = array(
        'labels' => $labels,
        'hierarchical' => true,
        'supports' => array('title', 'thumbnail', 'comments',), // title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments'
        'public' => true,
        'map_meta_cap' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'menu_icon' => 'dashicons-admin-post',
        'show_in_admin_bar' => true,
        'show_in_nav_menus' => true,
        'publicly_queryable' => true, // Добовляют ссылку к посту
        'exclude_from_search' => false,
        'has_archive' => 'articles',
        'query_var' => true,
        'can_export' => true,
        'rewrite' => array("slug" => "articles"), // URL
        'capability_type' => 'post'
    );

    register_post_type('articles', $args);
}

/* Функция Удаляет поле сайт и email из комментариев */
function remove_url_from_comments($fields)
{

    # Удаляет поле сайт
    unset($fields['url']);
    unset($fields['cookies']);

    #Удаляет поле E-mail (раскомментировать, если надо)
    #unset($fields['email']);

    return $fields;
}
add_filter("comment_form_default_fields", "remove_url_from_comments");




function register_taxonomies()
{
    /* $labels = array(
        'name' => 'section',
        'menu_name' => 'Раздел',
        'edit_item' => 'Редактировать',
    );
    register_taxonomy('tax_catalog', array('catalog'), array(
        'labels' => $labels,
        'hierarchical' => true,
        'show_ui' => true,
        'query_var' => true,
        'has_archive' => true,
        'rewrite' => array("slug" => "section"), // не поворять название поста
        'show_admin_column' => true,
        'show_in_nav_menus' => true
    ));*/
}

add_shortcode('article_phones', 'product_slider_shortcode');

function product_slider_shortcode($atts)
{

    ob_start();

?>
    <div class="phones-banner">
        <div class="phones-banner__bg">
            <picture>
                <source srcset="<?php bloginfo("template_url"); ?>/img/article-detai/banner-2.webp" type="image/webp"><img src="<?php bloginfo("template_url"); ?>/img/article-detai/banner-2.jpg" alt="" loading="lazy">
            </picture>
        </div>
        <div class="inner">
            <div class="phones-banner__title"><?= get_field('shrort', 'options')['title'] ?></div>
            <div class="phones-banner__text"><?= get_field('shrort', 'options')['text'] ?></div>
            <div class="phones-banner__phones">
                <?php foreach (get_field('shrort', 'options')['phone'] as $phone) : ?>
                    <a href="tel:<?= $phone['phone'] ?>"><?= $phone['phone'] ?></a>
                <?php endforeach; ?>
            </div>
            <div class="phones-banner__btns">
                <a class="btn white" href="//wa.me/<?= get_field('shrort', 'options')['link'] ?>" target="_blank">
                    <svg width="30" height="30">
                        <use xlink:href="<?php bloginfo("template_url"); ?>/img/sprites/sprite.svg#ic_whatsapp"></use>
                    </svg><span>Whatsapp</span></a>
                <a class="btn" href="mailto:<?= get_field('shrort', 'options')['email'] ?>"><?= get_field('shrort', 'options')['email'] ?> </a>
            </div>
        </div>
    </div>
<?php

    $content = ob_get_contents();


    ob_end_clean();
    return $content;
}


add_action('wp_ajax_form', 'my_form');
add_action('wp_ajax_nopriv_form', 'my_form');
function my_form()
{
    parse_str($_POST['formData'], $dataForm);
    $admin_email = $dataForm['mail'];
    $from = 'no-reply@dev.nerta.fvds.ru';
    $form_subject = "Ссылка на статью";
    if (empty($dataForm['mail'])) {
        exit(json_encode(array('status' => false, 'msg' => 'Все поля обязательны для заполнения')));
    }
    $c = true;
    $message = "Ссылка на статью " . $dataForm['name'] . " - " . $dataForm['url'];
    function adopt($text)
    {
        return '=?UTF-8?B?' . Base64_encode($text) . '?=';
    }
    $headers = "MIME-Version: 1.0" . PHP_EOL .
        "Content-Type: text/html; charset=utf-8" . PHP_EOL .
        'From: ' . "Nerta-sw" . ' <' . "php-sender-dev.nerta-sw.ru@undeliverable.masterhost.ru" . '>' . PHP_EOL .
        'Reply-To: ' . $admin_email . '' . PHP_EOL;

    if (mail($admin_email, adopt($form_subject), $message, $headers)) {
        exit(json_encode(array('status' => true, 'msg' => '<div class="success"><div class="success-title">Спасибо!</div><div class="success-desc">Ссылка на статью придет вам на почту!</div></div>')));
    }
}

add_filter( 'upload_mimes', 'svg_upload_allow' );

# Добавляет SVG в список разрешенных для загрузки файлов.
function svg_upload_allow( $mimes ) {
    $mimes['svg']  = 'image/svg+xml';

    return $mimes;
}
add_filter( 'wp_check_filetype_and_ext', 'fix_svg_mime_type', 10, 5 );

# Исправление MIME типа для SVG файлов.
function fix_svg_mime_type( $data, $file, $filename, $mimes, $real_mime = '' ){

    // WP 5.1 +
    if( version_compare( $GLOBALS['wp_version'], '5.1.0', '>=' ) )
        $dosvg = in_array( $real_mime, [ 'image/svg', 'image/svg+xml' ] );
    else
        $dosvg = ( '.svg' === strtolower( substr($filename, -4) ) );

    // mime тип был обнулен, поправим его
    // а также проверим право пользователя
    if( $dosvg ){

        // разрешим
        if( current_user_can('manage_options') ){

            $data['ext']  = 'svg';
            $data['type'] = 'image/svg+xml';
        }
        // запретим
        else {
            $data['ext'] = $type_and_ext['type'] = false;
        }

    }

    return $data;
}
add_filter( 'wp_prepare_attachment_for_js', 'show_svg_in_media_library' );

# Формирует данные для отображения SVG как изображения в медиабиблиотеке.
function show_svg_in_media_library( $response ) {

    if ( $response['mime'] === 'image/svg+xml' ) {

        // С выводом названия файла
        $response['image'] = [
            'src' => $response['url'],
        ];
    }

    return $response;
}