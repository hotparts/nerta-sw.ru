<?php
/**
 * Template Name: Service
 */

get_header(); ?>
    <div class="inner-header">
        <div class="container">
            <div class="inner-header-company">Nerta Selfwash</div>
            <?php the_title( '<h1 class="h1">', '</h1>' );?>
        </div>
    </div>

<?php
the_post();
the_content();
?>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript" defer></script>
    <script src="<?php echo get_template_directory_uri()?>/js/map.js" defer></script>
<?php get_footer(); ?>