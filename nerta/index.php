<?php get_header(); ?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/index.css">

    <section class="main">
        <div class="main__container">
            <h1 class="main__title">
                МОЙКИ <br>САМООБСЛУЖИВАНИЯ <br>NERTA-SW
            </h1>
            <div class="brief">
                <p class="brief__information">
                    Произвели более
                    800 постов
                </p>
                </p>
                <p class="brief__information">
                    Работаем с 2000 года
                </p>
                <p class="brief__information">
                    Гарантия – 2 года
                </p>
                <p class="brief__information brief__information_big">
                    Построили более 30 моек самообслуживания под ключ
                </p>
                <p class="brief__information brief__information_big">
                    Собственная сеть из 20 автомоек
                </p>
                <p class="brief__information brief__information_big">
                    Система онлайн-управления и мониторинга комплексом
                </p>
            </div>
        </div>
    </section>

    <div class="cost">
        <div class="container">
            <div class="title">
                Стоимость
                комплектаций
            </div>
            <div class="pagination">
                <div class="pagination-title">Количество постов</div>
                <ul>
                    <li class="js-post-count" data-count="1">
                        1
                    </li>
                    <li class="js-post-count" data-count="2">
                        2
                    </li>
                    <li class="js-post-count" data-count="3">
                        3
                    </li>
                    <li class="js-post-count" data-count="4">
                        4
                    </li>
                    <li class="js-post-count" data-count="5">
                        5
                    </li>
                    <li class="js-post-count" data-count="6">
                        6
                    </li>
                    <li class="js-post-count" data-count="7">
                        7
                    </li>
                    <li class="js-post-count" data-count="8">
                        8
                    </li>
                </ul>
            </div>
            <div class="costtable">
                <div class="costtable-dop">
                    <div class="costtable-dop-title">Дополнительно</div>
                    <label class="pagination-checkbox checkbox">
                        <input type="checkbox" name="montag" class="js-post-montag"> <span>Монтаж&nbsp;—&nbsp;<span
                                    class="strong"><span class="js-post-montag-cost"></span>&nbsp;&euro;</strong></span>
                    </label>
                    <label class="pagination-checkbox checkbox">
                        <input type="checkbox" name="dop" class="js-post-dop"> <span>Система водоподготовки и&nbsp;обратного осмоса, включая монтаж&nbsp;—&nbsp;<span
                                    class="strong"><span class="js-post-dop-cost"></span>&nbsp;&euro;</strong></span>
                    </label>
                </div>
                <ul>
                    <li class="start">
                        <div class="costtable-item js-costtable-btn" data-type="start">
                            <div class="costtable-text">
                                Start
                                <span><span class="js-post-cost" data-type="start"></span>&nbsp;&euro;</span>
                            </div>
                        </div>
                        <div class="costtable-compare js-costtable-compare" data-type="start">
                            Сравнить
                            <svg class="costtable-arr">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use>
                            </svg>
                        </div>
                    </li>
                    <li class="base">
                        <div class="costtable-item js-costtable-btn" data-type="base">
                            <div class="costtable-text">
                                Base
                                <span><span class="js-post-cost" data-type="base"></span>&nbsp;&euro;</span>
                            </div>
                        </div>
                        <div class="costtable-compare js-costtable-compare" data-type="base">
                            Сравнить
                            <svg class="costtable-arr">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use>
                            </svg>
                        </div>
                    </li>
                    <li class="light">
                        <div class="costtable-item">
                            <div class="costtable-text js-costtable-btn" data-type="light">
                                Light
                                <span><span class="js-post-cost" data-type="light"></span>&nbsp;&euro;</span>
                            </div>
                        </div>
                        <div class="costtable-compare js-costtable-compare" data-type="light">
                            Сравнить
                            <svg class="costtable-arr">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use>
                            </svg>
                        </div>
                    </li>
                    <li class="optima">
                        <div class="costtable-item js-costtable-btn" data-type="optima">
                            <div class="costtable-text">
                                Optima
                                <span><span class="js-post-cost" data-type="optima"></span>&nbsp;&euro;</span>
                            </div>
                        </div>
                        <div class="costtable-compare js-costtable-compare" data-type="optima">
                            Сравнить
                            <svg class="costtable-arr">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use>
                            </svg>
                        </div>
                    </li>
                    <li class="maxi">
                        <div class="costtable-item js-costtable-btn" data-type="maxi">
                            <div class="costtable-text">
                                Maxi
                                <span><span class="js-post-cost" data-type="maxi"></span>&nbsp;&euro;</span>
                            </div>
                        </div>
                        <div class="costtable-compare js-costtable-compare" data-type="maxi">
                            Сравнить
                            <svg class="costtable-arr">
                                <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arr"></use>
                            </svg>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="dop">
                <ul class="dop-list">
                    <li class="dop-item">
                        <div class="dop-img">
                            <img src="<?php bloginfo("template_url"); ?>/img/equipment/eq2.jpg" alt="Система
водоподготовки">
                        </div>
                        <div class="oh">
                            <div class="dop-title">Система водоподготовки</div>
                            <p>2&nbsp;фильтра умягчения
                                непрерывного действия</p>
                            <p>Контейнер для таблетированной соли</p>
                            <p>Установка фильтрации
                                непрерывного действия</p>
                            <p>Ёмкость с&nbsp;системой автоматики</p>
                            <p class="dop-cost">Цена: <span class="js-post-dop-price" data-type="preparation"></span>
                                &euro;</p>
                        </div>
                    </li>
                    <li class="dop-item">
                        <div class="dop-img">
                            <img src="<?php bloginfo("template_url"); ?>/img/equipment/eq3.jpg" alt="Система
обратного осмоса">
                        </div>
                        <div class="oh">
                            <div class="dop-title">Система обратного осмоса</div>

                            <p>Рама из&nbsp;нержавеющей стали</p>
                            <p>Система обратного осмоса
                                производительностью 750 литров в&nbsp;час</p>
                            <p>Электронная панель управления
                                с&nbsp;системой контроля качества воды</p>
                            <p>Автоматическая система отключения
                                осмоса при низком уровне воды
                                с&nbsp;информированием на&nbsp;табло</p>
                            <p class="dop-cost">Цена: <span class="js-post-dop-price" data-type="osmosis"></span> &euro;
                            </p>
                        </div>
                    </li>
                </ul>
            </div>

            <div class="costcompare">

            </div>
        </div>
    </div>

    <div class="equipment">
        <div class="container">
            <div class="title">Дополнительное оборудование</div>
            <ul class="equipment-list">
                <li class="equipment-item">
                    <div class="equipment-img">
                        <img src="<?php bloginfo("template_url"); ?>/img/equipment/eq1.jpg" alt="Пылесос самообслуживания
EWA на 1 пост">
                    </div>
                    <div class="equipment-title">Пылесос самообслуживания
                        EWA на&nbsp;1&nbsp;пост
                    </div>

                    <dl>
                        <dt>Мощность</dt>
                        <dd>2,5 кВт</dd>
                    </dl>
                    <dl>
                        <dt>Поток воздуха пылесоса</dt>
                        <dd>180 м3/час</dd>
                    </dl>
                    <dl>
                        <dt>Вид питания</dt>
                        <dd>3&times;400&nbsp;ВV 50hz</dd>
                    </dl>
                    <p class="equipment-cost">Цена: 2500 &euro;</p>
                </li>
                <li class="equipment-item">
                    <div class="equipment-img">
                        <img src="<?php bloginfo("template_url"); ?>/img/equipment/eq1.jpg" alt="Пылесос самообслуживания
EWA на 1 пост">
                    </div>
                    <div class="equipment-title">Пылесос самообслуживания
                        EWA на&nbsp;2&nbsp;поста
                    </div>

                    <dl>
                        <dt>Мощность</dt>
                        <dd>2 x 2,5 кВт</dd>
                    </dl>
                    <dl>
                        <dt>Поток воздуха пылесоса</dt>
                        <dd>2 x 180 м<sup>3</sup>/час</dd>
                    </dl>
                    <dl>
                        <dt>Вид питания</dt>
                        <dd>3&times;400&nbsp;ВV 50hz</dd>
                    </dl>
                    <p class="equipment-cost">Цена: 5000 &euro;</p>
                </li>
            </ul>
        </div>
    </div>

    <div class="feedback">
        <div class="container">
            <div class="feedback-content">
                <?php echo do_shortcode('[contact-form-7 id="12" title="Callback"]'); ?>
            </div>
        </div>
    </div>
    <div class="options">
        <div class="container">
            <div class="options-wrapper">
                <img src="<?php bloginfo("template_url"); ?>/img/options/rama.jpg"
                     alt="Рама с&nbsp;силовым оборудованием">
                <div class="title">Рама с&nbsp;силовым оборудованием</div>
                <div class="options-text">
                    <p>Изготовлена из&nbsp;нержавеющей стали марки Aisi 304, что делает ее&nbsp;максимально надежной и&nbsp;долговечной.
                        Конструкция рамы разработана с&nbsp;доступом к&nbsp;каждому элементу. Это позволяет быстро
                        производить техническое обслуживание и&nbsp;ремонтные работы. Независимые друг от&nbsp;друга
                        посты с&nbsp;индивидуальным управлением. Возможна сборка комплектации согласно&nbsp;ТЗ
                        заказчика. Удобно для тех, кто знает, что конкретно нужно выбрать.</p>
                </div>
                <br>
                <br>
                <div class="title">Панель управления</div>
                <div class="options-text">
                    <p>Дизайн панели может быть выполнен в&nbsp;фирменном стиле заказчика.</p>
                </div>
                <div class="panel">
                    <div class="panel-item">
                        <img src="<?php bloginfo("template_url"); ?>/img/options/panel1.jpg" alt="Комплектация Maxi">
                        <div class="panel-item-title">Комплектация Maxi</div>
                    </div>
                    <div class="panel-item">
                        <img src="<?php bloginfo("template_url"); ?>/img/options/panel2.jpg" alt="Light / Optima">
                        <div class="panel-item-title">Light / Optima</div>
                    </div>
                    <div class="panel-item last-child">
                        <img src="<?php bloginfo("template_url"); ?>/img/options/panel3.jpg" alt="Basic / Start">
                        <div class="panel-item-title">Basic / Start</div>
                    </div>
                    <div class="panel-payment">
                        <div class="panel-payment-title">Формы оплаты</div>
                        <ul>
                            <li>&mdash;&nbsp;Купюры</li>
                            <li>&mdash;&nbsp;Монеты</li>
                            <li>&mdash;&nbsp;Банковские карты</li>
                            <li>&mdash;&nbsp;Жетоны</li>
                            <li>&mdash;&nbsp;Карты лояльности</li>
                            <li>&mdash;&nbsp;Чип-ключи</li>
                        </ul>
                    </div>
                </div>
                <div class="clearfix">
                    <img src="<?php bloginfo("template_url"); ?>/img/options/panel4.jpg"
                         alt="Программы панели управления" class="options-text-panel">
                    <div class="options-text subtitle">Основные программы</div>
                    <div class="options-text oh">
                        <p>
                            <strong>&laquo;Пена&raquo;</strong><br>
                            работает с&nbsp;холодной водой, при пониженном давлении в&nbsp;магистрали, смесь воды,
                            вспененного шампуня и&nbsp;воздуха поступает к&nbsp;пистолету, предназначенному для
                            нанесения пены.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Вода+Пена&raquo;</strong><br>
                            Для полной мойки автомобиля с&nbsp;использованием только одного инструмента. Смесь воды и&nbsp;шампуня
                            поступает по&nbsp;магистрали к&nbsp;пистолету, предназначенному для высокого давления.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Холодная вода&raquo;, &laquo;Горячая вода&raquo;</strong><br>
                            Холодная или горячая вода под высоким давлением, поступает по&nbsp;магистрали
                            к&nbsp;пистолету, предназначенному для высокого давления. Предназначена
                            для ополаскивания автомобиля после мойки с&nbsp;шампунем или после нанесения пены.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Воск&raquo;</strong><br>
                            Работает с&nbsp;холодной водой, под высоким давлением в&nbsp;магистрали смесь воды
                            и&nbsp;воска поступает к&nbsp;пистолету, предназначенному для высокого давления.
                            Предназначена для нанесения защитного слоя полимерного воска.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Пауза&nbsp;/ Стоп&raquo;</strong><br>
                            Остановка любой из&nbsp;выбранных программ, например, для смены пистолета
                            или других действий.
                        </p>
                    </div>
                    <div class="options-text subtitle">Дополнительные программы</div>
                    <div class="options-text oh">
                        <p>
                            <strong>&laquo;Осмос&raquo;</strong><br>
                            Работает под высоким давлением холодной водой, подготовленной установкой обратного осмоса.
                            Деминерализованная вода поступает к&nbsp;пистолету, предназначенному для высокого давления.
                            Предназначена для удаления
                            с&nbsp;лакокрасочного покрытия автомобиля остаточных солей жесткости. Полностью исключает
                            появление разводов на&nbsp;кузове автомобиля, после применения данной функции машину можно
                            не&nbsp;протирать.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Воздух&raquo;</strong><br>
                            Обдув мощным потоком воздуха труднодоступные места автомобиля.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Мойка дисков&raquo;</strong><br>
                            Нанесение на&nbsp;автомобильные диски специального моющего средства, которое помогает
                            избавиться от&nbsp;сильного загрязнения и&nbsp;тормозной пыли.
                        </p>
                        <br>
                        <p>
                            <strong>&laquo;Средство от насекомых&raquo;</strong><br>
                            Нанесение специального химического состава против следов от&nbsp;мошки, мух и&nbsp;других
                            насекомых на&nbsp;поверхности транспортного средства.</p>
                        <br>
                        <p>
                            <strong>&laquo;Щетка&raquo;</strong><br>
                            Альтернативное нанесение пены с&nbsp;помощью специальной щетки на&nbsp;транспортное
                            средство, для удаления более глубоких загрязнений.
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="application">
            <div class="container">
                <img src="<?php bloginfo("template_url"); ?>/img/options/application.jpg" alt="Мониторинг">
            </div>
        </div>
        <div class="container">
            <div class="options-wrapper">
                <div class="title">Мониторинг</div>
                <div class="options-text">
                    <p>Нами разработана система удаленного контроля и&nbsp;управления моплексом самообслуживания. Каждый
                        пост оборудования снабжен GSM-модулем, что позволяет контролировать работу всего комплекса
                        самообслуживания из&nbsp;любой точки мира, где есть доступ к&nbsp;интернету. Вы&nbsp;всегда
                        будете в&nbsp;курсе технического и&nbsp;финансового состояния вашей мойки.</p>
                    <br>

                    <p>Круглосуточный доступ к&nbsp;следующей информации:</p>
                    <ul>
                        <li>Сумма выручки с&nbsp;каждого поста или количеству проданных жетонов;</li>
                        <li>Время работы каждой программы;</li>
                        <li>Расход и&nbsp;остаток используемых химических препаратов;</li>
                        <li>Текущие затраты на&nbsp;электроэнергию, воду, химию и&nbsp;т.д.;</li>
                        <li>Необходимость проведения технического обслуживания;</li>
                        <li>Управление стоимостью минут каждой программы;</li>
                        <li>Детализация любой информации за&nbsp;необходимый период времени по&nbsp;почте или смс.</li>
                    </ul>
                </div>
                <br>
                <br>
                <div class="cleafix">
                    <img src="<?php bloginfo("template_url"); ?>/img/options/discount.jpg" alt="Карта лояльности"
                         class="options-text-discount">
                    <div class="oh">
                        <div class="title">Карты лояльности</div>
                        <div class="options-text">
                            <p>Наша эксклюзивная разработка&nbsp;&mdash; оплата бесконтактными картами лояльности.
                                Данная форма оплаты позволяет &laquo;привязать&raquo; клиентов к&nbsp;вашим мойкам.
                                Карта может быть использована
                                для начисления баллов в&nbsp;зависимости от&nbsp;суммы разового пополнения. Пополнять
                                карту можно на&nbsp;мойке и&nbsp;через интернет. Сервер статистики позволяет отслеживать
                                клиентам свой баланс, частоту пополнения, посещения моек, выбора программ. Дизайн карты
                                может быть выполнен в&nbsp;любом стиле.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="gallery-list">
        <div class="gallery-item">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-name">Каркас</div>
                        <div class="slider-text-equip">ECONOM</div>
                        <div class="gallery-switcher">
                            <span class="active">Эко</span>
                            <span>Стандарт</span>
                            <span>СтандартПлюс</span>
                            <span>Премиум</span>
                            <span>Элит</span>
                        </div>
                        <p>Чёрный металл, окраска на&nbsp;месте, перегородки из&nbsp;баннерной ткани</p>
                        <table class="slider-text-list">
                            <tr>
                                <td>
                                    2&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    150&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    4&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    250&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    6&nbsp;постов
                                    <span class="delimetr">&mdash;</span>
                                    350&thinsp;000&thinsp;Р
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php echo do_shortcode('[metaslider id=17]'); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-name">Каркас</div>
                        <div class="slider-text-equip">STANDART</div>
                        <div class="gallery-switcher">
                            <span>Эко</span>
                            <span class="active">Стандарт</span>
                            <span>СтандартПлюс</span>
                            <span>Премиум</span>
                            <span>Элит</span>
                        </div>
                        <p>Чёрный металл, холодное цинкование на&nbsp;производстве, перегородки из&nbsp;баннерной
                            ткани</p>
                        <table class="slider-text-list">
                            <tr>
                                <td>
                                    2&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    450&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    4&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    700&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    6&nbsp;постов
                                    <span class="delimetr">&mdash;</span>
                                    900&thinsp;000&thinsp;Р
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php echo do_shortcode('[metaslider id=97]'); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-name">Каркас</div>
                        <div class="slider-text-equip">STANDART+</div>
                        <div class="gallery-switcher">
                            <span>Эко</span>
                            <span>Стандарт</span>
                            <span class="active">СтандартПлюс</span>
                            <span>Премиум</span>
                            <span>Элит</span>
                        </div>
                        <p>Чёрный металл, горячее цинкование на&nbsp;производстве, перегородки из&nbsp;поликарбоната</p>
                        <table class="slider-text-list">
                            <tr>
                                <td>
                                    2&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    600&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    4&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    1&thinsp;000&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    6&nbsp;постов
                                    <span class="delimetr">&mdash;</span>
                                    1&thinsp;400&thinsp;000&thinsp;Р
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php echo do_shortcode('[metaslider id=98]'); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-name">Каркас</div>
                        <div class="slider-text-equip">PREMIUM</div>
                        <div class="gallery-switcher">
                            <span>Эко</span>
                            <span>Стандарт</span>
                            <span>СтандартПлюс</span>
                            <span class="active">Премиум</span>
                            <span>Элит</span>
                        </div>
                        <p>Нержавеющая сталь Aisi 304, перегородки из&nbsp;каленого стекла или триплекса</p>
                        <table class="slider-text-list">
                            <tr>
                                <td>
                                    2&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    2&thinsp;400&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    4&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    3&thinsp;300&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    6&nbsp;постов
                                    <span class="delimetr">&mdash;</span>
                                    4&thinsp;500&thinsp;000&thinsp;Р
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php echo do_shortcode('[metaslider id=99]'); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-name">Каркас</div>
                        <div class="slider-text-equip">ELITE</div>
                        <div class="gallery-switcher">
                            <span>Эко</span>
                            <span>Стандарт</span>
                            <span>СтандартПлюс</span>
                            <span>Премиум</span>
                            <span class="active">Элит</span>
                        </div>
                        <p>Здание из&nbsp;сэндвич панелей, фасады из&nbsp;стекла, подвесная система открывания ворот</p>
                        <table class="slider-text-list">
                            <tr>
                                <td>
                                    2&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    3&thinsp;500&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    4&nbsp;поста
                                    <span class="delimetr">&mdash;</span>
                                    4&thinsp;400&thinsp;000&thinsp;Р
                                </td>
                                <td>
                                    6&nbsp;постов
                                    <span class="delimetr">&mdash;</span>
                                    5&thinsp;500&thinsp;000&thinsp;Р
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <?php echo do_shortcode('[metaslider id=100]'); ?>
            </div>
        </div>
    </div>

<?php get_footer(); ?>