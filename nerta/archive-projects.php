<?php
//Template Name: new projects
get_header();
$data = get_fields();

$my_posts = get_posts(array(
    'numberposts' => -1,
    'post_type' => 'projects',
));
$themes = array();
$years = array();
foreach ($my_posts as $post) {
    setup_postdata($post);
    if (!in_array(get_field('theme'), $themes)) {
        array_push($themes, get_field('theme'));
    }
    if (!in_array(get_the_date('Y'), $years)) {
        array_push($years, get_the_date('Y'));
    }
}

if ($_GET['topic'] or $_GET['years']) {
   
    $my_posts = get_posts(array(
        'numberposts' => -1,
        'post_type' => 'projects',
        'meta_key'        => 'theme',
        'meta_value'    => $_GET['topic'],
        'date_query' => array(
            array(
                'year'  => $_GET['years'] 
            ),
        ),
    ));
} else {
    $my_posts = get_posts(array(
        'numberposts' => 6,
        'post_type' => 'projects',
    ));
}


wp_reset_postdata();
?>
<div id="nerta-main-page">
    <section class="pageBanner without-description">
        <div class="inner">
            <picture class="pageBanner__bg">
                <source srcset="<?php bloginfo("template_url"); ?>/img/projects/banner.webp" type="image/webp">
                <img src="<?php bloginfo("template_url"); ?>/img/projects/banner.jpg" alt="Баннер">
            </picture>
            <div class="pageBanner__content">
                <h1 class="pageBanner__title">Проекты и отзывы</h1>
            </div>
            <div class="breadcrumbs">
                <a href="https://nerta-sw.ru">Главная</a>
                <span>Проекты и отзывы</span>
            </div>
        </div>
    </section>
    <section class="projects">
        <div class="inner">
            <div class="projects__head">
                <div class="projects__headSort">
                    <span>Сортировка:</span>

                    <form action="" method="GET">
                        <select name="years">
                            <option value="Год" disabled selected>Год</option>
                            <option value="" selected>За все время</option>
                            <?php foreach ($years as $item) : ?>
                                <option value="<?= $item ?>" <?= (isset($_GET['years']) and $_GET['years'] == $item) ? "selected" : "" ?>><?= $item ?></option>
                            <?php endforeach; ?>
                        </select>
                        <select name="topic">
                            <option value="Тематика" disabled selected>Тематика</option>
                            <option value="" selected>Все тематики</option>
                            <?php foreach ($themes as $item) : ?>
                                <option value="<?= $item ?>" <?= (isset($_GET['topic']) and $_GET['topic'] == $item) ? "selected" : "" ?>><?= $item ?></option>
                            <?php endforeach; ?>

                        </select>
                    </form>
                </div>
            </div>
            <div class="projects__items">
                <?php
                foreach ($my_posts as $post) {
                    setup_postdata($post);
                ?>
                    <div class="projects__item">
                        <a href="<?= get_permalink() ?>" class="image">
                            <picture>

                                <source srcset="<?= get_field('preview_image', get_the_ID())['url'] ?>.webp" alt="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",get_field('preview_image', get_the_ID())['title'])  ?>"  type="image/webp">
                                <img src="<?= get_field('preview_image', get_the_ID())['url'] ?>" alt="" loading="lazy">
                            </picture>
                        </a>
                        <a href="<?= get_permalink() ?>" class="name"><?= get_the_title() ?></a>
                        <div class="list">
                            <?php foreach (get_field('attr', get_the_ID()) as $item) : ?>
                                <div class="list-item">
                                    <div class="desc"><?= $item['name'] ?></div>
                                    <div class="value"><?= $item['value'] ?></div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="more"><a href="<?= get_permalink() ?>">Узнать подробнее</a></div>
                    </div>
                <?php
                }
                ?>
            </div>
                    <?php if (!$_GET['topic'] and !$_GET['years']):?>
                        <button class="projects__items--more">Показать больше</button>
                    <?php endif ?>
          
        </div>
    </section>
    <?php wp_reset_postdata(); ?>
    <section class="formQuestions">
        <div class="inner">
            <div class="formQuestions__left">
                <div class="formQuestions__title">ОСТАЛИСЬ ВОПРОСЫ?</div>
                <div class="formQuestions__desc">Звоните или оставляйте заявку на сайте. Мы ответим на все
                    интересующие вас вопросы!
                </div>
            </div>
            <div class="formQuestions__right">
                <form class="form-component formQuestions__form" action="/wp-json/contact-form-7/v1/contact-forms/14805/feedback" method="POST" data-form="banner">
                    <div class="form__field">
                        <input type="text" required name="your-name" placeholder="Имя">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__field">
                        <input type="tel" required name="your-phone" placeholder="+7 (9__) ___ __ __">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__submit">
                        <button type="submit">Оставить заявку</button>
                    </div>
                </form>
            </div>
        </div>

    </section>
</div>

<?php get_footer(); ?>