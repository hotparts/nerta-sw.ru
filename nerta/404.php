<?php
/**
 * Template Name: Inner
 */

get_header(); ?>
    <div class="inner-header">
        <div class="container">
            <div class="inner-header-company">Nerta Selfwash</div>
            <h1 class="h1">404</h1>
        </div>
    </div>
    <div class="services">
        <div class="container">
            <div class="services-feedback"><div class="feedback-content"> <?php echo do_shortcode( '[contact-form-7 id="12" title="Callback"]' ); ?></div></div>
            <div class="services-text">
                <h2>Страница не найдена!</h2>
            </div>
        </div>
    </div>

<?php get_footer(); ?>