<?php
get_header();
$data = get_fields();
$post = get_queried_object();
//To return the URL of the previous page, use the following php code:
$prev = get_permalink(get_adjacent_post(false, '', false));
//To return the URL of the next page, use the following php code:
$next = get_permalink(get_adjacent_post(false, '', true));
?>
<div id="nerta-main-page">
    <section class="pageBanner">
        <div class="inner">
            <picture class="pageBanner__bg">
                <source srcset="<?= get_field('detail_image', $post->ID) ?>.webp" type="image/webp">
                <img src="<?= get_field('detail_image', $post->ID) ?>" alt="Баннер">
            </picture>
            <div class="pageBanner__content">
                <h1 class="pageBanner__title"><?= $post->post_title ?></h1>
                <div class="pageBanner__desc"><?= get_field('desc', $post->ID) ?></div>
            </div>
            <div class="breadcrumbs"><a href="https://nerta-sw.ru">Главная</a><a href="/projects/">Проекты и отзывы</a><span><?= $post->post_title ?></span></div>
        </div>
    </section>
    <section class="projects-detail">
        <div class="inner">
            <div class="review-block">
                <div class="review-block__title">Отзыв заказчика</div>
                <div class="review-block__desc"><?= get_field('review', $post->ID) ?></div>
            </div>
            <div class="projects-detail__text">
                <?= get_field('text', $post->ID) ?>
            </div>
            <div class="projects-detail__mainslider splide">
                <div class="arrows splide__arrows">
                    <div class="splide__arrow splide__arrow--prev arrows__arrow prev">
                        <svg width="20" height="20">
                            <use xlink:href="<?php bloginfo("template_url"); ?>/img/sprites/sprite.svg#ic_arrow-left"></use>
                        </svg>
                    </div>
                    <div class="splide__arrow splide__arrow--next arrows__arrow next">
                        <svg width="20" height="20">
                            <use xlink:href="<?php bloginfo("template_url"); ?>/img/sprites/sprite.svg#ic_arrow-right"></use>
                        </svg>
                    </div>
                </div>
                <div class="splide__track">
                    <ul class="splide__list">

                        <?php foreach (get_field('slider', $post->ID) as $slide) : ?>

                            <?php if ($slide['coordinats']['x']) : ?>
                                <li class="splide__slide">
                                    <div class="image">
                                        <div id="map" data-x="<?= $slide['coordinats']['x'] ?>" data-y="<?= $slide['coordinats']['y'] ?>"></div>
                                    </div>
                                </li>
                            <?php else : ?>
                                <li class="splide__slide">
                                    <div class="image">
                                        <picture>
                                            <source srcset="<?= $slide['image']['url'] ?>.webp" alt="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['title'])   ?>"  type="image/webp">
                                            <img src="<?= $slide['image']['url'] ?>" alt="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['title'])   ?>" loading="lazy">
                                        </picture>
                                    </div>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <div class="projects-detail__thumbslider splide">
                <div class="splide__track">
                    <ul class="splide__list">
                        <?php foreach (get_field('slider', $post->ID) as $slide) : ?>
                            <?php if ($slide['coordinats']['x']) : ?>
                                <li class="splide__slide">
                                    <div class="image">
                                        <picture>
                                            <source srcset="<?= $slide['coordinats']['image']['url'] ?>.webp" alt="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['title'])   ?>"  type="image/webp">
                                            <img src="<?= $slide['coordinats']['image']['url'] ?>"  alt="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['title'])   ?>"  loading="lazy">
                                        </picture>
                                    </div>
                                </li>
                            <?php else : ?>
                                <li class="splide__slide">
                                    <div class="image">
                                        <picture>
                                            <source srcset="<?= $slide['image']['url'] ?>.webp"   alt="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['title'])   ?>" type="image/webp">
                                            <img src="<?= $slide['image']['url'] ?>"  alt="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['alt'])  ?>" title="<?=str_replace("\xE2\x80\x8B", "",$slide['image']['title'])   ?>"  loading="lazy">
                                        </picture>
                                    </div>
                                </li>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
            <?php if (get_field('table', $post->ID)) : ?>
                <div class="projects-detail__text">
                    <h2>Показатели мойки:</h2>
                    <div class="table">
                        <?= get_field('table', $post->ID) ?>
                    </div>
                </div>
            <?php endif; ?>
            <div class="projects-detail__text">
                <h2>Использованное в кейсе оборудование:</h2>
                <div class="equip">
                    <div class="equip__title"> <?= get_field('equip', $post->ID)['title'] ?></div>
                    <div class="equip__desc"><?= get_field('equip', $post->ID)['desc'] ?></div>
                    <div class="equip__more"><a href="<?= get_field('equip', $post->ID)['link'] ?>">Подробнее об
                            оборудовании</a></div>
                </div>

            </div>

            <div class="projects-detail__nav">
                <?php if ($prev != get_permalink()) : ?>
                    <a class="nav-item" href="<?= $prev ?>">
                        <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M0.792892 8.74617C0.402369 8.35565 0.402369 7.72248 0.792892 7.33196L7.15685 0.967995C7.54738 0.57747 8.18054 0.57747 8.57107 0.967995C8.96159 1.35852 8.96159 1.99168 8.57107 2.38221L2.91421 8.03906L8.57107 13.6959C8.96159 14.0864 8.96159 14.7196 8.57107 15.1101C8.18054 15.5007 7.54738 15.5007 7.15685 15.1101L0.792892 8.74617ZM20.5 9.03906H1.5V7.03906H20.5V9.03906Z" fill="#A3A4A5" />
                        </svg>
                        <span>Предыдующий кейс </span></a>
                <?php endif; ?>
                <?php if ($next != get_permalink()) : ?>
                    <a class="nav-item" href="<?= $next ?>"><span>Следующий кейс</span>
                        <svg width="21" height="16" viewBox="0 0 21 16" fill="none" xmlns="http://www.w3.org/2000/svg">
                            <path d="M20.2071 8.74617C20.5976 8.35565 20.5976 7.72248 20.2071 7.33196L13.8431 0.967995C13.4526 0.57747 12.8195 0.57747 12.4289 0.967995C12.0384 1.35852 12.0384 1.99168 12.4289 2.38221L18.0858 8.03906L12.4289 13.6959C12.0384 14.0864 12.0384 14.7196 12.4289 15.1101C12.8195 15.5007 13.4526 15.5007 13.8431 15.1101L20.2071 8.74617ZM0.5 9.03906H19.5V7.03906H0.5V9.03906Z" fill="#A3A4A5" />
                        </svg>
                    </a>
                <?php endif; ?>
            </div>
        </div>
    </section>
    <section class="formQuestions">
        <div class="inner">
            <div class="formQuestions__left">
                <div class="formQuestions__title">ОСТАЛИСЬ ВОПРОСЫ?</div>
                <div class="formQuestions__desc">Звоните или оставляйте заявку на сайте. Мы ответим на все
                    интересующие вас вопросы!
                </div>
            </div>
            <div class="formQuestions__right">
                <form class="form-component formQuestions__form" action="/wp-json/contact-form-7/v1/contact-forms/14805/feedback" method="POST" data-form="banner">
                    <div class="form__field">
                        <input type="text" required name="your-name" placeholder="Имя">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__field">
                        <input type="tel" required name="your-phone" placeholder="+7 (9__) ___ __ __">
                        <span class="form__field--caption"></span>
                    </div>
                    <div class="form__submit">
                        <button type="submit">Оставить заявку</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
</div>

<?php get_footer(); ?>