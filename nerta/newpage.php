<?php
/**
 * Template Name: equipment_new
 */
get_header();

$data=get_fields();
?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/equipments.css">
<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/start.css">

<section class="chief" style="background-image: url('<?= $data['banner_image']["url"]?>">
    <div class="chief__container">
        <div class="breadcrumbs">
            <?php the_breadcrumb() ?>

        </div>
        <h1 class="chief__title">
           <?=$data['banner_title']?>
        </h1>
        <div class="chief__description">
            <p class="chief__description__information">
                <?=$data['banner_text']?>
            </p>
        </div>
    </div>
</section>
<section class="options">
    <div class="options__container">
        <div class="options__subtitle">
            <h2>
                <?=$data['price_title']?>
            </h2>
        </div>
        <div class="options__row">
            <?php
            foreach ($data['prices'] as $i=>$price) {
                if(empty($price['image'])){
                    ?>
                    <div onclick="return location.href = '<?=$price['link']?>'"
                         class="options__block color_<?=($i<4)?$i+1:$i?> <?=($i==0)?"shadow_box":""?>">
                        <h2 class="options__title__big">
                            <?=$price['title']?>
                        </h2>
                        <h2 class="options__title">
                            <?=$price['text']?>
                        </h2>
                        <ul class="options__list">
                            <?php
                            foreach ($price['list'] as $item) {
                                ?>
                                <li><?=$item['item']?></li>
                                    <?php
                            }
                            ?>


                        </ul>

                        <div class="options__information">
                            <div class="block__cost">
                                <div class="block__cost__container">
                                    <div class="block__cost__text">
                                        <?=$price['price']['title']?>
                                    </div>
                                    <div class="block__cost__number">
                                        <div class="block__cost__number1">
                                            <?=$price['price']['euro']?>
                                        </div>
                                        <div class="block__cost__number3">
                                            <?=$price['price']['price']?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                        <?php
                }else{
                    ?>
                    <div class="options__block__image"
                         style="background-image: url('<?=$price['image']['url'] ?>)">
                    </div>
            <?php
                }
            }
            ?>

        </div>
    </div>
</section>
<?php
foreach ($data['systems'] as $i=>$system) {
    ?>
    <section class="preview">
        <div class="preview__container">
            <div class="preview__title">
                <h2>
                    <?=$system['title']?>
                </h2>
            </div>
            <div class="preview__row <?=($i==1)?" preview__row__reverse":""?>">
                <div class="preview__column">
                    <ul>
                        <?php
                        foreach ($system['list'] as $item) {
                            ?>
                            <li><?=$item['item']?></li>
                        <?php
                        }
                        ?>


                    </ul>
                    <p class="preview__p">
                        <?=$system['text']?>
                    </p>
                    <button class="preview__button">Подробнее</button>
                    <div class="block__cost">
                        <div class="block__cost__container">
                            <div class="block__cost__text">
                                <?=$system['prices']['title']?>
                            </div>
                            <div class="block__cost__number">
                                <div class="block__cost__number1">
                                    <?=$system['prices']['euro']?>
                                </div>
                                <div class="block__cost__number3">
                                    <?=$system['prices']['price']?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="preview__column__image">
                    <img class="preview__image" src="<?=$system['image']['url']?>"
                         alt="<?=$system['image']['alt']?>"
                         title="<?=$system['image']['title']?>">
                </div>
            </div>
        </div>
    </section>
    <?php
}
?>

<section class="dop__equipment dop__equipment__display">
    <div class="dop__equipment__container">
        <div class="dop__equipment__title">
            <h2>
                <?=$data['dop_title']?>
            </h2>
        </div>
        <div class="dop__equipment__row">
            <?php
            foreach ($data['dop'] as $i=>$item) {
                ?>
            <div class="dop__equipment__column" onclick="return location.href = '<?=$item['link']?>'" style="cursor:pointer">
                <img src="<?=$item['image']['url']?>"
                     alt="<?=$item['image']['alt']?>"
                     title="<?=$item['image']['title']?>">
                <div class="dop__equipment__column__title">
                    <p><?=$item['title']?></p>
                </div>
                <div class="dop__equipment__column__list">
                    <ul>
                        <?php
                        foreach ($item['list'] as $list) {
                            ?>
                            <li><?=$list['item']?></li>
                            <?php
                        }
                        ?>

                    </ul>
                </div>

                <button>Подробнее</button>
            </div>
                <?php
            }
            ?>

        </div>
    </div>
</section>
<section class="benefits">
    <div class="benefits__container">
        <div class="benefits__title">
            <h2><?=$data['benefits_title']?></h2>
        </div>
        <div class="benefits__content">
            <div class="benefits__content__column_1">
                <div class="benefits__content__block benefits__content__block__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            <?=$data['complect']['title']?>
                        </div>
                        <div class="benefits__content__block__text">
                            <?=$data['complect']['text']?>
                        </div>
                        <?php
                        foreach ($data['complect']['list'] as $datum) {
                            ?>
                            <div class="benefits__content__block__text">
                                <span class="block_1__bold"><?=$datum['item']?></span> <?=$datum['value']?>
                            </div>
                            <?php
                        }
                        ?>

                     
                    </div>
                </div>
            </div>
            <div class="benefits__content__column_2">

                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                           <?=$data['corpus']['title']?>
                        </div>
                        <ul>
                            <?php
                            foreach ($data['corpus']['list'] as $corpus) {
                                ?>
                                <li><?=$corpus['item']?></li>
                                <?php
                            }
                            ?>

                        </ul>
                    </div>
                </div>
                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            <?=$data['individual']['title']?>
                        </div>
                        <div class="benefits__content__block__text">
                            <?=$data['individual']['text']?>
                        </div>
                    </div>
                </div>
                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            <?=$data['garranty']['title']?>
                        </div>
                        <div class="benefits__content__block__text">
                            <?=$data['garranty']['text']?>
                        </div>
                    </div>
                </div>
                <div class="benefits__content__block benefits__content__row__column">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            <?=$data['lising']['title']?>
                        </div>
                        <div class="benefits__content__block__text">
                            <?=$data['lising']['text']?>
                        </div>
                        <button class="benefits__button" onclick="return location.href = '<?=$data['lising']['link']?>'"
                                class="">Подробнее
                        </button>
                    </div>

                </div>
                <div class="benefits__content__block">
                    <div class="benefits__content__block__information">
                        <div class="benefits__subtitle">
                            <?=$data['ergonomic']['title']?>
                        </div>
                        <div class="benefits__content__block__text">
                            <?=$data['ergonomic']['text']?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="peculiarities">
    <div class="peculiarities__container">
        <div class="peculiarities__title">
            <h2><?=$data['peculiarities_title']?></h2>
        </div>
        <?php
        foreach ($data['peculiarities'] as $i=>$peculiarity) {
            ?>
            <div class="peculiarities__blocks <?=($i==0)?"peculiarities__blocks__margin":""?>">
                <div class="peculiarities__row">
                    <div class="peculiarities__block">
                        <h3 class="peculiarities__block__subtitle">
                            <?=$peculiarity['title']?>
                        </h3>
                        <?=$peculiarity['text']?>
                    </div>
                    <div class="peculiarities__block__image">
                        <img src="<?=$peculiarity['image']['url']?>"
                             alt="<?=$peculiarity['image']['alt']?>"
                             title="<?=$peculiarity['image']['title']?>">
                    </div>
                </div>
            </div>
            <?php
        }
        ?>


    </div>
</section>
<section class="remote__control">
    <div class="remote__control__container">
        <div class="remote__control__title">
            <h2>
                <?=$data['remote_title']?>
            </h2>
        </div>
        <div class="remote__control__blocks">
            <div class="remote__control__block">
                <p>
                    <?=$data['remote_first_text']?>
                </p>
                <p class="remote__control__block__bold">
                    <?=$data['remote_text_title']?>
                </p>
                <?=$data['remote_text']?>
                <p class="remote__control__block__bold"> <?=$data['remote_title_after_text']?></p>
            </div>
            <div class="remote__control__block__image">
                <img src=" <?=$data['remote_image']['url']?>"
                     alt="<?=$data['remote_image']['alt']?>"
                     title="<?=$data['remote_image']['title']?>">
            </div>
        </div>
    </div>
</section>
<section class="loyalty__card">
    <div class="loyalty__card__container">
        <div class="loyalty__card__block">
            <h2 class="loyalty__card__title">
                <?=$data['card']['title']?>
            </h2>
            <p>
                <?=$data['card']['text']?>
            </p>
        </div>
        <div class="loyalty__card__block">
            <img src="<?=$data['card']['image']['url']?>"
                 alt="<?=$data['card']['image']['alt']?>"
                 title="<?=$data['card']['image']['title']?>">
        </div>
    </div>
</section>
<section class="feedbacks">
    <div class="feedbacks__container">
        <div class="feedbacks__block">
            <div class="feedbacks__title">
                <h2><?=$data['form_block']['title']?></h2>
            </div>
            <p class="feedbacks__text">
                Звоните <span class="feedbacks__text_big">  <?=$data['form_block']['phone']?></span> или отправляйте запрос на почту <span
                    class="feedbacks__text_big">  <?=$data['form_block']['email']?></span>.
            </p>
            <p class="feedbacks__text">
                <?=$data['form_block']['text']?>
            </p>
            <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function (w, d, u) {
                    var s = d.createElement('script');
                    s.async = true;
                    s.src = u + '?' + (Date.now() / 180000 | 0);
                    var h = d.getElementsByTagName('script')[0];
                    h.parentNode.insertBefore(s, h);
                })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
            <button class="feedbacks__btn">
                <?=$data['form_block']['button_text']?>
            </button>
        </div>
        <div class="feedbacks__block__image">
            <img class="feedbacks__image" src="<?=$data['form_block']['image']['url']?>"
                 alt="<?=$data['form_block']['image']['alt']?>"
                 title="<?=$data['form_block']['image']['title']?>">
        </div>
    </div>
</section>
<section class="additional__benefits">
    <div class="additional__benefits__container">
        <div class="additional__benefits__title">
            <h2>
               <?=$data['additional_title']?>
            </h2>
        </div>
        <div class="additional__benefits__subtitle">
            Воспользуйтесь услугой <span
                onclick="return location.href = '<?=$data['additional_benefots_link']?>'"
                class="font__blue"><?=$data['additional_benefots_title']?></span> Сделаем всё за вас.
        </div>
        <div class="additional__benefits__blocks">
            <div class="additional__benefits__row">
                <?php
                foreach ($data['additional_benefots'] as $i=>$additional_benefot) {
                    ?>
                    <div class="additional__benefits__block <?=($i==0)?"block__blue":""?> <?=($i>2)?"additional__benefits__block__big":""?>" >
                        <img src="<?=$additional_benefot['image']['url']?>" alt="">
                        <p><?=$additional_benefot['title']?></p>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
        <div class="additional__benefits__subtitle">
            <span class="font__blue"><?=$data['additional_benefots_under_text']?></span>
        </div>
    </div>
</section>
<section class="ready__business">
    <div class="ready__business__container">
        <h2 class="ready__business__title">
           <?=$data['ready_title']?>
        </h2>
        <div class="ready__business__content">
            <div class="ready__business__block">
                <div class="ready__business__subtitle">
                    Купите <span
                        onclick="return location.href = '<?=$data['ready_link']?>'"
                        class="text_blue"><?=$data['ready_text']?></span>
                </div>
                <?php
                foreach ($data['ready_list'] as $i=>$datum) {
                    ?>
                    <div class="ready__business__block__row">
                        <div class="ready__business__block__number"><?=$i+1?></div>
                        <div class="ready__business__block__li">
                           <?=$datum['item']?>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
            <div class="ready__business__block__image">
                <img class="ready__business__image" src="<?=$data['image']['url']?>"
                     alt="">
            </div>
        </div>

    </div>
</section>

<script src="<?php bloginfo("template_url"); ?>/js/cost.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/gallery.js"></script>

<?php get_footer(); ?>
