<?php

/**
 * Template Name: 500na700 page -2
 	Франшиза мойки самообслуживания NERTA SW
 */
get_header();
?>
<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/page_2.css">

<section class="main progress-bar-start" style="background: url(<?php bloginfo("template_url"); ?>/img/page_2/main-min.jpg) no-repeat center center / cover">
	<div class="main__container">

		<div class="breadcrumbs">
			<?php the_breadcrumb() ?>
		</div>

		<h1>Франшиза мойки самообслуживания NERTA SW</h1>
		<div class="main__blocks">
			<p>Без паушальных взносов и роялти</p>
			<p>Более 85 успешно работающих проектов</p>
			<p>Комплексная маркетинговая и информационная поддержка</p>
			<p>Окупаемость <br> 14-24 месяца</p>
			<p>Подходит <br> для любых городов России и стран СНГ</p>
		</div>
	</div>
</section>


<section class="description" id="description">
	<div class="description__container">
		<h2>Франшиза мойки самообслуживания под ключ от компании <span>Nerta-SW</span>, это:</h2>

		<div class="description__content">
			<div class="description__list">
				<div>
					<span>1</span>
					<p>Удалённый мониторинг <br> и контроль оборудования.</p>
				</div>
				<div>
					<span>2</span>
					<p>Обучение <br> персонала.</p>
				</div>
				<div>
					<span>3</span>
					<p>Сервисное обслуживание <br> и ремонт оборудования.</p>
				</div>
				<div>
					<span>4</span>
					<p>Поиск наиболее эффективных методов продвижения с учетом особенностей региона. Предоставление контактов опытных специалистов, занимающихся продвижением данного бизнеса. </p>
				</div>
				<div>
					<span>5</span>
					<p>Поставка расходных материалов, адаптированных под используемое <br> оборудование.</p>
				</div>
			</div>
			<div class="description__image">
				<img alt="франшиза мойка Nerta-SW" title="Франшиза самомойки без роялти и паушального взноса от компании Nerta-SW" src="data:image/gif;base64,R0lGODlhpwBkAIAAAP///wAAACH5BAEAAAEALAAAAACnAGQAAAKdjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubqztTAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/description-min.jpg">
				<span>Честная франшиза Nerta-SW</span>
			</div>
		</div>
		<h3>В компании Nerta-SW услуга открытия МСО по франчайзингу называется <span> «мойка самообслуживания под ключ».</span></h3>
	</div>
</section>

<div>
	<div class="mobile-tab id_mobile-tab">стоимость открытия мойки самообслуживания по франшизе <span>nerta-sw</span></div>
	<section class="cost" id="cost">
		<div class="cost__container">
			<h2>Стоимость открытия мойки самообслуживания по франшизе <span>Nerta-SW</span></h2>
			<div class="cost__content">
				<div class="cost__text">
					<p>Франшиза компании Nerta-SW включает полный спектр работ от помощи в подборе подходящего участка до сервисного обслуживания МСО. Бюджет проекта состоит из трёх ключевых статей расходов: </p>
					<span>Цена земельного участка (аренды или покупки). Данные достаточно сильно отличаются в зависимости от региона;</span>
					<span>Цена строительства объекта — от 2 000 000 млн. р. ;</span>
					<span>Цена оборудования — от 1 000 000 млн.р.</span>
					<p>Срок строительства МСО «под ключ» составляет около 4-6 месяцев и включает в себя все этапы, от разработки проекта до введения в эксплуатацию готового объекта. Срок выполнения может меняться как в большую, так и в меньшую сторону, исходя из масштаба и сложности проекта.</p>
				</div>
				<div class="cost__image">
					<img src="data:image/gif;base64,R0lGODlhpwBkAIAAAP///wAAACH5BAEAAAEALAAAAACnAGQAAAKdjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubqztTAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/cost-min.jpg" alt="франшиза мойка" title="Франшиза самомойки без роялти и паушального взноса от компании Nerta-SW">
					<span>Самомойка от компании Nerta-SW</span>
				</div>
			</div>
		</div>
	</section>
</div>

<section class="help">
	<div class="help__container">
		<p>Чтобы купить франшизу мойки самообслуживания <br> и узнать ориентировочную цену выполнения <br> проекта – позвоните по номеру </p>
		<a href="tel:+74956777000">+7 (495) 6-777-000 </a>
		<span>Наши специалисты проконсультируют,<br> ответят на интересующие вопросы.</span>
	</div>
</section>

<div>
	<div class="mobile-tab id_mobile-tab">Франшиза мойка самообслуживания «под ключ» от компании <span>nerta-sw</span></div>
	<section class="info">
		<div class="info__container">
			<h2>Франшиза мойка самообслуживания «под ключ» от компании <span>Nerta-SW</span></h2>
			<div class="info__content">
				<div class="info__block">
					<img src="data:image/gif;base64,R0lGODlhpwBkAIAAAP///wAAACH5BAEAAAEALAAAAACnAGQAAAKdjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubqztTAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/info_1-min.jpg" alt="франшиза на мойку самообслуживания" title="Франшиза от производителя самомоек - Nerta-SW">
					<span class="info__block__sign">Собственное производство</span>
					<h3>Все указанные выше работы предоставляются бесплатно.</h3>
					<p>Нашим партнерам не надо платить первоначальный паушальный взнос и ежемесячный роялти. Почему мы делимся наработками и опытом бесплатно? Компания Nerta-SW зарабатываетна изготовлении оборудования. <br><br>Собственное производство позволяет вести конкурентную политику цен, обеспечивать действительно интересные условия для партнеров.</p>
				</div>
				<div class="info__block">
					<img src="data:image/gif;base64,R0lGODlhpwBkAIAAAP///wAAACH5BAEAAAEALAAAAACnAGQAAAKdjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubqztTAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/info_2-min.jpg" alt="мой сам оборудование цена" title="Франшиза от производителя моечного оборудования - Nerta-SW">
					<span class="info__block__sign">Производство идивидуальных заказов</span>
					<h3>Если нет достаточных средств для запуска бизнеса, находим альтернативные способы его открытия:</h3>
					<span class="info__block__list">Помощь в получении лизинга. Работаем с лизинговой компанией Siemens Finance. Являемся одобренным поставщиком и предлагаем интересные условия получения лизинга.</span>
					<span class="info__block__list">Совместные инвестиции. Кроме предоставления помощи в получении лизинга открыты к сотрудничеству в формате долевого партнерства. Разрабатываем программу инвестирования, учитывая пожелания конкретного партнера.</span>
				</div>
			</div>

		</div>
	</section>
</div>

<section class="freedom">
	<div class="freedom__container">
		<div class="freedom__info">
			<h2>Обеспечиваем полную свободу развития и принятия решений!</h2>
			<p>Если нужна франшиза мойки самообслуживания, то стоимость — далеко не ключевой фактор, на который стоит обращать внимание. Важным отличием сотрудничества с компанией Nerta-SW выступает возможность выбора и самостоятельного развития своего бизнеса. Мы даём качественную поддержку, делимся опытом, но итоговые управленческие решения принимает партнер. В итоге обеспечивается дополнительная мотивация, стремление к экспериментам для увеличения доходности бизнеса. </p>
		</div>
		<div class="freedom__image">
			<img src="data:image/gif;base64,R0lGODlhpwBkAIAAAP///wAAACH5BAEAAAEALAAAAACnAGQAAAKdjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubqztTAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/freedom-min.jpg" alt="мойка самообслуживания под ключ франшиза" title="Франшиза от производителя моек самообслуживания - Nerta-SW">
			<span>Автомойка самообслуживания Nerta-SW</span>
		</div>
	</div>
</section>

<div>
	<div class="mobile-tab id_mobile-tab">Как быстро окупаются вложения, как построить эффективный бизнес?</div>
	<section class="fast" id="fast">
		<div class="fast__container">
			<h2>Как быстро окупаются вложения, как построить эффективный бизнес?</h2>
			<div class="fast__content">
				<p>Точно, и с конкретными цифрами, ответить на указанные вопросы вряд ли получится. Успех возможен только при кропотливой, усердной работе. Nerta-SW не предлагает волшебную таблетку или священный Грааль. Мы устанавливаем необходимое оборудование, даём актуальную информацию, практические рекомендации, которые помогут сформировать успешный бизнес.<br><br>Да, можно подготовить калькулятор доходности, где будет рассчитываться доходность с учетом количества моечных постов, загрузки мойки, среднего чека, других факторов. Но в реальности всё сложнее. Уровень конкуренции, фактическое расположение объекта, эффективность рекламной кампании – эти критерии непосредственно влияют на время, в течении которого, окупаются инвестиции в бизнес. <br><br> На сегодняшний день, мы реализовали более 85 проектов. Они отличаются по региональному расположению, уровню сложности и другим критериям. Стабильное увеличение количества построенных объектов свидетельствует о перспективности автомоечного бизнеса.</p>
				<div class="fast__content__image">
					<img src="data:image/gif;base64,R0lGODlhpwBkAIAAAP///wAAACH5BAEAAAEALAAAAACnAGQAAAKdjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6ipqqusra6voKGys7S1tre4ubqztTAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/fast-min.jpg" alt="Франшиза автомойки - бесплатная консультация" title="Франшиза от Nerta-SW - консультация от собственника">
					<span>Персональные консультации</span>
				</div>
			</div>
		</div>
	</section>
</div>

<section class="budget">
	<div class="budget__container">
		<h2>Как рационально использовать имеющийся бюджет?</h2>
		<div class="budget__text">
			<p>Не рекомендуем вкладывать в сомнительные франшизы, обладающие неоднозначной репутацией. Важно сразу подобрать надежное и функциональное оборудование, выбрать оптимальное количество моечных постов. </p>
			<p>На этой странице представлены способы открытия бизнеса в сфере моек самообслуживания. Каждый из способов отличается определенными особенностями. Для наглядности предлагаем систематизировать информацию в сравнительную таблицу, дополнив её самостоятельным строительством МСО.</p>
		</div>
		<div class="budget__table">
			<div class="budget__table__main">
				<span>Уплата паушального взноса</span>
				<span>Ежемесячная уплата роялти</span>
				<span>Свобода ведения бизнеса, принятия управленческих решений</span>
				<span>Информационная, маркетинговая поддержка</span>
				<span>Сложность входа в бизнес</span>
			</div>
			<div class="budget__table__columns">
				<div class="budget__table__column">
					<div>Франшиза <br> МСО <p>NERTA SW</p>
					</div>
					<span>нет</span>
					<span>нет</span>
					<span>да</span>
					<span>да</span>
					<span>Минимальная</span>
				</div>
				<div class="budget__table__column">
					<div>Стандартная <br> франшиза</div>
					<span class="dark">да</span>
					<span class="dark">да</span>
					<span class="dark">нет</span>
					<span>да</span>
					<span>Минимальная</span>
				</div>
				<div class="budget__table__column">
					<div>Самостоятельное строительство МСО</div>
					<span>нет</span>
					<span>нет</span>
					<span>да</span>
					<span class="dark">нет</span>
					<span class="dark">Высокая</span>
				</div>
			</div>
		</div>
		<span class="budget__sign">Таблица: мойка самообслуживания как открыть бизнес</span>
		<p class="budget__bottom">Франшиза МСО Nerta-SW и стандартная франшиза отличаются преимуществами перед самостоятельным открытием бизнеса. Начинающие предприниматели получают шанс избежать ошибок, которые приводят к внушительным денежным потерям. Плюс к этому, новичку трудно конкурировать с другими профессиональными участниками, которые уже давно на рынке.</p>
	</div>
</section>

<section class="distribution">
	<div class="distribution__container">
		<h2>Распределение валовой выручки мойки самообслуживания</h2>
		<div class="distribution__content">
			<div class="distribution__graph">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/distribution_1.svg" alt="автомойка самообслуживания рентабельность" title="Рентабельность моек самообслуживания Nerta-SW">
				<span>Диаграмма: доходы и расходы МСО</span>
			</div>
			<div class="distribution__info">
				<p>Как правило, 60-70% от полученных средств являются чистыми доходами бизнеса. При этом, оставшиеся 30-40% это текущие расходы, которые преимущественно распределяются таким образом:</p>
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/distribution_2.png">
				<img class="mobile" data-src="<?php bloginfo("template_url"); ?>/img/page_2/distribution_2_mobile.svg">
				<p>8-12% - расходные материалы (шампуни, воск и другие моющие средства); <br> 7-10% - оплата труда администратора и других сотрудников, <br> при использовании наёмных работников; 10-15% - коммунальные платежи (свет, вода, сброс стоков и т.д.);<br> 1-6% - налоговые отчисления зависимо от выбранной формы налогообложения;<br> 5% - другие платежи и взносы, включая расходы в случае непредвиденных <br>форсмажорных обстоятельств (ремонт выявленных поломок, сезонное <br>сервисное обслуживание оборудования и т.д.).</p>
			</div>
		</div>
	</div>
</section>

<div>
	<div class="mobile-tab id_mobile-tab">Условия строительства мойки самообслуживания</div>
	<section class="conditions" id="conditions">
		<div class="conditions__container">
			<h2>Условия строительства мойки самообслуживания</h2>

			<div class="conditions__content">
				<div class="conditions__info">
					<p>Для заказа в компании Nerta-SW возведение комплекса и запуск бизнеса, клиент должен соответствовать ряду критериев: </p>
					<span>Наличие средств для выполнения проекта, либо готовность к лизингу или долевому партнерству;</span>
					<span>Земельный участок в собственности или долгосрочной аренде (свободная площадь в спальном районе, на въезде в город, на парковке в торговом центре и т.д., либо обычная мойка, которую можно переоборудовать в МСО);</span>
					<span>Подведенные к участку инженерные коммуникации (вода, электричество, канализация);</span>
					<span>Готовность к плодотворной работе по развитию бизнеса.</span>
				</div>
				<div class="conditions__image">
					<img src="data:image/gif;base64,R0lGODlhpwBPAIAAAP///wAAACH5BAEAAAEALAAAAACnAE8AAAKIjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWspZAAA7" data-src="<?php bloginfo("template_url"); ?>/img/page_2/conditions-min.jpg" alt="Франшиза мойки самообслуживания под ключ - строительство" title="Франшиза от Nerta-SW - Бизнес под ключ">
					<span>Франшиза мойка самообслуживания под ключ</span>
				</div>
			</div>
		</div>
	</section>
</div>

<section class="work progress-bar-end">
	<div class="work__container">
		<h2>Работаем «под ключ» так, чтобы нас могли рекомендовать друзьям!</h2>
		<div class="work__content">
			<p class="work__content__text">Этот тезис касается как обеспечения качества работы возводимых моек самообслуживания, так и сотрудничества с партнерами.<br><br> По накопленной за 20 лет статистике, 70% выполненных фирмой проектов — это повторные заказы. 20% заказов мы получаем по рекомендациям партнеров.<br><br> Компания Nerta-SW ценит полученное доверие клиентов и делает всё, чтобы сформировать оптимальные условия для долгосрочной 70% совместной работы. </p>
			<div class="work__content__image">
				<img src="data:image/gif;base64,R0lGODlhnwBVAIAAAP///wAAACH5BAEAAAEALAAAAACfAFUAAAKKjI+py+0Po5y02ouz3rz7D4biSJbmiabqyrbuC8fyTNf2jef6zvf+DwwKh8Si8YhMKpfMpvMJjUqn1Kr1is1qt9yu9wsOi8fksvmMTqvX7Lb7DY/L5/S6/Y7P6/f8vv8PGCg4SFhoeIiYqLjI2Oj4CBkpOUlZaXmJmam5ydnp+QkaKjpKWmp6mlkAADs=" data-src="<?php bloginfo("template_url"); ?>/img/page_2/work.svg" alt="Виды клиентов компании Nerta-SW" title="Клиентская база компании по производству моек самообслуживания под ключ Nerta-SW">
				<img class="mobile" data-src="<?php bloginfo("template_url"); ?>/img/page_2/work_mobile.svg" alt="Виды клиентов компании Nerta-SW" title="Клиентская база компании по производству моек самообслуживания под ключ Nerta-SW">
				<span>Диаграмма: наши клиенты</span>
			</div>
		</div>
	</div>
</section>

<section class="prices">
	<div class="prices__container">
		<h2>Франшиза мойки самообслуживания «под ключ» — цена, окупаемость, расходы</h2>
		<div class="prices__subtitle">Предлагаем сравнить предложение компании «Nerta-SW» с условиями конкурентов:</div>
		<div class="prices__table">
			<div class="prices__table__main">
				<span>Nerta-SW</span>
				<span>Park&Go</span>
				<span>DIGITAL MOIKA</span>
				<span>MOIKA BLESK</span>
				<span>Wonder Wash</span>
				<span>Сухомой</span>
			</div>
			<div class="prices__table__columns">
				<div class="prices__table__column">
					<div>Затраты <br> на открытие</div>
					<span>От 3 000 000 <b class="rub">&nbsp;₽</b></span>
					<span>550 000 <b class="rub">&nbsp;₽</b></span>
					<span>900 000 <b class="rub">&nbsp;₽</b></span>
					<span>250 000 <b class="rub">&nbsp;₽</b></span>
					<span>900 000 <b class="rub">&nbsp;₽</b></span>
					<span>830 000 <b class="rub">&nbsp;₽</b></span>
				</div>
				<div class="prices__table__column">
					<div>Паушальный<br>взнос</div>
					<span>Отсутствует</span>
					<span>Отсутствует</span>
					<span>490 000 <b class="rub">&nbsp;₽</b></span>
					<span>250 000 <b class="rub">&nbsp;₽</b></span>
					<span>Отсутствует</span>
					<span>Отсутствует</span>
				</div>
				<div class="prices__table__column">
					<div>Всего <br> затрат</div>
					<span>От 3 000 000 <b class="rub">&nbsp;₽</b></span>
					<span>550 000 <b class="rub">&nbsp;₽</b></span>
					<span>1 390 000 <b class="rub">&nbsp;₽</b></span>
					<span>500 000 <b class="rub">&nbsp;₽</b></span>
					<span>900 000 <b class="rub">&nbsp;₽</b></span>
					<span>830 000 <b class="rub">&nbsp;₽</b></span>
				</div>
				<div class="prices__table__column">
					<div>Окупаемость</div>
					<span>15-24 месяцев</span>
					<span>7 месяцев</span>
					<span>23 месяца</span>
					<span>3 месяца</span>
					<span>14 месяцев</span>
					<span>16 месяцев</span>
				</div>
				<div class="prices__table__column">
					<div>Роялти</div>
					<span>Отсутствует</span>
					<span>3%</span>
					<span>Отсутствует</span>
					<span>Отсутствует</span>
					<span>3 000 <b class="rub">&nbsp;₽</b> в месяц</span>
					<span>Отсутствует</span>
				</div>
			</div>
		</div>
		<span class="prices__sign">Таблица: сравнение франшиз МСО</span>
		<div class="prices__bottom">
			<p>Различия в ценах обусловлены комплектацией, надежностью и качеством устанавливаемого оборудования. Некоторые компании осознанно занижают стоимость строительство МСО, экономя на строительстве и используемых моечных устройствах. К сожалению, редко подобная экономия становится оправданной. В большинстве случаев она приводит к уменьшению срока эксплуатации оборудования и необходимости регулярного ремонта мойки самообслуживания. </p>
			<p>Многие компании лукавят для привлечения новых франчайзи, указывая заниженные сроки окупаемости. В каждом конкретном случае срок окупаемости будет индивидуальным с учетом существующего уровня конкуренции, расположения участка и т.д. Поэтому трудно вывести универсальную цифру, которая будет подходить для каждого проекта.</p>
		</div>
	</div>
</section>

<section class="build">
	<div class="build__container">
		<h2>Строительство мойки самообслуживания — франшиза,цена и способ заказа</h2>
		<div class="build__subtitle">Процесс сотрудничества можно разделить на 5 основных этапов:</div>
		<div class="build__blocks">
			<div class="build__block">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_img_1.svg">
				<div>Оформление запроса</div>
				<p>Специалисты компании Nerta-SW выполнят анализ рентабельности бизнеса в Вашем регионе, предоставят консультации и помощь в решении существующих вопросов.</p>
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_1.svg">
			</div>
			<div class="build__block second">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_img_2.svg">
				<div>Предоставление плана участка </div>
				<p>На основании полученной иформации эксперты нашей фирмы подготовят рекомендации по наиболееэффективному выполнению проекта.</p>
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_2.svg">
			</div>
			<div class="build__block">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_img_3.svg">
				<div>Строительство мойки самообслуживания</div>
				<p>Выполняем строительные работы со строгим соблюдением существующих строительных и других стандартов.</p>
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_3.svg">
			</div>
		</div>
		<div class="build__blocks">
			<div class="build__block four">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_img_4.svg">
				<div>Монтаж оборудования</div>
				<p>Устанавливаем моечные комплексы и выполняем обучение <br> персонала. </p>
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_4.svg">
			</div>
			<div class="build__block five">
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_img_5.svg">
				<div>Начало бизнеса</div>
				<p>После получения пакета разрешительной документации осуществляется официальный запуск МСО, организовывается рекламная кампания по привлечению клиентов. </p>
				<img data-src="<?php bloginfo("template_url"); ?>/img/page_2/build_5.svg">
			</div>
		</div>
	</div>
</section>

<section class="form">
	<div class="form__container">
		<div class="form__block">
			<?php echo do_shortcode('[contact-form-7 id="10561" title="для страница 500na700 page-2"]'); ?>
		</div>
		<p class="form__text">
			<span>Заполните форму заявки</span>
			на этой странице и cпециалисты компании Nerta-SW оперативно свяжутся с Вами для подготовки
			<span>коммерческого предложения и начала работ</span>
			по открытию нового прибыльного бизнеса!
		</p>
	</div>
</section>

<div class="progress-bar" id="progress-bar"></div>



<?php get_footer(); ?>

<script src="https://cdn.jsdelivr.net/npm/lazyload@2.0.0-rc.2/lazyload.js"></script>
<script src="<?php bloginfo("template_url"); ?>/js/page_2.js"></script>