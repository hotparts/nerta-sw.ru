<?php
/**
 * Template Name: Serv
 */

get_header(); ?>
    <div class="inner-header">
        <div class="container">
            <div class="inner-header-company">Nerta Selfwash</div>
            <?php the_title( '<h1 class="h1">', '</h1>' );?>
        </div>
    </div>
    <div class="services">
        <div class="container">
            <div class="services-text about-text">
                <h2 class="title">Переоборудование</h2>
                <p>Производим демонтаж старого оборудования, подготавливаем помещение
                    к&nbsp;монтажу и&nbsp;переоборудованию под мойку самообслуживания.
                    Возможно переоборудовать только часть мойки под самообслуживание, также возможно использование существующего оборудования.</p>
            </div>
        </div>
        <br>
        <div class="gallery-item">
            <div class="nerta-metaslider">
                <?php echo do_shortcode( '[metaslider id=384]' ); ?>
            </div>
        </div>
        <br>
        <br>
        <div class="container">
            <div class="services-feedback"><div class="feedback-content"> <?php echo do_shortcode( '[contact-form-7 id="12" title="Callback"]' ); ?></div></div>
            <div class="services-text">
                <?php
                the_post();
                the_content();
                ?>
            </div>
        </div>
    </div>
    <div class="gallery-list">
        <div class="gallery-item">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-service">Проектирование</div>
                        <div class="gallery-switcher">
                            <span class="active">Проектирование</span>
                            <span>Строительство</span>
                            <span>Металлоконструкция</span>
                            <span>Производство</span>
                            <span>Монтаж</span>
                            <span>Реклама&nbsp;и&nbsp;продвижение</span>
                        </div>
                        <p><strong>Проектирование от&nbsp;50&nbsp;000 рублей за&nbsp;комплекс</strong></p>
                    </div>
                </div>
                <?php echo do_shortcode( '[metaslider id=379]' ); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-service">Строительство</div>
                        <div class="gallery-switcher">
                            <span class="active">Проектирование</span>
                            <span>Строительство</span>
                            <span>Металлоконструкция</span>
                            <span>Производство</span>
                            <span>Монтаж</span>
                            <span>Реклама&nbsp;и&nbsp;продвижение</span>
                        </div>
                        <p><strong>Строительство от&nbsp;300&nbsp;000 рублей из&nbsp;расчета за&nbsp;пост</strong></p>
                    </div>
                </div>
                <?php echo do_shortcode( '[metaslider id=380]' ); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-service">Металлоконструкция</div>
                        <div class="gallery-switcher">
                            <span class="active">Проектирование</span>
                            <span>Строительство</span>
                            <span>Металлоконструкция</span>
                            <span>Производство</span>
                            <span>Монтаж</span>
                            <span>Реклама&nbsp;и&nbsp;продвижение</span>
                        </div>
                        <p><strong>Металлоконструкция от&nbsp;75&nbsp;000 рублей за&nbsp;1&nbsp;пост</strong></p>
                    </div>
                </div>
                <?php echo do_shortcode( '[metaslider id=381]' ); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-service">Производство</div>
                        <div class="gallery-switcher">
                            <span class="active">Проектирование</span>
                            <span>Строительство</span>
                            <span>Металлоконструкция</span>
                            <span>Производство</span>
                            <span>Монтаж</span>
                            <span>Реклама&nbsp;и&nbsp;продвижение</span>
                        </div>
                        <p><strong>Производство&nbsp;&mdash; стоимость оборудования от&nbsp;350&nbsp;000 рублей за&nbsp;1&nbsp;пост</strong></p>
                    </div>
                </div>
                <?php echo do_shortcode( '[metaslider id=382]' ); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-service">Монтаж</div>
                        <div class="gallery-switcher">
                            <span class="active">Проектирование</span>
                            <span>Строительство</span>
                            <span>Металлоконструкция</span>
                            <span>Производство</span>
                            <span>Монтаж</span>
                            <span>Реклама&nbsp;и&nbsp;продвижение</span>
                        </div>
                        <p><strong>Монтаж от&nbsp;150&nbsp;000 рублей за&nbsp;комплекс</strong></p>
                    </div>
                </div>
                <?php echo do_shortcode( '[metaslider id=383]' ); ?>
            </div>
        </div>
        <div class="gallery-item" style="display: none;">
            <div class="nerta-metaslider">
                <div class="slider-text">
                    <div class="container">
                        <div class="slider-text-service">Реклама и&nbsp;продвижение</div>
                        <div class="gallery-switcher">
                            <span class="active">Проектирование</span>
                            <span>Строительство</span>
                            <span>Металлоконструкция</span>
                            <span>Производство</span>
                            <span>Монтаж</span>
                            <span>Реклама&nbsp;и&nbsp;продвижение</span>
                        </div>
                        <?//<p><strong>Стоимость строительных работ от&nbsp;300&nbsp;000 рублей из&nbsp;расчёта на&nbsp;1&nbsp;пост</strong></p> ?>
                    </div>
                </div>
                <?php echo do_shortcode( '[metaslider id="673"]' ); ?>
            </div>
        </div>
    </div>
<?php get_footer(); ?>