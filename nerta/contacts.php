<?php
/**
 * Template Name: Contacts
 */
get_header();
$data = get_fields();
?>
    <div id="nerta-main-page">
        <section class="pageBanner without-description">
            <div class="inner">
                <picture class="pageBanner__bg">
                    <source srcset="<?= $data['banner']['image'] ?>.webp" type="image/webp">
                    <img src="<?= $data['banner']['image'] ?>" alt="Баннер">
                </picture>
                <div class="pageBanner__content">
                    <h1 class="pageBanner__title"><?= $data['banner']['title'] ?></h1>
                </div>
                <div class="breadcrumbs"><a href="https://nerta-sw.ru">Главная</a><span>Контакты</span></div>
            </div>
        </section>
        <section class="contacts">
            <div class="inner">
                <?php foreach ($data['office'] as $item) : ?>
                    <div class="contacts__item">
                        <div class="office"><?= $item['title'] ?></div>
                        <div class="contacts__itemLeft">
                            <?php foreach ($item['info'] as $info) : ?>
                                <div class="info-item">
                                    <div class="info-name"><?= $info['title'] ?></div>
                                    <?php foreach ($info['values'] as $values) : ?>
                                        <?php if (strpos($info['title'], 'Телефоны') !== false): ?>
                                            <div class="info-value"><a href="tel:<?=$values['value'] ?>"><?= $values['value'] ?></a></div>
                                        <?php elseif(strpos($info['title'], 'Email') !== false): ?>
                                            <div class="info-value"><a href="mailto:<?=$values['value'] ?>"><?= $values['value'] ?></a></div>
                                        <?php else: ?>
                                            <div class="info-value"><a><?= $values['value'] ?></a></div>
                                        <?php endif; ?>
                                    <?php endforeach; ?>
                                </div>
                            <?php endforeach; ?>
                        </div>
                        <div class="contacts__itemRight">
                            <div class="map-container">
                                <iframe loading="lazy" src="<?= $item['map'] ?>" allowfullscreen="true" width="100%"
                                        height="100%" frameborder="1"></iframe>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </section>
        <section class="informcard">
            <div class="inner">
                <div class="informcard__title">ИНФОРМАЦИОННАЯ КАРТОЧКА<br> ООО «КОМПАНИЯ «НЕРТА»</div>
                <div class="informcard__table">
                    <?php foreach ($data['info'] as $i => $info) : ?>
                        <div class="informcard__row <?= (($i + 1) % 2 != 0 ? "odd" : "") ?>">
                            <div class="name"><?= $info['name'] ?></div>
                            <div class="value"><?= $info['value'] ?></div>
                        </div>

                    <?php endforeach; ?>
                </div>
            </div>
        </section>
        <section class="formQuestions formContacts">
            <div class="inner">
                <div class="formQuestions__left">
                    <div class="formQuestions__title">ОСТАЛИСЬ ВОПРОСЫ?</div>
                    <div class="formQuestions__desc">Звоните или оставляйте заявку на сайте. Мы ответим на все
                        интересующие вас вопросы!
                    </div>
                </div>
                <div class="formQuestions__right">
                    <form class="form-component formQuestions__form"
                          action="/wp-json/contact-form-7/v1/contact-forms/14768/feedback" method="POST"
                          data-form="banner">
                        <div class="form__field">
                            <input type="text" name="your-name" placeholder="Как к вам обращаться?" required>
                            <span class="form__field--caption"></span>
                        </div>
                        <div class="form__field">
                            <input type="tel" name="your-phone" placeholder="Телефон" required>
                            <span class="form__field--caption"></span>
                        </div>
                        <div class="form__field">
                            <input type="email" name="email" placeholder="Куда направлять ответ?" required>
                            <span class="form__field--caption"></span>
                        </div>
                        <div class="form__field">
                            <input type="text" name="where" placeholder="Где планируете мойку?" required>
                            <span class="form__field--caption"></span>
                        </div>
                        <div class="form__field">
                            <input type="text" name="what" placeholder="Чем мы можем вам помочь?" required>
                            <span class="form__field--caption"></span>
                        </div>
                        <div class="form__submit">
                            <button type="submit">Отправить</button>
                        </div>
                        <div class="form__accept">
                            <label>
                                <input type="checkbox" name="acceptance-793"><span>я принимаю <a href="#">политику конфиденциальности</a></span>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </section>
    </div>


<?php get_footer(); ?>