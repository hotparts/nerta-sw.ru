<?php
/**
 * Template Name: vacuum cleaner
 */
get_header(); ?>

<link rel="stylesheet" type="text/css" href="<?php bloginfo("template_url"); ?>/css/vacuum_cleaner.css">
<div id="nerta-main-page">

    <section class="main"
             style="background-image: url('<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/main_cleaner_page.jpg')">
        <div class="main__container">
            <div class="breadcrumbs">
                <?php the_breadcrumb() ?>
            </div>
            <h1 class="main__title">
                ПЫЛЕСОСЫ <br>САМООБСЛУЖИВАНИЯ <br>NERTA-SW
            </h1>
            <div class="main__description">
                <p class="main__description-elem">
                    Увеличивает посещаемость мойки до 20%
                </p>
                </p>
                <p class="main__description-elem">
                    Приносит до 100 000 ₽ в месяц
                </p>
                <p class="main__description-elem">
                    Полностью автономен, управляется удалённо
                </p>
            </div>
        </div>
    </section>

    <section class="contractor">
        <div class="contractor__container">
            <div class="contractor__title">
                <h2>
                    Пылесос самообслуживания – выгодное приобретение
                </h2>
            </div>
            <div class="contractor__blocks">
                <div class="contractor__column">
                    <div class="contractor__block">
                        <div class="contractor__number">1</div>
                        <div class="contractor__text">
                            <p>
                                Поддерживает все виды платежей
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">2</div>
                        <div class="contractor__text">
                            <p>
                                Минимальные затраты на обслуживание
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">3</div>
                        <div class="contractor__text">
                            <p>
                                Устанавливается в протирочной зоне
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">4</div>
                        <div class="contractor__text">
                            <p>
                                Занимает площадь не более 0,8 кв.м
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">5</div>
                        <div class="contractor__text">
                            <p>
                                Химчистка самообслуживания (доп. опция)
                            </p>
                        </div>
                    </div>
                    <div class="contractor__block">
                        <div class="contractor__number">6</div>
                        <div class="contractor__text">
                            <p>
                                Подкачка шин и воздух (доп. опция)
                            </p>
                        </div>
                    </div>
                </div>
                <div class="contractor__column__image">
                    <img alt="Пылесосы самообслуживания Nerta-SW"
                         title="Производство пылесосов самообслуживания Nerta-SW"
                         src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/21.png">
                </div>
            </div>
        </div>
    </section>

    <section class="dop__equipment dop__equipment__display">
        <div class="dop__equipment__container">
            <div class="dop__equipment__title">
                <h2>
                    Пылесосы самообслуживания – 220 вольт
                </h2>
            </div>
            <div class="dop__equipment__row">
                <div class="dop__equipment__column">
                    <img src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/22.png"
                         alt="Пылесос самообслуживания 220 вольт Nerta-SW"
                         title="Пылесос самообслуживания на 1 пост, 220 вольт от Nerta-SW">
                    <div class="dop__equipment__column__title">
                        <p>пылесос на 1 ПОСТ</p>
                    </div>
                    <div class="dop__equipment__column__list">
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Мощность</div>
                            <div class="dop__equipment__column__list-elem2">3х1,2 кВт</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Производительность</div>
                            <div class="dop__equipment__column__list-elem2">180 м3/час</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Занимаемая площадь</div>
                            <div class="dop__equipment__column__list-elem2">500х1000 мм</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Расчётная система</div>
                            <div class="dop__equipment__column__list-elem2">Google Pay, Apple Pay, Samsung Pay, Visa,
                                Mastercard, наличные
                            </div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Цена</div>
                            <div class="dop__equipment__column__list-elem3">от 210 000 ₽</div>
                        </div>
                    </div>
                    <button>Узнать подробнее</button>
                </div>
                <div class="dop__equipment__column">
                    <img src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/23.png"
                         alt="Купить пылесос самообслуживания 220 вольт Nerta-SW"
                         title="Пылесос самообслуживания на 2 поста, 220 вольт от Nerta-SW">
                    <div class="dop__equipment__column__title">
                        <p>пылесос на 2 ПОСТа</p>
                    </div>
                    <div class="dop__equipment__column__list">
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Мощность</div>
                            <div class="dop__equipment__column__list-elem2">6х1,2 кВт</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Производительность</div>
                            <div class="dop__equipment__column__list-elem2">2х180 м3/час</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Занимаемая площадь</div>
                            <div class="dop__equipment__column__list-elem2">600х1200 мм</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Расчётная система</div>
                            <div class="dop__equipment__column__list-elem2">Google Pay, Apple Pay, Samsung Pay, Visa,
                                Mastercard, наличные
                            </div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Цена</div>
                            <div class="dop__equipment__column__list-elem3">от 380 000 ₽</div>
                        </div>
                    </div>
                    <button>Узнать подробнее</button>
                </div>
                <div class="dop__equipment__column">
                    <img src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/24.png"
                         alt="Мойка самообслуживания с химчисткой Nerta-SW"
                         title="Химчистка + пылеводосос, 220 вольт от Nerta-SW">
                    <div class="dop__equipment__column__title">
                        <p>химчистка + пылеводосос</p>
                    </div>
                    <div class="dop__equipment__column__list">
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Мощность</div>
                            <div class="dop__equipment__column__list-elem2">6х1,2 кВт</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Производительность</div>
                            <div class="dop__equipment__column__list-elem2">2х180 м3/час</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Занимаемая площадь</div>
                            <div class="dop__equipment__column__list-elem2">500х1000 мм</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Расчётная система</div>
                            <div class="dop__equipment__column__list-elem2">Google Pay, Apple Pay, Samsung Pay, Visa,
                                Mastercard, наличные
                            </div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Цена</div>
                            <div class="dop__equipment__column__list-elem3">от 320 000 ₽</div>
                        </div>
                    </div>
                    <button>Узнать подробнее</button>
                </div>
            </div>
        </div>
    </section>

    <section class="dop__equipment dop__equipment__display">
        <div class="dop__equipment__container">
            <div class="dop__equipment__title">
                <h2>
                    Пылесосы самообслуживания – 380 вольт
                </h2>
            </div>
            <div class="dop__equipment__row">
                <div class="dop__equipment__column">
                    <div class="">
                        <img src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/25.png"
                             alt="Пылесос самообслуживания 380 вольт Nerta-SW"
                             title="Пылесос самообслуживания на 1 пост, 380 вольт от Nerta-SW">
                        <div class="dop__equipment__column__title">
                            <p>пылесос на 1 ПОСТ</p>
                        </div>
                        <div class="dop__equipment__column__list">
                            <div class="dop__equipment__column__list-elem">
                                <div class="dop__equipment__column__list-elem1">Мощность</div>
                                <div class="dop__equipment__column__list-elem2">2.5кВт</div>
                            </div>
                            <div class="dop__equipment__column__list-elem">
                                <div class="dop__equipment__column__list-elem1">Производительность</div>
                                <div class="dop__equipment__column__list-elem2">180 м3/час</div>
                            </div>
                            <div class="dop__equipment__column__list-elem">
                                <div class="dop__equipment__column__list-elem1">Занимаемая площадь</div>
                                <div class="dop__equipment__column__list-elem2">500х1000 мм</div>
                            </div>
                            <div class="dop__equipment__column__list-elem">
                                <div class="dop__equipment__column__list-elem1">Расчётная система</div>
                                <div class="dop__equipment__column__list-elem2">Google Pay, Apple Pay, Samsung Pay,
                                    Visa, Mastercard, наличные
                                </div>
                            </div>
                            <div class="dop__equipment__column__list-elem">
                                <div class="dop__equipment__column__list-elem1">Цена</div>
                                <div class="dop__equipment__column__list-elem3">от 250 000 ₽</div>
                            </div>
                        </div>
                    </div>
                    <button>Узнать подробнее</button>
                </div>
                <div class="dop__equipment__column">
                    <img src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/26.png"
                         alt="Купить пылесос самообслуживания 380 вольт Nerta-SW"
                         title="Пылесос самообслуживания на 2 поста, 380 вольт от Nerta-SW">
                    <div class="dop__equipment__column__title">
                        <p>пылесос на 2 ПОСТа</p>
                    </div>
                    <div class="dop__equipment__column__list">
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Мощность</div>
                            <div class="dop__equipment__column__list-elem2">2х2.5кВт</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Производительность</div>
                            <div class="dop__equipment__column__list-elem2">2х180 м3/час</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Занимаемая площадь</div>
                            <div class="dop__equipment__column__list-elem2">600х1200 мм</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Расчётная система</div>
                            <div class="dop__equipment__column__list-elem2">Google Pay, Apple Pay, Samsung Pay, Visa,
                                Mastercard, наличные
                            </div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Цена</div>
                            <div class="dop__equipment__column__list-elem3">от 420 000 ₽</div>
                        </div>
                    </div>
                    <button>Узнать подробнее</button>
                </div>
                <div class="dop__equipment__column">
                    <img src="<?= get_template_directory_uri(); ?>/img/vacuum_cleaner/27.png"
                         alt="Мойка самообслуживания с химчисткой 380 вольт Nerta-SW"
                         title="Пылеводосос + химчистка, 380 вольт от Nerta-SW">
                    <div class="dop__equipment__column__title">
                        <p>химчистка + пылеводосос</p>
                    </div>
                    <div class="dop__equipment__column__list">
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Мощность</div>
                            <div class="dop__equipment__column__list-elem2">2х2.5кВт</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Производительность</div>
                            <div class="dop__equipment__column__list-elem2">2х180 м3/час</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Занимаемая площадь</div>
                            <div class="dop__equipment__column__list-elem2">500х1000 мм</div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Расчётная система</div>
                            <div class="dop__equipment__column__list-elem2">Google Pay, Apple Pay, Samsung Pay, Visa,
                                Mastercard, наличные
                            </div>
                        </div>
                        <div class="dop__equipment__column__list-elem">
                            <div class="dop__equipment__column__list-elem1">Цена</div>
                            <div class="dop__equipment__column__list-elem3">от 520 000 ₽</div>
                        </div>
                    </div>
                    <button>Узнать подробнее</button>
                </div>
            </div>
        </div>
    </section>

    <section class="additional__benefits">
        <div class="additional__benefits__container">
            <div class="additional__benefits__title">
                <h2>
                    Преимущества стационарных пылеводососов NERTA-SW:
                </h2>
            </div>
            <div class="additional__benefits__subtitle">
                Отличительные особенности
            </div>
            <div class="additional__benefits__blocks">
                <div class="additional__benefits__row">
                    <div class="additional__benefits__block">
                        <p class="additional__benefits__block-elem1">Долговечность и износостойкость</p>
                        <p class="additional__benefits__block-elem2">Корпус и все внутренние детали выполнены полностью
                            из нержавеющей стали AIsi 304 толщиной
                            1,5мм
                        </p>
                    </div>
                    <div class="additional__benefits__block">
                        <p class="additional__benefits__block-elem1">Грантия надежности</p>
                        <p class="additional__benefits__block-elem2">Используем только проверенные комплектующие от
                            известных брэндов. Гарантия на оборудование –
                            1 год</p>
                    </div>
                    <div class="additional__benefits__block">
                        <p class="additional__benefits__block-elem1">Удобство в деталях</p>
                        <p class="additional__benefits__block-elem2">Удобные баки для сбора мусора, которые позволяют
                            максимально быстро производить их очистку и
                            замену</p>
                    </div>
                    <div class="additional__benefits__block">
                        <p class="additional__benefits__block-elem1">Вариативность способов оплаты</p>
                        <p class="additional__benefits__block-elem2">Любые способы оплаты услуг пылесосов – наличные,
                            монеты, банковские карты, карты лояльности,
                            жетоны</p>
                    </div>
                    <div class="additional__benefits__block">
                        <p class="additional__benefits__block-elem1">Дистанционный контроль</p>
                        <p class="additional__benefits__block-elem2">Мониторинг и удаленное управление, доступ ко всей
                            финансовой статистике on-line в любое время
                            в любом месте</p>
                    </div>
                    <div class="additional__benefits__block">
                        <p class="additional__benefits__block-elem1">Индивидуальный дизайн</p>
                        <p class="additional__benefits__block-elem2">Возможны любые цветовые решения. Готовы выполнить
                            любое пожелание заказчика по
                            предоставленному ТЗ</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="feedback_grey">
        <div class="feedback__container">
            <div class="feedback__block">
                <p class="feedback__title">
                    Остались вопросы? Звоните <span class="blue">8 (800) 555-25-93</span> или оставляйте заявку на сайте
                </p>
                <p class="feedback__subtitle">
                    Наши специалисты подберут для вас идеальное решение.</p>
            </div>
            <div class="feedback__form">
                <script data-b24-form="inline/2/bvcqvg" data-skip-moving="true"> (function (w, d, u) {
                        var s = d.createElement('script');
                        s.async = true;
                        s.src = u + '?' + (Date.now() / 180000 | 0);
                        var h = d.getElementsByTagName('script')[0];
                        h.parentNode.insertBefore(s, h);
                    })(window, document, 'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_2.js'); </script>
                <!--                --><?php //echo do_shortcode('[contact-form-7 id="14156" title="Feedback index"]'); ?>
            </div>
        </div>
    </section>

    <?php get_footer(); ?>
