</div>

<footer class="footer">
    <div class="footer__container">
        <div class="footer__row">
            <div class="footer__column">
                <div class="footer__contacts">
                    <a class="footer__email" href="mailto:info@nerta-sw.ru">info@nerta-sw.ru</a><br>
                    <a class="footer__phone" itemprop="telephone" href="tel:84956777000">8 (495) 6-777-000</a><br>
                    <a class="footer__phone" itemprop="telephone" href="tel:88005552593">8 (800) 555-25-93</a><br>
<!--                    <p>8 (800) 555-25-93</p>-->
<!--                    <p>+7 (495) 6-777-000</p>-->
                </div>
                <script data-b24-form="click/4/zy99qe" data-skip-moving="true"> (function(w,d,u){ var s=d.createElement('script');s.async=true;s.src=u+'?'+(Date.now()/180000|0); var h=d.getElementsByTagName('script')[0];h.parentNode.insertBefore(s,h); })(window,document,'https://cdn-ru.bitrix24.ru/b17852604/crm/form/loader_4.js'); </script>
                <button class="footer__feedback-button">Перезвоните мне</button>
                <div class="footer__address" itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                    Россия, <span itemprop="addressLocality">Московская <br> область,</span> <span itemprop="streetAddress">Остаповский <br> проезд, 3,стр. 5 </span><br><br>
<!--                    <a href="/kontakty/#map_contacts">на карте</a>-->
                </div>
                <a href="#" class="footer__map__link">НА КАРТЕ</a>
            </div>
            <div class="footer__column" id="map" style="overflow: hidden;">
                <!-- <img src="<?= get_template_directory_uri(); ?>/img/index/map.png"
                     title="Карта до офиса компании производителя моющего оборудования Nerta-SW"
                     alt="карта проезда в офис компанииNerta-SW"> -->
	            <div style="position:relative;overflow:hidden;"><a href="https://yandex.by/maps/213/moscow/?utm_medium=mapframe&utm_source=maps" style="color:#eee;font-size:12px;position:absolute;top:0px;">Москва</a><a href="https://yandex.by/maps/213/moscow/house/ostapovskiy_proyezd_3s5/Z04YcQdhSkAPQFtvfXtzcnRjZA==/?from=mapframe&ll=37.701750%2C55.723767&utm_medium=mapframe&utm_source=maps&z=19.43" style="color:#eee;font-size:12px;position:absolute;top:14px;">Остаповский проезд, 3с5 на карте Москвы, ближайшее метро Волгоградский проспект — Яндекс.Карты</a><iframe loading="lazy" src="https://yandex.by/map-widget/v1/-/CCUymRVY0A" width="100%" height="291" frameborder="1" allowfullscreen="true" style="position:relative;"></iframe></div>
            </div>
            <div class="footer__column">
                <menu>
                    <a href='<?php home_url("template_url"); ?>/oborudovanie_dlya-moek_samoobsluzhivaniya/'>оборудование</a>
                    <a href='<?php home_url("template_url"); ?>/mojka-samoobsluzhivaniya-pod-klyuch/'>мойка самообслуживания под ключ</a>
                    <a href='#'>услуги</a>
                    <ul class="submenu">
                        <li><a href='<?php home_url("template_url"); ?>/mojka-samoobsluzhivaniya-pod-klyuch/'>Автомойка под ключ</a></li>
                        <li><a href='<?php home_url("template_url"); ?>/avtomoika-v-lizing-pod-kluch/'>Автомойка в лизинг</a></li>
                        <li><a href='<?php home_url("template_url"); ?>/franshiza-mojka-samoobsluzhivaniya/'>Франшиза мойки самообслуживания</a></li>
                        <li><a href='<?php home_url("template_url"); ?>/gotovii-biznes/'>Продажа автомоек самообслуживания</a></li>
                    </ul>
                    <a href='<?php home_url("template_url"); ?>/o-kompanii/'>о компании</a>
                    <a href='<?php home_url("template_url"); ?>/kontakty/'>контакты</a>
                </menu>
            </div>
        </div>
        <div class="footer__row__information">
            <p class="">© Nerta Selfwash, 2001–2020</p>
            <div style="cursor: pointer" onclick="return location.href = '<?php home_url("template_url"); ?>/politika-konfidencialnosti/'" class="">Политика конфиденциальности</div>
            <p class="footer__empty__column">%%%%%%%%%%%%%%%%%%%%%%%%</p>
        </div>
    </div>
</footer>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-142223446-2"></script>

<?php
    wp_enqueue_script('jquery');
    wp_enqueue_script('newscript', get_template_directory_uri() . '/js/nerta.js');
    wp_footer(); ?>
<noscript><div><img src="https://mc.yandex.ru/watch/22985389" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
</body>
</html>
