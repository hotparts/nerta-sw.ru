const { src, dest, parallel, series, watch } = require('gulp');
const uglify = require('gulp-uglify-es').default;
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const cleancss = require('gulp-clean-css');
const imagemin = require('gulp-imagemin');
const newer = require('gulp-newer');
const del = require('del');
const browserify = require('browserify');
const source  = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const merge = require('merge-stream');
const glob = require('glob');
const path = require('path');

function scripts() {
    let files = glob.sync('./assets/js/*.js');
    return merge(files.map(function(file) {
        return browserify({
            entries: file,
            debug: true
        })
            .transform('babelify', {
                presets: ['@babel/env'],
                plugins: ['@babel/transform-runtime']
            })
            .bundle()
            .pipe(source(path.basename(file, '.js') + ".js"))
            .pipe(buffer())
            .pipe(uglify())
            .pipe(dest('js/'))
    }));
}

exports.scripts = scripts;

function styles() {
    return src('assets/sass/*.sass')
        .pipe(sass())
        .pipe(autoprefixer({ overrideBrowserslist: ['last 10 versions'], grid: true }))
        .pipe(cleancss( { level: { 1: { specialComments: 0 } } } ))
        .pipe(dest('css/'))
}

exports.styles = styles;

function images() {
    return src('assets/image/**/*')
        .pipe(newer('img/'))
        // .pipe(imagemin())
        .pipe(dest('img/'))
}

exports.images = images;

// для очистки папки с оптимизированными изображениями
function cleanimg() {
    return del('img/**/*', { force: true })
}

exports.cleanimg = cleanimg;

function watchFiles() {
    watch("assets/sass/**/*", styles);
    watch("assets/js/**/*", scripts);
    watch("assets/image/**/*", images);
}

exports.watch = watchFiles;

exports.build = series(cleanimg, styles, scripts, images);
