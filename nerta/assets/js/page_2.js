import { ProgressBar } from "./components/progress-bar.js";

const progressBar = new ProgressBar('#progress-bar', {
	classStart: '.progress-bar-start', // блок, после которого начинается отображение progress bar
	classEnd: '.progress-bar-end',	  // блок, на котором прекращается оторабражение progress bar
	animationSpeed: 400,  // скорость анимации, по умолчанию 0
	items: [
		{
			'positionProgress': 1,
			'blockId': 			'#description',
			'text': 			'Франшиза <br> Nerta-SW'
		},
		{
			'positionProgress': 2,
			'blockId': 			'#cost',
			'text': 			'Стоимость <br> открытия'
		},
		{
			'positionProgress': 3,
			'blockId': 			'#fast',
			'text': 			'Бюджет <br> и выручка'
		},
		{
			'positionProgress': 4,
			'blockId': 			'#conditions',
			'text': 			'Условия <br> строительства'
		},
	],
})

jQuery(document).ready(function() {
	let images = document.querySelectorAll("img[data-src]");
	lazyload(images);	
});	