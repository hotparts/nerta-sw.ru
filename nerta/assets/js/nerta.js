window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());
  gtag('config', 'UA-142223446-2');

// Yandex.Metrika counter
(function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
(window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

ym(22985389, "init", {
clickmap:true,
trackLinks:true,
accurateTrackBounce:true,
webvisor:true,
ecommerce:"dataLayer"
});

(function (d, w, c) {
(w[c] = w[c] || []).push(function() {
    try {
        w.yaCounter22985389 = new Ya.Metrika({
            id:22985389,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true
        });
    } catch(e) { }
});

var n = d.getElementsByTagName("script")[0],
    s = d.createElement("script"),
    f = function () { n.parentNode.insertBefore(s, n); };
s.type = "text/javascript";
s.async = true;
s.src = "https://mc.yandex.ru/metrika/watch.js";

if (w.opera == "[object Opera]") {
    d.addEventListener("DOMContentLoaded", f, false);
} else { f(); }
})(document, window, "yandex_metrika_callbacks");

var main = function($) {
    $('.js-menu-show').on('click', function(e){
        e.preventDefault();
        $(this).parent().toggleClass('menu-open');
    });

    // @dinar 24.08.20 mobile menu script
    $('#header-menu').click(function(event) {
        $('body').toggleClass('disable');
        $(this).toggleClass('active');
        $('#menu').toggleClass('active');
    });

    function openPopup() {
        var $body = $('body');
        $body.css({
            'overflow':'hidden',
            'width': $body.width()
        });
        $('#js-callback-popup').show();
    }

    function closePopup() {
        $('body').css({
            'overflow':'',
            'width': ''
        });
        $('#js-callback-popup').hide();
    }

    $('.js-callback-popup-show').on('click', function(e){
        e.preventDefault();
        openPopup();
    });

    $('#js-callback-popup').on('click', function(e){
        e.preventDefault();
        closePopup();
    });

    $('#js-callback-popup .popup').on('click', function(e){
        e.stopPropagation();
    });


    //slider switcher

    var $sliderSwithcers = $('.gallery-switcher');
    var $sliderList = $('.gallery-list .gallery-item');

    $sliderSwithcers.on('click', 'span', function(){
        var index = $(this).index();
        var $slider = $sliderList.eq(index);
        $sliderSwithcers.each(function(){
            $(this).find('span').each(function(){
                $(this).toggleClass('active', $(this).index() == index);
            });
        });

        $sliderList.hide(0);
        $slider.show(0, function(){
            $(window).trigger('resize');
        });
    });

    (function(){

        var complectation_active = 'light';
        var complectation_compare = 'base';

        //calculator

        var post_count = 4;
        var montag = false;
        var dop = false;

        var price = {
            'start' : {
                'montag': 0,
                'post-cost-small': 2.4,
                'post-cost-large': 2.4
            },
            'base' : {
                'montag': 2,
                'post-cost-small': 5,
                'post-cost-large': 5
            },
            'light' : {
                'montag': 3,
                'post-cost-small': 8,
                'post-cost-large': 7
            },
            'optima' : {
                'montag': 3,
                'post-cost-small': 10.5,
                'post-cost-large': 10
            },
            'maxi' : {
                'montag': 3,
                'post-cost-small': 13,
                'post-cost-large': 12
            }
        };

        var dop_price = {
            'preparation': {
                'post-cost-small': 5.5,
                'post-cost-medium': 7.5,
                'post-cost-large': 9
            },
            'osmosis': {
                'post-cost-small': 2.5,
                'post-cost-medium': 3.5,
                'post-cost-large': 4
            }
        };

        var $post_count_btn = $('.js-post-count');
        var $post_cost = $('.js-post-cost');
        var $post_montag = $('.js-post-montag');
        var $post_montag_cost = $('.js-post-montag-cost');
        var $post_dop = $('.js-post-dop');
        var $post_dop_cost = $('.js-post-dop-cost');

        function postRefresh () {
            var dop_cost = dopRefresh();
            $post_cost.each(function(){
                var type = $(this).data('type');
                console.log("___");
                console.log('type=', type);
                var cost = Math.round((post_count * (post_count <= 6 ? price[type]['post-cost-small'] : price[type]['post-cost-large']) + (montag ? price[type]['montag'] : 0)) * 1000 + (dop ? dop_cost : 0));
                console.log('post_count=', post_count);
                console.log('montag=', montag);
                console.log('price[type][montag]=', price[type]['montag']);
                console.log("___");
                $(this).html(cost);
                if (type === complectation_active) {
                    $post_montag_cost.html(price[type]['montag'] ? price[type]['montag'] * 1000 : '-');
                }
            });
            $post_count_btn.each(function(){
                $(this).toggleClass('active', ($(this).data('count')  == post_count));
            });
        }

        var $dop_cost = $('.js-post-dop-price');

        function dopRefresh() {
            var dop_cost = 0;
            $dop_cost.each(function(){
                var type = $(this).data('type');
                var cost = 0;
                if (post_count < 4) {
                    cost = dop_price[type]['post-cost-small'];
                } else if (post_count < 7) {
                    cost = dop_price[type]['post-cost-medium'];
                } else {
                    cost = dop_price[type]['post-cost-large'];
                }
                cost = Math.round(cost * 1000);
                dop_cost += cost;
                $(this).html(cost);
            });
            $post_dop_cost.html(dop_cost);
            return dop_cost;
        }

        $post_count_btn.on('click', function(){
            post_count = $(this).data('count');
            postRefresh();
        });

        $post_montag.on('change', function(){
            montag = $(this).is(':checked');
            postRefresh();
        });

        $post_dop.on('change', function(){
            dop = $(this).is(':checked');
            postRefresh();
        });

        postRefresh();

        //complectation

        var complectation = {
            1: 'Рама из нержавеющей стали Aisi 304',
            2: 'Электромагнитный клапан (Италия)',
            3: 'Электродвигатель Ravel (Италия)',
            4: 'Насосная станция Wilo (Германия)',
            5: 'Система против замерзания ANTI FROST',
            6: 'Магистрали подачи воды с шаровым кранами',
            7: 'Система удаленного контроля и управления постом самообслуживания',
            8: 'Пистолеты высокого давления (Германия)',
            9: 'Пистолеты низкого давления (Германия)',
            10: 'Держатель для пистолета из нержавеющей стали',
            11: 'Поворотная консоль 1800 мм из нержавеющей стали',
            12: 'Держатель для автомобильных ковриков из нержавеющей стали',
            13: 'Упрощённый вариант панели управления мойкой. С сенсорными клавишами и подсветкой. ',
            14: '<span class="gray">Для этой комплектации монтаж не предоставляется.</span>',
            15: 'Промышленный насос Annovi Reverberi (Италия) ',
            16: 'Купюроприемник ICT ',
            17: 'Монетоприемник EU-2',
            18: 'Система дозировки жидких химических реагентов Etatron (Италия) ',
            19: 'Магистраль подачи воды с шаровым кранами',
            20: 'Электрический шкаф (Россия)',
            21: 'Комплект рукавов высокого давления (Тайвань) ',
            22: 'Воздушный компрессор',
            23: 'Бойлер косвенного нагрева на 300 литров',
            24: 'Частотный преобразователь Prostar (Китай)',
            25: 'Внешняя панель управления мойкой (панель пользователя) с сенсорными клавишами с подсветкой',
            26: 'Купюроприемник CashCode MVU (Канада)',
            27: 'Монетоприемник Comestero (Италия)',
            28: 'Воздушный компрессор Remeza (Италия-Белоруссия)',
            29: 'Бойлер косвенного нагрева Baxi на 400 литров (Германия)',
            30: 'Станция повышения давления Wilo (Германия)',
            31: 'Система дозировки жидких химических реагентов Etatron PRO (Италия)',
            32: 'Электрический шкаф (Франция)',
            33: 'Комплект электрооборудования (Франция)',
            34: 'Частотный преобразователь Schneider Electric (Франция)',
            35: 'Комплект рукавов высокого давления (Германия)',
            36: 'Промышленный насос CatPump (Япония)',
            37: 'Купюроприемник CashCode SM с кассетой на 1000 купюр (Канада)',
            38: 'Воздушный компрессор LA PADANA (Италия)',
            39: 'Бойлер косвенного нагрева Baxi на 300 литров (Германия',
            40: 'Система дозировки жидких химических реагентов Seco (Италия)',
            41: 'Бесконтактные карты оплаты',
            42: 'Электрический шкаф (Германия) ',
            43: 'Комплект электрооборудования (Великобритания)',
            44: 'Частотный преобразователь Eaton (Великобритания)',
            45: 'Пылесос самообслуживания на 1 пост'
        };

        var complectation_type = {
            'start': [13,14],
            'base': [15,13,16,17,18,19,20,21],
            'light': [15,13,16,17,22,23,18,20,24,21],
            'optima': [25,15,26,27,28,29,30,31,32,33,34,35],
            'maxi': [25,36,37,27,38,39,30,40,42,43,44,35,45,41]
        };

        var complectation_general = [1,2,3,4,5,6,7,8,9,10,11,12];

        var complectation_block = $('.costcompare');
        var complectation_btn = $('.js-costtable-btn');
        var complectation_compare_btn = $('.js-costtable-compare');


        function create_item(str_left, str_right) {
            var str =   '<div class="costcompare-list">' +
                        '<div class="costcompare-item">' +
                        '<div class="costcompare-text">' +
                            str_left +
                        '</div>' +
                        '</div>' +
                        '<div class="costcompare-item costcompare-item-right">' +
                        '<div class="costcompare-text">' +
                            str_right +
                        '</div>' +
                        '</div>' +
                        '</div>';
            return $(str);
        }

        function complectation_show (main,slave,same) {
            complectation_block.html('');

            complectation_block.append('<div class="costcompare-difference"><span>Комплектации</span></div>');
            complectation_block.append(create_item('<div class="costcompare-title">' + complectation_active + '</div>','<div class="costcompare-title">' + complectation_compare + '</div>'));

            $.each(same, function(index,value){
                complectation_block.append(create_item(complectation[value],complectation[value]));
            });

            if (complectation_compare) complectation_block.append('<div class="costcompare-difference"><span>Различия</span></div>');

            var count = Math.max(main.length, slave.length);

            for (var i = 0; i < count; i++) {
                complectation_block.append(create_item(main[i] ? complectation[main[i]] : '',slave[i] ? complectation[slave[i]] : ''));
            }

            complectation_block.append('<div class="costcompare-difference"><span>Оборудование ниже входит во все комплектации</span></div>');
            complectation_block.append('<div class="costcompare-difference-mobile"><span>Входит во все комплектации</span></div>');

            $.each(complectation_general, function(index,value){
                complectation_block.append(create_item(complectation[value], complectation_compare ? complectation[value] : ''));
            });
        }

        function complectation_refresh(){
            var main = [];
            var slave = [];
            var same = [];

            complectation_btn.each(function(){
                $(this).closest('li').toggleClass('active', $(this).data('type') == complectation_active);
            });

            if (!complectation_compare) {
                main = complectation_type[complectation_active].slice();
            } else {
                $.each(complectation_type[complectation_active], function(index, value){
                    if ($.inArray(value,complectation_type[complectation_compare]) == -1){
                        main.push(value);
                    } else {
                        same.push(value)
                    }
                });
                $.each(complectation_type[complectation_compare], function(index, value){
                    if ($.inArray(value,complectation_type[complectation_active]) == -1)
                        slave.push(value);
                });
            }

            complectation_show(main,slave,same);
        }

        complectation_btn.on('click', function(){
            var type = $(this).data('type');
            complectation_active = type;
            complectation_compare = '';
            complectation_refresh();
            postRefresh();
        });

        complectation_compare_btn.on('click', function(){
            var type = $(this).data('type');
            complectation_compare = type;
            complectation_refresh();
        });

        complectation_refresh();
    })();
};

var timer_metaslider_19 = function() {
    !window.jQuery ? window.setTimeout(timer_metaslider_19, 100) : !jQuery.isReady ? window.setTimeout(timer_metaslider_19, 1) : main(window.jQuery);
};
timer_metaslider_19();
