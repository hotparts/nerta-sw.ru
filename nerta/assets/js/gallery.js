// import Swiper JS
import Swiper from 'swiper';

const swiper = new Swiper('.swiper-1', {
    navigation: {
        nextEl: '.swiper-button-next1',
        prevEl: '.swiper-button-prev1',
    },
    spaceBetween: 5,
    slidesPerView: 1,
    breakpoints: {
        768: {
            slidesPerView: 2
        },
        1140: {
            slidesPerView: 3
        },
        1440: {
            slidesPerView: 4
        }
    },
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});

const swiper_2 = new Swiper('.swiper-2', {
    navigation: {
        nextEl: '.swiper-button-next2',
        prevEl: '.swiper-button-prev2',
    },
    spaceBetween: 5,
    slidesPerView: 1,
    breakpoints: {
        768: {
            slidesPerView: 2
        },
        1140: {
            slidesPerView: 3
        },
        1440: {
            slidesPerView: 4
        }
    },
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});

const swiper_3 = new Swiper('.swiper-3', {
    navigation: {
        nextEl: '.swiper-button-next3',
        prevEl: '.swiper-button-prev3',
    },
    spaceBetween: 5,
    slidesPerView: 1,
    breakpoints: {
        720: {
            slidesPerView: 2
        },
        970: {
            slidesPerView: 3
        }
    },
    freeMode: true,
    watchSlidesVisibility: true,
    watchSlidesProgress: true,
});
