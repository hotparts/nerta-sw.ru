import { ProgressBar } from "./components/progress-bar.js";

const progressBar = new ProgressBar('#progress-bar', {
	classStart: '.progress-bar-start', // блок, после которого начинается отображение progress bar
	classEnd: '.progress-bar-end',	  // блок, на котором прекращается оторабражение progress bar
	animationSpeed: 400,  // скорость анимации, по умолчанию 0
	items: [
		{
			'positionProgress': 1,
			'blockId': 			'#conditions',
			'text': 			'Условия'
		},
		{
			'positionProgress': 2,
			'blockId': 			'#who',
			'text': 			'Для кого?'
		},
		{
			'positionProgress': 3,
			'blockId': 			'#advantages',
			'text': 			'Преимущества'
		},
		{
			'positionProgress': 4,
			'blockId': 			'#price',
			'text': 			'Цена'
		},
		{
			'positionProgress': 5,
			'blockId': 			'#reliability',
			'text': 			'Лизингодатель'
		},
	],
})

jQuery(document).ready(function() {
	let images = document.querySelectorAll("img[data-src]");
	lazyload(images);	
});	