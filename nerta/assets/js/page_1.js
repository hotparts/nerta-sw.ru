import { ProgressBar } from "./components/progress-bar.js";
 
const progressBar = new ProgressBar('#progress-bar', {
	classStart: '.start', // блок, после которого начинается отображение progress bar
	classEnd: '.end',	  // блок, на котором прекращается оторабражение progress bar
	animationSpeed: 400,
	items: [
		{
			'positionProgress': 1,
			'blockId': 			'#steps',
			'text': 			'Этапы <br> реализации'
		},
		{
			'positionProgress': 2,
			'blockId': 			'#costs',
			'text': 			'Итоговая <br> стоимость'
		},
		{
			'positionProgress': 3,
			'blockId': 			'#reasons',
			'text': 			'5 причин <br> выбрать <br>нас'
		},
		{
			'positionProgress': 4,
			'blockId': 			'#how',
			'text': 			'С чего <br> начать?'
		},
	],
})

jQuery(document).ready(function() {
	let images = document.querySelectorAll("img[data-src]");
	lazyload(images);	
});	
