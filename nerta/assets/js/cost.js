jQuery('.block__cost__number').each( replace_currency );
jQuery('.services__information__numbers').each( replace_currency );

function replace_currency() {
    let block_numbers = jQuery(this)[0]['children'];
    let eur = Number(((block_numbers[0]['innerText'].slice(0, -2)).replace(/\s/g, '')).replace('от', ''));
    if(eur > 0){
        block_numbers[1]['innerText'] = (Math.round(eur * 88)).toLocaleString('ru') + ' ₽' ;
    }
}