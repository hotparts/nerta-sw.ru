import { ProgressBar } from "./components/progress-bar.js";

const progressBar = new ProgressBar('#progress-bar', {
    classStart: '.progress-bar-start', // блок, после которого начинается отображение progress bar
    classEnd: '.progress-bar-end',	  // блок, на котором прекращается оторабражение progress bar
    animationSpeed: 400,  // скорость анимации, по умолчанию 0
    items: [
        {
            'positionProgress': 1,
            'blockId': 			'#business',
            'text': 			'Готовый <br> бизнес'
        },
        {
            'positionProgress': 2,
            'blockId': 			'#safety',
            'text': 			'Безопасность'
        },
        {
            'positionProgress': 3,
            'blockId': 			'#mso',
            'text': 			'Как <br>выбрать<br>МСО?'
        },
        {
            'positionProgress': 4,
            'blockId': 			'#reasons',
            'text': 			'Почему их <br> продают?'
        },
        {
            'positionProgress': 5,
            'blockId': 			'#price',
            'text': 			'Цена'
        },
    ],
})

jQuery(document).ready(function() {
    let images = document.querySelectorAll("img[data-src]");
    lazyload(images);
});